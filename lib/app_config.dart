String baseUrl = 'https://erp-dev-api.devslane.com:8080/api';
bool isProduction = false;
String sentryDSNUrl = '';

void setupEnv(Environment env) {
  switch (env) {
    case Environment.dev:
      {
        baseUrl = 'https://erp-dev-api.devslane.com:8080/api';

        break;
      }

    case Environment.prod:
      {
        isProduction = true;
        baseUrl = 'https://erp-api.devslane.com:8080/api';
        sentryDSNUrl =
            'https://de8e705420d0419f8ea546f016b8a94e@sentry.io/1544219';
      }
  }
}

enum Environment { dev, prod }
