import 'package:devslane_erp_flutter/app_config.dart';
import 'package:devslane_erp_flutter/main.dart';
import 'package:flutter/material.dart';

void main() {
  setupEnv(Environment.dev);
  runApp(DevsLaneErpApp());
}
