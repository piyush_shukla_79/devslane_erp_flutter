import 'package:devslane_erp_flutter/app_config.dart';
import 'package:devslane_erp_flutter/presentations/calendar_page.dart';
import 'package:devslane_erp_flutter/presentations/daily_updates_page.dart';
import 'package:devslane_erp_flutter/presentations/employees_profile_page.dart';
import 'package:devslane_erp_flutter/presentations/forget_password_page.dart';
import 'package:devslane_erp_flutter/presentations/function/add_dailyupdate.dart';
import 'package:devslane_erp_flutter/presentations/home_page.dart';
import 'package:devslane_erp_flutter/presentations/login_page.dart';
import 'package:devslane_erp_flutter/presentations/profile_edit_page.dart';
import 'package:devslane_erp_flutter/presentations/requests_page.dart';
import 'package:devslane_erp_flutter/presentations/splash_page.dart';
import 'package:devslane_erp_flutter/stores/calendar_store.dart';
import 'package:devslane_erp_flutter/stores/daily_update_store.dart';
import 'package:devslane_erp_flutter/stores/employee_store.dart';
import 'package:devslane_erp_flutter/stores/holiday_store.dart';
import 'package:devslane_erp_flutter/stores/projects_store.dart';
import 'package:devslane_erp_flutter/stores/request_store.dart';
import 'package:devslane_erp_flutter/stores/user_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'models/provider.dart';

class DevsLaneErpApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: MultiProvider(
        providers: <SingleChildCloneableWidget>[
          Provider<UserStore>.value(value: UserStore()),
          Provider<DailyUpdateStore>.value(value: DailyUpdateStore()),
          Provider<ProjectsStore>.value(value: ProjectsStore()),
          Provider<EmployeeStore>.value(value: EmployeeStore()),
          Provider<RequestStore>.value(value: RequestStore()),
          Provider<CalendarStore>.value(value: CalendarStore()),
          Provider<HolidayStore>.value(value: HolidayStore()),
        ],
        child: ChangeNotifierProvider<NavigationProvider>(
          builder: (_) => NavigationProvider(),
          child: MaterialApp(
            title: 'DevsLane Erp',
            debugShowCheckedModeBanner: !isProduction,
            theme: ThemeData(
                primarySwatch: Colors.blue, primaryColor: Colors.white),
            initialRoute: SplashPage.routeNamed,
            routes: {
              HomePage.routeNamed: (BuildContext context) => HomePage(),
              DailyUpdatesPage.routeNamed: (BuildContext context) =>
                  DailyUpdatesPage(),
              LoginPage.routeNamed: (BuildContext context) => LoginPage(),
              SplashPage.routeNamed: (BuildContext context) => SplashPage(),
              AddDailyUpdates.routeNamed: (BuildContext context) =>
                  AddDailyUpdates(),
              RequestsPage.routeNamed: (BuildContext context) =>
                  const RequestsPage(),
              EmployeesProfilePage.routeNamed: (BuildContext context) =>
                  EmployeesProfilePage(),
              ProfileEditPage.routeNamed: (BuildContext context) =>
                  ProfileEditPage(),
              CalendarPage.routeNamed: (BuildContext context) => CalendarPage(),
              ForgetPasswordPage.routeNamed: (BuildContext context) =>
                  ForgetPasswordPage(),
            },
          ),
        ),
      ),
    );
  }
}
