import 'package:devslane_erp_flutter/models/user.dart';
import 'package:devslane_erp_flutter/utils/globals.dart';
import 'package:mobx/mobx.dart';

part 'employee_store.g.dart';

// This is the class used by rest of your codebase
class EmployeeStore = _EmployeeStore with _$EmployeeStore;

// The store-class
abstract class _EmployeeStore with Store {
  @observable
  ObservableList<User> employeeList = ObservableList<User>();

  @observable
  bool isLoading = false;

  @computed
  Map<int, User> get getGroupedDailyUpdates {
    return employeeList.fold(
        {},
        (Map<int, User> dailyUpdatesMap, User user) =>
            dailyUpdatesMap..addAll({user.id: user}));
  }

  @action
  void fetchData() {
    isLoading = true;
    employeeService
        .getEmployeeDetails()
        .then(fetchDataComplete)
        .catchError((dynamic e) {
      stopFetch();
    }).timeout(const Duration(seconds: 10), onTimeout: () {
      isLoading = false;
    });
  }

  @action
  void fetchDataComplete(List<User> objects) {
    employeeList.addAll(objects);
    isLoading = false;
  }

  @action
  void clearEmployeeStore() {
    employeeList.clear();
    isLoading = false;
  }

  @action
  void stopFetch() {
    isLoading = false;
  }
}
