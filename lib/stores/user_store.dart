import 'package:devslane_erp_flutter/models/login_response.dart';
import 'package:devslane_erp_flutter/models/user.dart';
import 'package:devslane_erp_flutter/utils/globals.dart';
import 'package:mobx/mobx.dart';

part 'user_store.g.dart';

class UserStore = _UserStore with _$UserStore;

abstract class _UserStore with Store {
  @observable
  bool isLoggedIn = false;

  @observable
  bool isLoggingIn = false;

  @observable
  bool isUpdating = false;

  @observable
  bool isChangingPassword = false;

  @observable
  bool isLoading = false;

  @observable
  User loggedInUser;

  @observable
  bool isError = false;

  @observable
  String errorDetail;

  @action
  void clearUserStore() {
    loggedInUser = null;
    isLoggedIn = false;
    loggedInUser = null;
    isLoggingIn = false;
    isUpdating = false;
    isChangingPassword = false;
    isLoading = false;
    loggedInUser = null;
    isError = false;
    errorDetail = null;
  }

  @action
  void changePassword() {
    isChangingPassword = false;
  }

  @action
  void setIsLoggingIn({Map<String, dynamic> loginDetails}) {
    isLoggingIn = true;
    authService
        .login(email: loginDetails['email'], password: loginDetails['password'])
        .then((LoginResponse loginResponse) {
      setLoggedIn(loginResponse.user);
      preferencesService.setAuthToken(loginResponse.token);
      preferencesService.setAuthUser(loginResponse.user);
      preferencesService.setIsFirstLaunch(false);
    }).catchError(errorHandling);
  }

  @action
  void forgetPassword(String email) {
    isLoggingIn = true;
    isChangingPassword = false;
    authService.forgetPassword(email: email).then((response) {
      isLoggingIn = false;
      isChangingPassword = true;
    }).catchError(errorHandling);
  }

  @action
  void errorHandling(dynamic e) {
    isError = true;
    errorDetail = e.toString();
    isLoggingIn = false;
  }

  @action
  void setLoggedIn(User user) {
    isLoggedIn = true;
    isLoggingIn = false;
    isError = false;
    loggedInUser = user;
    preferencesService.setAuthUser(user);
  }

  @action
  void setUserLoggedIn() {
    isLoggedIn = true;
  }

  @computed
  User get userDetails {
    if (loggedInUser == null) {
      fetchUserDetails();
    } else {
      return loggedInUser;
    }
  }

  @action
  void fetchUserDetails() {
    if (loggedInUser == null) {
      isLoading = true;
      profilePageService
          .getProfileDetails()
          .then(fetchUserDetailsComplete)
          .catchError((dynamic e) {})
          .timeout(
        const Duration(seconds: 10),
        onTimeout: () {
          isLoading = false;
        },
      );
    }
  }

  @action
  void updateUserDetails(Map<String, dynamic> user) {
    isUpdating = true;
    profilePageService
        .updateUserProfile(user: user)
        .then(fetchUserDetailsComplete);
  }

  @action
  void fetchUserDetailsComplete(dynamic user) {
    loggedInUser = user;
    isLoading = false;
    isUpdating = false;
  }
}
