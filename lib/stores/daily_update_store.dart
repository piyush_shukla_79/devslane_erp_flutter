import 'package:devslane_erp_flutter/models/daily_update.dart';
import 'package:devslane_erp_flutter/models/user.dart';
import 'package:devslane_erp_flutter/utils/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';

part 'daily_update_store.g.dart';

// This is the class used by rest of your codebase
class DailyUpdateStore = _DailyUpdateStore with _$DailyUpdateStore;

// The store-class
abstract class _DailyUpdateStore with Store {
  @observable
  ObservableList<DailyUpdates> dailyUpdates = ObservableList<DailyUpdates>();

  @observable
  ObservableList<DailyUpdates> filteredDailyUpdates =
      ObservableList<DailyUpdates>();

  @observable
  bool isFilterApplied = false;

  @observable
  User user;

  @observable
  bool isLoading = true;

  @observable
  bool hasMore = true;

  @observable
  bool hasMoreFilterUpdates = true;

  @observable
  bool isFetching = false;

  @observable
  bool isAdding = false;

  @observable
  String startDate;

  @observable
  bool showFilter = false;

  @observable
  String endDate;

  @observable
  @computed
  Map<String, List<DailyUpdates>> get getGroupedDailyUpdates {
    if (!isFilterApplied) {
      return dailyUpdates.fold(
        <String, List<DailyUpdates>>{},
        (Map<String, List<DailyUpdates>> dailyUpdatesMap,
                DailyUpdates update) =>
            dailyUpdatesMap
              ..addAll(
                <String, List<DailyUpdates>>{
                  update.date:
                      (dailyUpdatesMap[update.date] ?? <DailyUpdates>[])
                        ..add(update)
                },
              ),
      );
    } else {
      if (filteredDailyUpdates.isEmpty) {
        return <String, List<DailyUpdates>>{};
      } else {
        if (startDate != null) {
          if (endDate != null) {
            return filteredDailyUpdates
                .where((DailyUpdates dailyUpdates) =>
                    dailyUpdates.date.compareTo(startDate) > 0 &&
                    dailyUpdates.date.compareTo(endDate) <= 0)
                .toList()
                .fold(<String, List<DailyUpdates>>{},
                    (Map<String, List<DailyUpdates>> dailyUpdatesMap,
                        DailyUpdates update) {
              if (dailyUpdatesMap.containsKey(update.date) &&
                  dailyUpdatesMap[update.date].contains(update)) {
                return dailyUpdatesMap
                  ..addAll(<String, List<DailyUpdates>>{
                    update.date:
                        (dailyUpdatesMap[update.date] ?? <DailyUpdates>[])
                          ..add(update)
                  });
              } else {
                return dailyUpdatesMap
                  ..addAll(<String, List<DailyUpdates>>{
                    update.date:
                        (dailyUpdatesMap[update.date] ?? <DailyUpdates>[])
                          ..add(update)
                  });
              }
            });
          } else {
            return filteredDailyUpdates
                .where((DailyUpdates dailyUpdates) =>
                    dailyUpdates.date.compareTo(startDate) > 0)
                .toList()
                .fold(<String, List<DailyUpdates>>{},
                    (Map<String, List<DailyUpdates>> dailyUpdatesMap,
                        DailyUpdates update) {
              if (dailyUpdatesMap.containsKey(update.date) &&
                  dailyUpdatesMap[update.date].contains(update)) {
                return dailyUpdatesMap
                  ..addAll(<String, List<DailyUpdates>>{
                    update.date:
                        (dailyUpdatesMap[update.date] ?? <DailyUpdates>[])
                          ..add(update)
                  });
              } else {
                return dailyUpdatesMap
                  ..addAll(<String, List<DailyUpdates>>{
                    update.date:
                        (dailyUpdatesMap[update.date] ?? <DailyUpdates>[])
                          ..add(update)
                  });
              }
            });
          }
        } else if (endDate != null) {
          return filteredDailyUpdates
              .where((DailyUpdates dailyUpdates) =>
                  dailyUpdates.date.compareTo(endDate) <= 0)
              .toList()
              .fold(<String, List<DailyUpdates>>{},
                  (Map<String, List<DailyUpdates>> dailyUpdatesMap,
                      DailyUpdates update) {
            if (dailyUpdatesMap.containsKey(update.date) &&
                dailyUpdatesMap[update.date].contains(update)) {
              return dailyUpdatesMap
                ..addAll(<String, List<DailyUpdates>>{
                  update.date:
                      (dailyUpdatesMap[update.date] ?? <DailyUpdates>[])
                        ..add(update)
                });
            } else {
              return dailyUpdatesMap
                ..addAll(<String, List<DailyUpdates>>{
                  update.date:
                      (dailyUpdatesMap[update.date] ?? <DailyUpdates>[])
                        ..add(update)
                });
            }
          });
        } else {
          return filteredDailyUpdates.fold(<String, List<DailyUpdates>>{},
              (Map<String, List<DailyUpdates>> dailyUpdatesMap,
                  DailyUpdates update) {
            if (dailyUpdatesMap.containsKey(update.date) &&
                dailyUpdatesMap[update.date].contains(update)) {
              return dailyUpdatesMap
                ..addAll(<String, List<DailyUpdates>>{
                  update.date:
                      (dailyUpdatesMap[update.date] ?? <DailyUpdates>[])
                        ..add(update)
                });
            } else {
              return dailyUpdatesMap
                ..addAll(<String, List<DailyUpdates>>{
                  update.date:
                      (dailyUpdatesMap[update.date] ?? <DailyUpdates>[])
                        ..add(update)
                });
            }
          });
        }
      }
    }
  }

  @action
  void showFilterFunction() {
    preferencesService.getAuthUser().then((User user) {
      if (user.role == 'admin' || user.role == 'owner') {
        showFilter = true;
      }
    });
  }

  @action
  void onChanged({String startDate, String endDate, User user}) {
    if (user != null) {
      isFilterApplied = true;
    }
    this.user = user;
    this.startDate = startDate;
    this.endDate = endDate;
    hasMoreFilterUpdates = true;
  }

  @action
  void deleteDailyUpdate(int index) {
    dailyUpdates.removeAt(index);
  }

  @action
  void fetchData({int length = 20}) {
    isFetching = true;
    dailyUpdateService
        .getDailyUpdates(
            pageCount: length, userId: user != null ? user.id.toString() : null)
        .then((List<DailyUpdates> updates) {
      if (updates.length < length) {
        hasMore = false;
      }
      fetchDataComplete(updates);
    }).catchError((dynamic e) {
      stopFetch();
    }).timeout(Duration(seconds: 10), onTimeout: () {
      isFetching = false;
    });
  }

  @action
  void fetchDataComplete(List<DailyUpdates> objects) {
    if (dailyUpdates.isEmpty) {
      showFilterFunction();
    }
    if (isFilterApplied) {
      filteredDailyUpdates.clear();
      filteredDailyUpdates = ObservableList<DailyUpdates>.of(objects
        ..sort(
            (DailyUpdates prevDailyUpdate, DailyUpdates presentDailyUpdate) =>
                presentDailyUpdate.date.compareTo(prevDailyUpdate.date)));
    } else
      dailyUpdates = ObservableList<DailyUpdates>.of(objects
        ..sort(
            (DailyUpdates prevDailyUpdate, DailyUpdates presentDailyUpdate) =>
                presentDailyUpdate.date.compareTo(prevDailyUpdate.date)));
    isFetching = false;
    isLoading = false;
  }

  @action
  void removeFilter() {
    isFilterApplied = false;
    hasMoreFilterUpdates = true;
    filteredDailyUpdates.clear();
  }

  @action
  void addDailyUpdate({@required String date, @required String description}) {
    isAdding = true;
    dailyUpdateService
        .postDailyUpdates(message: description, date: date)
        .then(addDailyUpdateComplete)
        .catchError((dynamic e) {
      isAdding = false;
    });
  }

  @action
  void addDailyUpdateComplete(DailyUpdates dailyUpdate) {
    isAdding = false;
    fetchData();
  }

  @action
  void stopFetch() {
    isLoading = false;
  }

  @action
  void sample() {
    isFetching = !isFetching;
  }

  @action
  void clearDailyUpdateStore() {
    dailyUpdates.clear();
    filteredDailyUpdates.clear();
    isLoading = true;
    isFetching = false;
    isAdding = false;
    hasMoreFilterUpdates = true;
    isFilterApplied = false;
    showFilter = false;
    startDate = null;
    user = null;
    endDate = null;
    hasMore = true;
  }
}
