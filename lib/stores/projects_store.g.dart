// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'projects_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$ProjectsStore on _ProjectsStore, Store {
  final _$projectsAtom = Atom(name: '_ProjectsStore.projects');

  @override
  ObservableList<Projects> get projects {
    _$projectsAtom.context.enforceReadPolicy(_$projectsAtom);
    _$projectsAtom.reportObserved();
    return super.projects;
  }

  @override
  set projects(ObservableList<Projects> value) {
    _$projectsAtom.context.conditionallyRunInAction(() {
      super.projects = value;
      _$projectsAtom.reportChanged();
    }, _$projectsAtom, name: '${_$projectsAtom.name}_set');
  }

  final _$isLoadingAtom = Atom(name: '_ProjectsStore.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.context.enforceReadPolicy(_$isLoadingAtom);
    _$isLoadingAtom.reportObserved();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.context.conditionallyRunInAction(() {
      super.isLoading = value;
      _$isLoadingAtom.reportChanged();
    }, _$isLoadingAtom, name: '${_$isLoadingAtom.name}_set');
  }

  final _$_ProjectsStoreActionController =
      ActionController(name: '_ProjectsStore');

  @override
  void fetchProjects() {
    final _$actionInfo = _$_ProjectsStoreActionController.startAction();
    try {
      return super.fetchProjects();
    } finally {
      _$_ProjectsStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void fetchDataComplete(List<Projects> objects) {
    final _$actionInfo = _$_ProjectsStoreActionController.startAction();
    try {
      return super.fetchDataComplete(objects);
    } finally {
      _$_ProjectsStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearProjectsStore() {
    final _$actionInfo = _$_ProjectsStoreActionController.startAction();
    try {
      return super.clearProjectsStore();
    } finally {
      _$_ProjectsStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void stopFetch() {
    final _$actionInfo = _$_ProjectsStoreActionController.startAction();
    try {
      return super.stopFetch();
    } finally {
      _$_ProjectsStoreActionController.endAction(_$actionInfo);
    }
  }
}
