import 'package:devslane_erp_flutter/models/holiday.dart';
import 'package:devslane_erp_flutter/utils/globals.dart';
import 'package:mobx/mobx.dart';

part 'holiday_store.g.dart';

class HolidayStore = _HolidayStore with _$HolidayStore;

abstract class _HolidayStore with Store {
  @observable
  ObservableMap<int, Holiday> holidays = ObservableMap<int, Holiday>();

  @observable
  bool isLoading = false;

  @observable
  bool hasMore = true;

  @computed
  List<Holiday> todayHoliday() {
    return holidays.values
        .where((Holiday holiday) =>
            DateTime.parse(holiday.date).month == DateTime.now().month &&
            DateTime.parse(holiday.date).day >= DateTime.now().day)
        .toList();
  }

  @computed
  List<Holiday> overallHoliday() {
    return holidays.values
        .where((Holiday holiday) =>
            DateTime.parse(holiday.date).month >= DateTime.now().month &&
            DateTime.parse(holiday.date).day >= DateTime.now().day)
        .toList();
  }

  @action
  void fetchData() {
    isLoading = true;
    holidayService
        .getHolidays()
        .then(fetchDataComplete)
        .catchError((dynamic e) {
      stopFetch();
    });
  }

  @action
  void fetchDataComplete(List<Holiday> objects) {
    hasMore = false;
    objects.sort((Holiday prevHoliday, Holiday presentHoliday) =>
        prevHoliday.date.compareTo(presentHoliday.date));
    holidays.addAll(objects.fold(
        {},
        (Map<int, Holiday> objectsMap, Holiday holiday) =>
            objectsMap..addAll({holiday.id: holiday})));
    isLoading = false;
  }

  @action
  void clearHolidayStore() {
    holidays.clear();
    isLoading = false;
    hasMore = true;
  }

  @action
  void stopFetch() {
    isLoading = false;
  }
}
