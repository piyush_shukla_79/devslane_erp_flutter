import 'package:devslane_erp_flutter/models/projects.dart';
import 'package:devslane_erp_flutter/utils/globals.dart';
import 'package:mobx/mobx.dart';

part 'projects_store.g.dart';

class ProjectsStore = _ProjectsStore with _$ProjectsStore;

abstract class _ProjectsStore with Store {
  @observable
  ObservableList<Projects> projects = ObservableList<Projects>();

  @observable
  bool isLoading = true;

  @action
  void fetchProjects() {
    getProjectsService
        .getProjects()
        .then(fetchDataComplete)
        .catchError((dynamic e) {
      stopFetch();
    });
  }

  @action
  void fetchDataComplete(List<Projects> objects) {
    projects.addAll(objects);
    isLoading = false;
  }

  @action
  void clearProjectsStore() {
    projects.clear();
    isLoading = true;
  }

  @action
  void stopFetch() {
    isLoading = false;
  }
}
