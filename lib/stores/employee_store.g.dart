// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'employee_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$EmployeeStore on _EmployeeStore, Store {
  Computed<Map<int, User>> _$getGroupedDailyUpdatesComputed;

  @override
  Map<int, User> get getGroupedDailyUpdates =>
      (_$getGroupedDailyUpdatesComputed ??=
              Computed<Map<int, User>>(() => super.getGroupedDailyUpdates))
          .value;

  final _$employeeListAtom = Atom(name: '_EmployeeStore.employeeList');

  @override
  ObservableList<User> get employeeList {
    _$employeeListAtom.context.enforceReadPolicy(_$employeeListAtom);
    _$employeeListAtom.reportObserved();
    return super.employeeList;
  }

  @override
  set employeeList(ObservableList<User> value) {
    _$employeeListAtom.context.conditionallyRunInAction(() {
      super.employeeList = value;
      _$employeeListAtom.reportChanged();
    }, _$employeeListAtom, name: '${_$employeeListAtom.name}_set');
  }

  final _$isLoadingAtom = Atom(name: '_EmployeeStore.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.context.enforceReadPolicy(_$isLoadingAtom);
    _$isLoadingAtom.reportObserved();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.context.conditionallyRunInAction(() {
      super.isLoading = value;
      _$isLoadingAtom.reportChanged();
    }, _$isLoadingAtom, name: '${_$isLoadingAtom.name}_set');
  }

  final _$_EmployeeStoreActionController =
      ActionController(name: '_EmployeeStore');

  @override
  void fetchData() {
    final _$actionInfo = _$_EmployeeStoreActionController.startAction();
    try {
      return super.fetchData();
    } finally {
      _$_EmployeeStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void fetchDataComplete(List<User> objects) {
    final _$actionInfo = _$_EmployeeStoreActionController.startAction();
    try {
      return super.fetchDataComplete(objects);
    } finally {
      _$_EmployeeStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearEmployeeStore() {
    final _$actionInfo = _$_EmployeeStoreActionController.startAction();
    try {
      return super.clearEmployeeStore();
    } finally {
      _$_EmployeeStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void stopFetch() {
    final _$actionInfo = _$_EmployeeStoreActionController.startAction();
    try {
      return super.stopFetch();
    } finally {
      _$_EmployeeStoreActionController.endAction(_$actionInfo);
    }
  }
}
