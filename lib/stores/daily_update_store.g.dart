// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'daily_update_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$DailyUpdateStore on _DailyUpdateStore, Store {
  Computed<Map<String, List<DailyUpdates>>> _$getGroupedDailyUpdatesComputed;

  @override
  Map<String, List<DailyUpdates>> get getGroupedDailyUpdates =>
      (_$getGroupedDailyUpdatesComputed ??=
              Computed<Map<String, List<DailyUpdates>>>(
                  () => super.getGroupedDailyUpdates))
          .value;

  final _$dailyUpdatesAtom = Atom(name: '_DailyUpdateStore.dailyUpdates');

  @override
  ObservableList<DailyUpdates> get dailyUpdates {
    _$dailyUpdatesAtom.context.enforceReadPolicy(_$dailyUpdatesAtom);
    _$dailyUpdatesAtom.reportObserved();
    return super.dailyUpdates;
  }

  @override
  set dailyUpdates(ObservableList<DailyUpdates> value) {
    _$dailyUpdatesAtom.context.conditionallyRunInAction(() {
      super.dailyUpdates = value;
      _$dailyUpdatesAtom.reportChanged();
    }, _$dailyUpdatesAtom, name: '${_$dailyUpdatesAtom.name}_set');
  }

  final _$filteredDailyUpdatesAtom =
      Atom(name: '_DailyUpdateStore.filteredDailyUpdates');

  @override
  ObservableList<DailyUpdates> get filteredDailyUpdates {
    _$filteredDailyUpdatesAtom.context
        .enforceReadPolicy(_$filteredDailyUpdatesAtom);
    _$filteredDailyUpdatesAtom.reportObserved();
    return super.filteredDailyUpdates;
  }

  @override
  set filteredDailyUpdates(ObservableList<DailyUpdates> value) {
    _$filteredDailyUpdatesAtom.context.conditionallyRunInAction(() {
      super.filteredDailyUpdates = value;
      _$filteredDailyUpdatesAtom.reportChanged();
    }, _$filteredDailyUpdatesAtom,
        name: '${_$filteredDailyUpdatesAtom.name}_set');
  }

  final _$isFilterAppliedAtom = Atom(name: '_DailyUpdateStore.isFilterApplied');

  @override
  bool get isFilterApplied {
    _$isFilterAppliedAtom.context.enforceReadPolicy(_$isFilterAppliedAtom);
    _$isFilterAppliedAtom.reportObserved();
    return super.isFilterApplied;
  }

  @override
  set isFilterApplied(bool value) {
    _$isFilterAppliedAtom.context.conditionallyRunInAction(() {
      super.isFilterApplied = value;
      _$isFilterAppliedAtom.reportChanged();
    }, _$isFilterAppliedAtom, name: '${_$isFilterAppliedAtom.name}_set');
  }

  final _$userAtom = Atom(name: '_DailyUpdateStore.user');

  @override
  User get user {
    _$userAtom.context.enforceReadPolicy(_$userAtom);
    _$userAtom.reportObserved();
    return super.user;
  }

  @override
  set user(User value) {
    _$userAtom.context.conditionallyRunInAction(() {
      super.user = value;
      _$userAtom.reportChanged();
    }, _$userAtom, name: '${_$userAtom.name}_set');
  }

  final _$isLoadingAtom = Atom(name: '_DailyUpdateStore.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.context.enforceReadPolicy(_$isLoadingAtom);
    _$isLoadingAtom.reportObserved();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.context.conditionallyRunInAction(() {
      super.isLoading = value;
      _$isLoadingAtom.reportChanged();
    }, _$isLoadingAtom, name: '${_$isLoadingAtom.name}_set');
  }

  final _$hasMoreAtom = Atom(name: '_DailyUpdateStore.hasMore');

  @override
  bool get hasMore {
    _$hasMoreAtom.context.enforceReadPolicy(_$hasMoreAtom);
    _$hasMoreAtom.reportObserved();
    return super.hasMore;
  }

  @override
  set hasMore(bool value) {
    _$hasMoreAtom.context.conditionallyRunInAction(() {
      super.hasMore = value;
      _$hasMoreAtom.reportChanged();
    }, _$hasMoreAtom, name: '${_$hasMoreAtom.name}_set');
  }

  final _$hasMoreFilterUpdatesAtom =
      Atom(name: '_DailyUpdateStore.hasMoreFilterUpdates');

  @override
  bool get hasMoreFilterUpdates {
    _$hasMoreFilterUpdatesAtom.context
        .enforceReadPolicy(_$hasMoreFilterUpdatesAtom);
    _$hasMoreFilterUpdatesAtom.reportObserved();
    return super.hasMoreFilterUpdates;
  }

  @override
  set hasMoreFilterUpdates(bool value) {
    _$hasMoreFilterUpdatesAtom.context.conditionallyRunInAction(() {
      super.hasMoreFilterUpdates = value;
      _$hasMoreFilterUpdatesAtom.reportChanged();
    }, _$hasMoreFilterUpdatesAtom,
        name: '${_$hasMoreFilterUpdatesAtom.name}_set');
  }

  final _$isFetchingAtom = Atom(name: '_DailyUpdateStore.isFetching');

  @override
  bool get isFetching {
    _$isFetchingAtom.context.enforceReadPolicy(_$isFetchingAtom);
    _$isFetchingAtom.reportObserved();
    return super.isFetching;
  }

  @override
  set isFetching(bool value) {
    _$isFetchingAtom.context.conditionallyRunInAction(() {
      super.isFetching = value;
      _$isFetchingAtom.reportChanged();
    }, _$isFetchingAtom, name: '${_$isFetchingAtom.name}_set');
  }

  final _$isAddingAtom = Atom(name: '_DailyUpdateStore.isAdding');

  @override
  bool get isAdding {
    _$isAddingAtom.context.enforceReadPolicy(_$isAddingAtom);
    _$isAddingAtom.reportObserved();
    return super.isAdding;
  }

  @override
  set isAdding(bool value) {
    _$isAddingAtom.context.conditionallyRunInAction(() {
      super.isAdding = value;
      _$isAddingAtom.reportChanged();
    }, _$isAddingAtom, name: '${_$isAddingAtom.name}_set');
  }

  final _$startDateAtom = Atom(name: '_DailyUpdateStore.startDate');

  @override
  String get startDate {
    _$startDateAtom.context.enforceReadPolicy(_$startDateAtom);
    _$startDateAtom.reportObserved();
    return super.startDate;
  }

  @override
  set startDate(String value) {
    _$startDateAtom.context.conditionallyRunInAction(() {
      super.startDate = value;
      _$startDateAtom.reportChanged();
    }, _$startDateAtom, name: '${_$startDateAtom.name}_set');
  }

  final _$showFilterAtom = Atom(name: '_DailyUpdateStore.showFilter');

  @override
  bool get showFilter {
    _$showFilterAtom.context.enforceReadPolicy(_$showFilterAtom);
    _$showFilterAtom.reportObserved();
    return super.showFilter;
  }

  @override
  set showFilter(bool value) {
    _$showFilterAtom.context.conditionallyRunInAction(() {
      super.showFilter = value;
      _$showFilterAtom.reportChanged();
    }, _$showFilterAtom, name: '${_$showFilterAtom.name}_set');
  }

  final _$endDateAtom = Atom(name: '_DailyUpdateStore.endDate');

  @override
  String get endDate {
    _$endDateAtom.context.enforceReadPolicy(_$endDateAtom);
    _$endDateAtom.reportObserved();
    return super.endDate;
  }

  @override
  set endDate(String value) {
    _$endDateAtom.context.conditionallyRunInAction(() {
      super.endDate = value;
      _$endDateAtom.reportChanged();
    }, _$endDateAtom, name: '${_$endDateAtom.name}_set');
  }

  final _$_DailyUpdateStoreActionController =
      ActionController(name: '_DailyUpdateStore');

  @override
  void showFilterFunction() {
    final _$actionInfo = _$_DailyUpdateStoreActionController.startAction();
    try {
      return super.showFilterFunction();
    } finally {
      _$_DailyUpdateStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void onChanged({String startDate, String endDate, User user}) {
    final _$actionInfo = _$_DailyUpdateStoreActionController.startAction();
    try {
      return super
          .onChanged(startDate: startDate, endDate: endDate, user: user);
    } finally {
      _$_DailyUpdateStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void deleteDailyUpdate(int index) {
    final _$actionInfo = _$_DailyUpdateStoreActionController.startAction();
    try {
      return super.deleteDailyUpdate(index);
    } finally {
      _$_DailyUpdateStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void fetchData({int length = 20}) {
    final _$actionInfo = _$_DailyUpdateStoreActionController.startAction();
    try {
      return super.fetchData(length: length);
    } finally {
      _$_DailyUpdateStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void fetchDataComplete(List<DailyUpdates> objects) {
    final _$actionInfo = _$_DailyUpdateStoreActionController.startAction();
    try {
      return super.fetchDataComplete(objects);
    } finally {
      _$_DailyUpdateStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void removeFilter() {
    final _$actionInfo = _$_DailyUpdateStoreActionController.startAction();
    try {
      return super.removeFilter();
    } finally {
      _$_DailyUpdateStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addDailyUpdate({String date, String description}) {
    final _$actionInfo = _$_DailyUpdateStoreActionController.startAction();
    try {
      return super.addDailyUpdate(date: date, description: description);
    } finally {
      _$_DailyUpdateStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addDailyUpdateComplete(DailyUpdates dailyUpdate) {
    final _$actionInfo = _$_DailyUpdateStoreActionController.startAction();
    try {
      return super.addDailyUpdateComplete(dailyUpdate);
    } finally {
      _$_DailyUpdateStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void stopFetch() {
    final _$actionInfo = _$_DailyUpdateStoreActionController.startAction();
    try {
      return super.stopFetch();
    } finally {
      _$_DailyUpdateStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void sample() {
    final _$actionInfo = _$_DailyUpdateStoreActionController.startAction();
    try {
      return super.sample();
    } finally {
      _$_DailyUpdateStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearDailyUpdateStore() {
    final _$actionInfo = _$_DailyUpdateStoreActionController.startAction();
    try {
      return super.clearDailyUpdateStore();
    } finally {
      _$_DailyUpdateStoreActionController.endAction(_$actionInfo);
    }
  }
}
