// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'calendar_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$CalendarStore on _CalendarStore, Store {
  final _$eventsAtom = Atom(name: '_CalendarStore.events');

  @override
  Events get events {
    _$eventsAtom.context.enforceReadPolicy(_$eventsAtom);
    _$eventsAtom.reportObserved();
    return super.events;
  }

  @override
  set events(Events value) {
    _$eventsAtom.context.conditionallyRunInAction(() {
      super.events = value;
      _$eventsAtom.reportChanged();
    }, _$eventsAtom, name: '${_$eventsAtom.name}_set');
  }

  final _$isLoadingAtom = Atom(name: '_CalendarStore.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.context.enforceReadPolicy(_$isLoadingAtom);
    _$isLoadingAtom.reportObserved();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.context.conditionallyRunInAction(() {
      super.isLoading = value;
      _$isLoadingAtom.reportChanged();
    }, _$isLoadingAtom, name: '${_$isLoadingAtom.name}_set');
  }

  final _$eventsMapAtom = Atom(name: '_CalendarStore.eventsMap');

  @override
  ObservableMap<String, Events> get eventsMap {
    _$eventsMapAtom.context.enforceReadPolicy(_$eventsMapAtom);
    _$eventsMapAtom.reportObserved();
    return super.eventsMap;
  }

  @override
  set eventsMap(ObservableMap<String, Events> value) {
    _$eventsMapAtom.context.conditionallyRunInAction(() {
      super.eventsMap = value;
      _$eventsMapAtom.reportChanged();
    }, _$eventsMapAtom, name: '${_$eventsMapAtom.name}_set');
  }

  final _$_CalendarStoreActionController =
      ActionController(name: '_CalendarStore');

  @override
  void fetchEventsData({DateTime month, int id}) {
    final _$actionInfo = _$_CalendarStoreActionController.startAction();
    try {
      return super.fetchEventsData(month: month, id: id);
    } finally {
      _$_CalendarStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void fetchEventsDataComplete(Events values, DateTime month, int id) {
    final _$actionInfo = _$_CalendarStoreActionController.startAction();
    try {
      return super.fetchEventsDataComplete(values, month, id);
    } finally {
      _$_CalendarStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearCalenderStore() {
    final _$actionInfo = _$_CalendarStoreActionController.startAction();
    try {
      return super.clearCalenderStore();
    } finally {
      _$_CalendarStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void stopFetch() {
    final _$actionInfo = _$_CalendarStoreActionController.startAction();
    try {
      return super.stopFetch();
    } finally {
      _$_CalendarStoreActionController.endAction(_$actionInfo);
    }
  }
}
