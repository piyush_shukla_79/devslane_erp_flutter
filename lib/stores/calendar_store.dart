import 'package:devslane_erp_flutter/models/events.dart';
import 'package:devslane_erp_flutter/utils/globals.dart';
import 'package:intl/intl.dart';
import 'package:mobx/mobx.dart';

part 'calendar_store.g.dart';

class CalendarStore = _CalendarStore with _$CalendarStore;

abstract class _CalendarStore with Store {
  @observable
  Events events = Events();

  @observable
  bool isLoading = false;

  @observable
  ObservableMap<String, Events> eventsMap = ObservableMap<String, Events>();

  @action
  void fetchEventsData({DateTime month, int id}) {
    isLoading = true;
    final DateFormat _dateFormat = DateFormat('yyyy-MM');

    calendarService
        .getCalendarEvents(month: _dateFormat.format(month).toString(), id: id)
        .then((Events values) {
      fetchEventsDataComplete(values, month, id);
    }).catchError((dynamic e) {
      stopFetch();
    });
  }

  @computed
  Events getAllEvents(DateTime month, int id) {
    return eventsMap[DateFormat('yyyy-MM').format(month)];
  }

  @action
  void fetchEventsDataComplete(Events values, DateTime month, int id) {
    eventsMap
        .addAll(<String, Events>{DateFormat('yyyy-MM').format(month): values});
    isLoading = false;
  }

  @action
  void clearCalenderStore() {
    events = Events();
    isLoading = false;
    eventsMap.clear();
  }

  @action
  void stopFetch() {
    isLoading = false;
  }
}
