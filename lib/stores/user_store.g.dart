// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$UserStore on _UserStore, Store {
  Computed<User> _$userDetailsComputed;

  @override
  User get userDetails =>
      (_$userDetailsComputed ??= Computed<User>(() => super.userDetails)).value;

  final _$isLoggedInAtom = Atom(name: '_UserStore.isLoggedIn');

  @override
  bool get isLoggedIn {
    _$isLoggedInAtom.context.enforceReadPolicy(_$isLoggedInAtom);
    _$isLoggedInAtom.reportObserved();
    return super.isLoggedIn;
  }

  @override
  set isLoggedIn(bool value) {
    _$isLoggedInAtom.context.conditionallyRunInAction(() {
      super.isLoggedIn = value;
      _$isLoggedInAtom.reportChanged();
    }, _$isLoggedInAtom, name: '${_$isLoggedInAtom.name}_set');
  }

  final _$isLoggingInAtom = Atom(name: '_UserStore.isLoggingIn');

  @override
  bool get isLoggingIn {
    _$isLoggingInAtom.context.enforceReadPolicy(_$isLoggingInAtom);
    _$isLoggingInAtom.reportObserved();
    return super.isLoggingIn;
  }

  @override
  set isLoggingIn(bool value) {
    _$isLoggingInAtom.context.conditionallyRunInAction(() {
      super.isLoggingIn = value;
      _$isLoggingInAtom.reportChanged();
    }, _$isLoggingInAtom, name: '${_$isLoggingInAtom.name}_set');
  }

  final _$isUpdatingAtom = Atom(name: '_UserStore.isUpdating');

  @override
  bool get isUpdating {
    _$isUpdatingAtom.context.enforceReadPolicy(_$isUpdatingAtom);
    _$isUpdatingAtom.reportObserved();
    return super.isUpdating;
  }

  @override
  set isUpdating(bool value) {
    _$isUpdatingAtom.context.conditionallyRunInAction(() {
      super.isUpdating = value;
      _$isUpdatingAtom.reportChanged();
    }, _$isUpdatingAtom, name: '${_$isUpdatingAtom.name}_set');
  }

  final _$isChangingPasswordAtom = Atom(name: '_UserStore.isChangingPassword');

  @override
  bool get isChangingPassword {
    _$isChangingPasswordAtom.context
        .enforceReadPolicy(_$isChangingPasswordAtom);
    _$isChangingPasswordAtom.reportObserved();
    return super.isChangingPassword;
  }

  @override
  set isChangingPassword(bool value) {
    _$isChangingPasswordAtom.context.conditionallyRunInAction(() {
      super.isChangingPassword = value;
      _$isChangingPasswordAtom.reportChanged();
    }, _$isChangingPasswordAtom, name: '${_$isChangingPasswordAtom.name}_set');
  }

  final _$isLoadingAtom = Atom(name: '_UserStore.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.context.enforceReadPolicy(_$isLoadingAtom);
    _$isLoadingAtom.reportObserved();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.context.conditionallyRunInAction(() {
      super.isLoading = value;
      _$isLoadingAtom.reportChanged();
    }, _$isLoadingAtom, name: '${_$isLoadingAtom.name}_set');
  }

  final _$loggedInUserAtom = Atom(name: '_UserStore.loggedInUser');

  @override
  User get loggedInUser {
    _$loggedInUserAtom.context.enforceReadPolicy(_$loggedInUserAtom);
    _$loggedInUserAtom.reportObserved();
    return super.loggedInUser;
  }

  @override
  set loggedInUser(User value) {
    _$loggedInUserAtom.context.conditionallyRunInAction(() {
      super.loggedInUser = value;
      _$loggedInUserAtom.reportChanged();
    }, _$loggedInUserAtom, name: '${_$loggedInUserAtom.name}_set');
  }

  final _$isErrorAtom = Atom(name: '_UserStore.isError');

  @override
  bool get isError {
    _$isErrorAtom.context.enforceReadPolicy(_$isErrorAtom);
    _$isErrorAtom.reportObserved();
    return super.isError;
  }

  @override
  set isError(bool value) {
    _$isErrorAtom.context.conditionallyRunInAction(() {
      super.isError = value;
      _$isErrorAtom.reportChanged();
    }, _$isErrorAtom, name: '${_$isErrorAtom.name}_set');
  }

  final _$errorDetailAtom = Atom(name: '_UserStore.errorDetail');

  @override
  String get errorDetail {
    _$errorDetailAtom.context.enforceReadPolicy(_$errorDetailAtom);
    _$errorDetailAtom.reportObserved();
    return super.errorDetail;
  }

  @override
  set errorDetail(String value) {
    _$errorDetailAtom.context.conditionallyRunInAction(() {
      super.errorDetail = value;
      _$errorDetailAtom.reportChanged();
    }, _$errorDetailAtom, name: '${_$errorDetailAtom.name}_set');
  }

  final _$_UserStoreActionController = ActionController(name: '_UserStore');

  @override
  void clearUserStore() {
    final _$actionInfo = _$_UserStoreActionController.startAction();
    try {
      return super.clearUserStore();
    } finally {
      _$_UserStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changePassword() {
    final _$actionInfo = _$_UserStoreActionController.startAction();
    try {
      return super.changePassword();
    } finally {
      _$_UserStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setIsLoggingIn({Map<String, dynamic> loginDetails}) {
    final _$actionInfo = _$_UserStoreActionController.startAction();
    try {
      return super.setIsLoggingIn(loginDetails: loginDetails);
    } finally {
      _$_UserStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void forgetPassword(String email) {
    final _$actionInfo = _$_UserStoreActionController.startAction();
    try {
      return super.forgetPassword(email);
    } finally {
      _$_UserStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void errorHandling(dynamic e) {
    final _$actionInfo = _$_UserStoreActionController.startAction();
    try {
      return super.errorHandling(e);
    } finally {
      _$_UserStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setLoggedIn(User user) {
    final _$actionInfo = _$_UserStoreActionController.startAction();
    try {
      return super.setLoggedIn(user);
    } finally {
      _$_UserStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setUserLoggedIn() {
    final _$actionInfo = _$_UserStoreActionController.startAction();
    try {
      return super.setUserLoggedIn();
    } finally {
      _$_UserStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void fetchUserDetails() {
    final _$actionInfo = _$_UserStoreActionController.startAction();
    try {
      return super.fetchUserDetails();
    } finally {
      _$_UserStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void updateUserDetails(Map<String, dynamic> user) {
    final _$actionInfo = _$_UserStoreActionController.startAction();
    try {
      return super.updateUserDetails(user);
    } finally {
      _$_UserStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void fetchUserDetailsComplete(dynamic user) {
    final _$actionInfo = _$_UserStoreActionController.startAction();
    try {
      return super.fetchUserDetailsComplete(user);
    } finally {
      _$_UserStoreActionController.endAction(_$actionInfo);
    }
  }
}
