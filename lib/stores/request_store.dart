import 'package:devslane_erp_flutter/models/request.dart';
import 'package:devslane_erp_flutter/models/user.dart';
import 'package:devslane_erp_flutter/utils/globals.dart';
import 'package:flutter/foundation.dart';
import 'package:mobx/mobx.dart';

part 'request_store.g.dart';

class RequestStore = _RequestStore with _$RequestStore;

abstract class _RequestStore with Store {
  @observable
  bool isLoading = false;

  @observable
  bool isMyRequestLoading = false;

  @observable
  bool isFetching = false;

  @observable
  bool hasMore = true;

  @observable
  bool isMyRequestFetching = false;

  @observable
  bool hasMoreMyRequest = true;

  @observable
  bool isCreatingRequest = false;

  @observable
  ObservableList<Request> requestList = ObservableList<Request>();

  @observable
  ObservableList<Request> approvedRequestList = ObservableList<Request>();

  @observable
  ObservableList<Request> pendingRequestList = ObservableList<Request>();

  @observable
  ObservableList<Request> myRequestList = ObservableList<Request>();

  @observable
  ObservableList<Request> myApprovedRequestList = ObservableList<Request>();

  @observable
  ObservableList<Request> myPendingRequestList = ObservableList<Request>();

  User adminUser;

  @observable
  ObservableMap<int, bool> processRequest = ObservableMap<int, bool>();

  @observable
  ObservableMap<int, bool> myProcessRequest = ObservableMap<int, bool>();

  @action
  void fetchData({int length = 20}) {
    isLoading = true;
    isFetching = true;
    if (adminUser == null) {
      preferencesService.getAuthUser().then((User user) {
        adminUser = user;
      });
    }
    requestService.getRequests(pageCount: length).then((data) {
      if (data.length <= requestList.length) {
        hasMore = false;
      }
      fetchDataComplete(data);
    }).catchError((dynamic e) {
      stopFetch();
    }).timeout(Duration(seconds: 10), onTimeout: () {
      isLoading = false;
      isFetching = false;
      isMyRequestLoading = false;
    });
  }

  @action
  void fetchMyData({int length = 20}) {
    isMyRequestLoading = true;
    isFetching = true;
    if (adminUser != null) {
      requestService
          .getRequests(pageCount: length, userId: adminUser.id.toString())
          .then((List<Request> data) {
        if (data.length <= myRequestList.length) {
          hasMoreMyRequest = false;
        }
        fetchMyDataComplete(data);
      });
    } else {
      preferencesService.getAuthUser().then((User user) {
        adminUser = user;
        requestService
            .getRequests(pageCount: length, userId: adminUser.id.toString())
            .then((List<Request> data) {
          if (data.length <= myRequestList.length) {
            hasMoreMyRequest = false;
          }
          fetchMyDataComplete(data);
        });
      });
    }
  }

  @action
  void fetchMyDataComplete(List<Request> data) {
    isMyRequestLoading = false;
    myRequestList = ObservableList<Request>.of(data);
    myPendingRequestList.clear();
    myApprovedRequestList.clear();
    myProcessRequest.clear();
    data.forEach((Request request) {
      if (request.actionPerformed) {
        myApprovedRequestList.add(request);
      } else {
        myPendingRequestList.add(request);
      }
      myProcessRequest.addAll({request.id: false});
    });
    isFetching = false;
  }

  @computed
  Map<int, Request> get getRequestMap {
    return requestList.fold(
        <int, Request>{},
        (Map<int, Request> requestMap, Request request) =>
            requestMap..addAll(<int, Request>{request.id: request}));
  }

  @action
  void acceptRequest({@required Request request}) {
    processRequest[request.id] = true;
    requestService.approve(request.id).then((Request updatedRequest) {
      pendingRequestList.remove(request);
      updatedRequest.data = request.data;
      approvedRequestList.add(updatedRequest);
      processRequest[request.id] = false;
      fetchData(length: requestList.length);
    }).catchError((dynamic e) {
      processRequest[request.id] = false;
    }).timeout(Duration(seconds: 10), onTimeout: () {
      processRequest[request.id] = false;
    });
  }

  @action
  void createRequest({
    String description,
    String endDate,
    String startDate,
    String type,
  }) {
    isCreatingRequest = true;
    requestService
        .createRequest(
            description: description,
            endDate: endDate,
            startDate: startDate,
            type: type)
        .then(createRequestComplete);
  }

  @action
  void createRequestComplete(Request request) {
    if (requestList.isEmpty) {
      fetchData();
    }
    requestList.add(request);
    myRequestList.add(request);
    myPendingRequestList.add(request);
    pendingRequestList.add(request);
    isCreatingRequest = false;
  }

  @action
  void rejectRequest({@required Request request, @required String note}) {
    processRequest[request.id] = true;
    requestService.reject(request.id, note).then((Request updatedRequest) {
      pendingRequestList.remove(request);
      updatedRequest.data = request.data;
      approvedRequestList.add(updatedRequest);
      processRequest[request.id] = false;
      fetchData(length: requestList.length);
    }).catchError((dynamic e) {
      processRequest[request.id] = false;
    }).timeout(Duration(seconds: 10), onTimeout: () {
      processRequest[request.id] = false;
    });
  }

  @action
  void fetchDataComplete(List<Request> data) {
    isLoading = false;
    requestList = ObservableList<Request>.of(data);
    pendingRequestList.clear();
    approvedRequestList.clear();
    processRequest.clear();

    data.forEach((Request request) {
      if (request.actionPerformed) {
        approvedRequestList.add(request);
      } else {
        pendingRequestList.add(request);
      }
      processRequest.addAll({request.id: false});
    });
    requestList.sort((Request prevRequest, Request presentRequest) =>
        presentRequest.createdAt.compareTo(prevRequest.createdAt));
    isFetching = false;
  }

  @action
  void clearRequestStore() {
    requestList.clear();
    pendingRequestList.clear();
    approvedRequestList.clear();
    processRequest.clear();
    adminUser = null;
    hasMore = true;
    isLoading = false;
    isMyRequestLoading = false;
    isFetching = false;
    isMyRequestFetching = false;
    hasMoreMyRequest = true;
    isCreatingRequest = false;
    myRequestList.clear();
    myApprovedRequestList.clear();
    myPendingRequestList.clear();
    myProcessRequest.clear();
  }

  @action
  void stopFetch() {
    isLoading = false;
  }
}
