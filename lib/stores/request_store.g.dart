// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$RequestStore on _RequestStore, Store {
  Computed<Map<int, Request>> _$getRequestMapComputed;

  @override
  Map<int, Request> get getRequestMap => (_$getRequestMapComputed ??=
          Computed<Map<int, Request>>(() => super.getRequestMap))
      .value;

  final _$isLoadingAtom = Atom(name: '_RequestStore.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.context.enforceReadPolicy(_$isLoadingAtom);
    _$isLoadingAtom.reportObserved();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.context.conditionallyRunInAction(() {
      super.isLoading = value;
      _$isLoadingAtom.reportChanged();
    }, _$isLoadingAtom, name: '${_$isLoadingAtom.name}_set');
  }

  final _$isMyRequestLoadingAtom =
      Atom(name: '_RequestStore.isMyRequestLoading');

  @override
  bool get isMyRequestLoading {
    _$isMyRequestLoadingAtom.context
        .enforceReadPolicy(_$isMyRequestLoadingAtom);
    _$isMyRequestLoadingAtom.reportObserved();
    return super.isMyRequestLoading;
  }

  @override
  set isMyRequestLoading(bool value) {
    _$isMyRequestLoadingAtom.context.conditionallyRunInAction(() {
      super.isMyRequestLoading = value;
      _$isMyRequestLoadingAtom.reportChanged();
    }, _$isMyRequestLoadingAtom, name: '${_$isMyRequestLoadingAtom.name}_set');
  }

  final _$isFetchingAtom = Atom(name: '_RequestStore.isFetching');

  @override
  bool get isFetching {
    _$isFetchingAtom.context.enforceReadPolicy(_$isFetchingAtom);
    _$isFetchingAtom.reportObserved();
    return super.isFetching;
  }

  @override
  set isFetching(bool value) {
    _$isFetchingAtom.context.conditionallyRunInAction(() {
      super.isFetching = value;
      _$isFetchingAtom.reportChanged();
    }, _$isFetchingAtom, name: '${_$isFetchingAtom.name}_set');
  }

  final _$hasMoreAtom = Atom(name: '_RequestStore.hasMore');

  @override
  bool get hasMore {
    _$hasMoreAtom.context.enforceReadPolicy(_$hasMoreAtom);
    _$hasMoreAtom.reportObserved();
    return super.hasMore;
  }

  @override
  set hasMore(bool value) {
    _$hasMoreAtom.context.conditionallyRunInAction(() {
      super.hasMore = value;
      _$hasMoreAtom.reportChanged();
    }, _$hasMoreAtom, name: '${_$hasMoreAtom.name}_set');
  }

  final _$isMyRequestFetchingAtom =
      Atom(name: '_RequestStore.isMyRequestFetching');

  @override
  bool get isMyRequestFetching {
    _$isMyRequestFetchingAtom.context
        .enforceReadPolicy(_$isMyRequestFetchingAtom);
    _$isMyRequestFetchingAtom.reportObserved();
    return super.isMyRequestFetching;
  }

  @override
  set isMyRequestFetching(bool value) {
    _$isMyRequestFetchingAtom.context.conditionallyRunInAction(() {
      super.isMyRequestFetching = value;
      _$isMyRequestFetchingAtom.reportChanged();
    }, _$isMyRequestFetchingAtom,
        name: '${_$isMyRequestFetchingAtom.name}_set');
  }

  final _$hasMoreMyRequestAtom = Atom(name: '_RequestStore.hasMoreMyRequest');

  @override
  bool get hasMoreMyRequest {
    _$hasMoreMyRequestAtom.context.enforceReadPolicy(_$hasMoreMyRequestAtom);
    _$hasMoreMyRequestAtom.reportObserved();
    return super.hasMoreMyRequest;
  }

  @override
  set hasMoreMyRequest(bool value) {
    _$hasMoreMyRequestAtom.context.conditionallyRunInAction(() {
      super.hasMoreMyRequest = value;
      _$hasMoreMyRequestAtom.reportChanged();
    }, _$hasMoreMyRequestAtom, name: '${_$hasMoreMyRequestAtom.name}_set');
  }

  final _$isCreatingRequestAtom = Atom(name: '_RequestStore.isCreatingRequest');

  @override
  bool get isCreatingRequest {
    _$isCreatingRequestAtom.context.enforceReadPolicy(_$isCreatingRequestAtom);
    _$isCreatingRequestAtom.reportObserved();
    return super.isCreatingRequest;
  }

  @override
  set isCreatingRequest(bool value) {
    _$isCreatingRequestAtom.context.conditionallyRunInAction(() {
      super.isCreatingRequest = value;
      _$isCreatingRequestAtom.reportChanged();
    }, _$isCreatingRequestAtom, name: '${_$isCreatingRequestAtom.name}_set');
  }

  final _$requestListAtom = Atom(name: '_RequestStore.requestList');

  @override
  ObservableList<Request> get requestList {
    _$requestListAtom.context.enforceReadPolicy(_$requestListAtom);
    _$requestListAtom.reportObserved();
    return super.requestList;
  }

  @override
  set requestList(ObservableList<Request> value) {
    _$requestListAtom.context.conditionallyRunInAction(() {
      super.requestList = value;
      _$requestListAtom.reportChanged();
    }, _$requestListAtom, name: '${_$requestListAtom.name}_set');
  }

  final _$approvedRequestListAtom =
      Atom(name: '_RequestStore.approvedRequestList');

  @override
  ObservableList<Request> get approvedRequestList {
    _$approvedRequestListAtom.context
        .enforceReadPolicy(_$approvedRequestListAtom);
    _$approvedRequestListAtom.reportObserved();
    return super.approvedRequestList;
  }

  @override
  set approvedRequestList(ObservableList<Request> value) {
    _$approvedRequestListAtom.context.conditionallyRunInAction(() {
      super.approvedRequestList = value;
      _$approvedRequestListAtom.reportChanged();
    }, _$approvedRequestListAtom,
        name: '${_$approvedRequestListAtom.name}_set');
  }

  final _$pendingRequestListAtom =
      Atom(name: '_RequestStore.pendingRequestList');

  @override
  ObservableList<Request> get pendingRequestList {
    _$pendingRequestListAtom.context
        .enforceReadPolicy(_$pendingRequestListAtom);
    _$pendingRequestListAtom.reportObserved();
    return super.pendingRequestList;
  }

  @override
  set pendingRequestList(ObservableList<Request> value) {
    _$pendingRequestListAtom.context.conditionallyRunInAction(() {
      super.pendingRequestList = value;
      _$pendingRequestListAtom.reportChanged();
    }, _$pendingRequestListAtom, name: '${_$pendingRequestListAtom.name}_set');
  }

  final _$myRequestListAtom = Atom(name: '_RequestStore.myRequestList');

  @override
  ObservableList<Request> get myRequestList {
    _$myRequestListAtom.context.enforceReadPolicy(_$myRequestListAtom);
    _$myRequestListAtom.reportObserved();
    return super.myRequestList;
  }

  @override
  set myRequestList(ObservableList<Request> value) {
    _$myRequestListAtom.context.conditionallyRunInAction(() {
      super.myRequestList = value;
      _$myRequestListAtom.reportChanged();
    }, _$myRequestListAtom, name: '${_$myRequestListAtom.name}_set');
  }

  final _$myApprovedRequestListAtom =
      Atom(name: '_RequestStore.myApprovedRequestList');

  @override
  ObservableList<Request> get myApprovedRequestList {
    _$myApprovedRequestListAtom.context
        .enforceReadPolicy(_$myApprovedRequestListAtom);
    _$myApprovedRequestListAtom.reportObserved();
    return super.myApprovedRequestList;
  }

  @override
  set myApprovedRequestList(ObservableList<Request> value) {
    _$myApprovedRequestListAtom.context.conditionallyRunInAction(() {
      super.myApprovedRequestList = value;
      _$myApprovedRequestListAtom.reportChanged();
    }, _$myApprovedRequestListAtom,
        name: '${_$myApprovedRequestListAtom.name}_set');
  }

  final _$myPendingRequestListAtom =
      Atom(name: '_RequestStore.myPendingRequestList');

  @override
  ObservableList<Request> get myPendingRequestList {
    _$myPendingRequestListAtom.context
        .enforceReadPolicy(_$myPendingRequestListAtom);
    _$myPendingRequestListAtom.reportObserved();
    return super.myPendingRequestList;
  }

  @override
  set myPendingRequestList(ObservableList<Request> value) {
    _$myPendingRequestListAtom.context.conditionallyRunInAction(() {
      super.myPendingRequestList = value;
      _$myPendingRequestListAtom.reportChanged();
    }, _$myPendingRequestListAtom,
        name: '${_$myPendingRequestListAtom.name}_set');
  }

  final _$processRequestAtom = Atom(name: '_RequestStore.processRequest');

  @override
  ObservableMap<int, bool> get processRequest {
    _$processRequestAtom.context.enforceReadPolicy(_$processRequestAtom);
    _$processRequestAtom.reportObserved();
    return super.processRequest;
  }

  @override
  set processRequest(ObservableMap<int, bool> value) {
    _$processRequestAtom.context.conditionallyRunInAction(() {
      super.processRequest = value;
      _$processRequestAtom.reportChanged();
    }, _$processRequestAtom, name: '${_$processRequestAtom.name}_set');
  }

  final _$myProcessRequestAtom = Atom(name: '_RequestStore.myProcessRequest');

  @override
  ObservableMap<int, bool> get myProcessRequest {
    _$myProcessRequestAtom.context.enforceReadPolicy(_$myProcessRequestAtom);
    _$myProcessRequestAtom.reportObserved();
    return super.myProcessRequest;
  }

  @override
  set myProcessRequest(ObservableMap<int, bool> value) {
    _$myProcessRequestAtom.context.conditionallyRunInAction(() {
      super.myProcessRequest = value;
      _$myProcessRequestAtom.reportChanged();
    }, _$myProcessRequestAtom, name: '${_$myProcessRequestAtom.name}_set');
  }

  final _$_RequestStoreActionController =
      ActionController(name: '_RequestStore');

  @override
  void fetchData({int length = 20}) {
    final _$actionInfo = _$_RequestStoreActionController.startAction();
    try {
      return super.fetchData(length: length);
    } finally {
      _$_RequestStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void fetchMyData({int length = 20}) {
    final _$actionInfo = _$_RequestStoreActionController.startAction();
    try {
      return super.fetchMyData(length: length);
    } finally {
      _$_RequestStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void fetchMyDataComplete(List<Request> data) {
    final _$actionInfo = _$_RequestStoreActionController.startAction();
    try {
      return super.fetchMyDataComplete(data);
    } finally {
      _$_RequestStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void acceptRequest({Request request}) {
    final _$actionInfo = _$_RequestStoreActionController.startAction();
    try {
      return super.acceptRequest(request: request);
    } finally {
      _$_RequestStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void createRequest(
      {String description, String endDate, String startDate, String type}) {
    final _$actionInfo = _$_RequestStoreActionController.startAction();
    try {
      return super.createRequest(
          description: description,
          endDate: endDate,
          startDate: startDate,
          type: type);
    } finally {
      _$_RequestStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void createRequestComplete(Request request) {
    final _$actionInfo = _$_RequestStoreActionController.startAction();
    try {
      return super.createRequestComplete(request);
    } finally {
      _$_RequestStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void rejectRequest({Request request, String note}) {
    final _$actionInfo = _$_RequestStoreActionController.startAction();
    try {
      return super.rejectRequest(request: request, note: note);
    } finally {
      _$_RequestStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void fetchDataComplete(List<Request> data) {
    final _$actionInfo = _$_RequestStoreActionController.startAction();
    try {
      return super.fetchDataComplete(data);
    } finally {
      _$_RequestStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearRequestStore() {
    final _$actionInfo = _$_RequestStoreActionController.startAction();
    try {
      return super.clearRequestStore();
    } finally {
      _$_RequestStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void stopFetch() {
    final _$actionInfo = _$_RequestStoreActionController.startAction();
    try {
      return super.stopFetch();
    } finally {
      _$_RequestStoreActionController.endAction(_$actionInfo);
    }
  }
}
