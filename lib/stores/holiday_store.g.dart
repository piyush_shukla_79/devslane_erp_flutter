// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'holiday_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$HolidayStore on _HolidayStore, Store {
  final _$holidaysAtom = Atom(name: '_HolidayStore.holidays');

  @override
  ObservableMap<int, Holiday> get holidays {
    _$holidaysAtom.context.enforceReadPolicy(_$holidaysAtom);
    _$holidaysAtom.reportObserved();
    return super.holidays;
  }

  @override
  set holidays(ObservableMap<int, Holiday> value) {
    _$holidaysAtom.context.conditionallyRunInAction(() {
      super.holidays = value;
      _$holidaysAtom.reportChanged();
    }, _$holidaysAtom, name: '${_$holidaysAtom.name}_set');
  }

  final _$isLoadingAtom = Atom(name: '_HolidayStore.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.context.enforceReadPolicy(_$isLoadingAtom);
    _$isLoadingAtom.reportObserved();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.context.conditionallyRunInAction(() {
      super.isLoading = value;
      _$isLoadingAtom.reportChanged();
    }, _$isLoadingAtom, name: '${_$isLoadingAtom.name}_set');
  }

  final _$hasMoreAtom = Atom(name: '_HolidayStore.hasMore');

  @override
  bool get hasMore {
    _$hasMoreAtom.context.enforceReadPolicy(_$hasMoreAtom);
    _$hasMoreAtom.reportObserved();
    return super.hasMore;
  }

  @override
  set hasMore(bool value) {
    _$hasMoreAtom.context.conditionallyRunInAction(() {
      super.hasMore = value;
      _$hasMoreAtom.reportChanged();
    }, _$hasMoreAtom, name: '${_$hasMoreAtom.name}_set');
  }

  final _$_HolidayStoreActionController =
      ActionController(name: '_HolidayStore');

  @override
  void fetchData() {
    final _$actionInfo = _$_HolidayStoreActionController.startAction();
    try {
      return super.fetchData();
    } finally {
      _$_HolidayStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void fetchDataComplete(List<Holiday> objects) {
    final _$actionInfo = _$_HolidayStoreActionController.startAction();
    try {
      return super.fetchDataComplete(objects);
    } finally {
      _$_HolidayStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearHolidayStore() {
    final _$actionInfo = _$_HolidayStoreActionController.startAction();
    try {
      return super.clearHolidayStore();
    } finally {
      _$_HolidayStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void stopFetch() {
    final _$actionInfo = _$_HolidayStoreActionController.startAction();
    try {
      return super.stopFetch();
    } finally {
      _$_HolidayStoreActionController.endAction(_$actionInfo);
    }
  }
}
