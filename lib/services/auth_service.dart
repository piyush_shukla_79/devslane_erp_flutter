import 'package:devslane_erp_flutter/models/login_response.dart';

import 'api_service.dart';

class AuthService extends APIService {
  AuthService._();

  factory AuthService.getInstance() => _instance;
  static final AuthService _instance = AuthService._();

  Future<LoginResponse> login({String email, String password}) async {
    final Map<String, String> body = {'email': email, 'password': password};
    final Map<String, dynamic> response = await post(
      '/authenticate',
      body: body,
      useAuthHeaders: true,
    );
    return LoginResponse.fromJson(response);
  }

  Future<Null> forgetPassword({String email}) async {
    final Map<String, String> body = {'email': email};
    await post(
      '/forgot-password',
      body: body,
      useAuthHeaders: true,
    );
    return;
  }
}
