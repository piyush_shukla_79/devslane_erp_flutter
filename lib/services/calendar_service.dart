import 'package:devslane_erp_flutter/models/events.dart';

import 'api_service.dart';

class CalendarService extends APIService {
  CalendarService._();

  factory CalendarService.getInstance() => _instance;
  static final CalendarService _instance = CalendarService._();

  Future<Events> getCalendarEvents({int id, String month}) async {
    final Map<String, String> body = {
      'user_id': id.toString(),
      'month': month,
      'include': 'user,approvedBy,rejectedBy',
    };
    final Map<String, dynamic> response = await get(
      '/calendars',
      params: body,
      useAuthHeaders: true,
    );
    if (response['data'].isEmpty) {
      response.remove('data');
    }
    return Events.fromJson(response);
  }
}
