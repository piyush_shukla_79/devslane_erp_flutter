import 'dart:convert';

import 'package:devslane_erp_flutter/models/user.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferencesService {
  PreferencesService._();

  factory PreferencesService.getInstance() => _instance;

  static final PreferencesService _instance = PreferencesService._();

  static const String AUTH_TOKEN = 'auth_token';
  static const String LOGGED_IN_USER = 'logged_in_user';
  static const String FIRST_LAUNCH = 'first_launch';

  BuildContext _context;

  void setGlobalContext(BuildContext context) {
    _context = context;
  }

  TargetPlatform getTargetPlatform() {
    return Theme.of(_context).platform;
  }

  Future<SharedPreferences> _getInstance() async {
    return SharedPreferences.getInstance();
  }

  Future<void> setAuthToken(String token) async {
    (await _getInstance()).setString(PreferencesService.AUTH_TOKEN, token);
  }

  Future<void> setAuthUser(User user) async {
    if (user != null) {
      (await _getInstance()).setString(
          PreferencesService.LOGGED_IN_USER, json.encode(User.toJson(user)));
    } else {
      (await _getInstance())
          .setString(PreferencesService.LOGGED_IN_USER, user.toString());
    }
  }

  Future<User> getAuthUser() async {
    final String user =
        (await _getInstance()).getString(PreferencesService.LOGGED_IN_USER);
    if (user == null) {
      return null;
    }
    return User.fromJson(json.decode(user));
  }

  Future<String> getAuthToken() async {
    return (await _getInstance()).getString(PreferencesService.AUTH_TOKEN);
  }

  Future<bool> getIsFirstLaunch() async {
    return (await _getInstance()).getBool(PreferencesService.FIRST_LAUNCH);
  }

  Future<void> setIsFirstLaunch(bool val) async {
    (await _getInstance()).setBool(PreferencesService.FIRST_LAUNCH, val);
  }
}
