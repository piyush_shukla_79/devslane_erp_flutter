import 'package:devslane_erp_flutter/models/holiday.dart';
import 'package:devslane_erp_flutter/models/request.dart';

import 'api_service.dart';

class HolidayService extends APIService {
  HolidayService._();

  factory HolidayService.getInstance() => _instance;
  static final HolidayService _instance = HolidayService._();

  Future<List<Holiday>> getHolidays() async {
    final Map<String, dynamic> response = await get(
      '/holidays',
      params: null,
      useAuthHeaders: true,
    );
    return (response['data'] as List<dynamic>)
        .map((dynamic o) => Holiday.fromJson(o))
        .toList();
  }
}
