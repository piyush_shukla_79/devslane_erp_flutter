import 'package:devslane_erp_flutter/models/daily_update.dart';

import 'api_service.dart';

class DailyUpdateService extends APIService {
  DailyUpdateService._();

  factory DailyUpdateService.getInstance() => _instance;
  static final DailyUpdateService _instance = DailyUpdateService._();

  Future<List<DailyUpdates>> getDailyUpdates({
    String sortOrder = 'desc',
    String sortBy = 'created_at',
    int pageCount = 10,
    String userId,
  }) async {
    final Map<String, String> body = {
      'sort_order': sortOrder,
      'sort_by': sortBy,
      'per_page': pageCount.toString(),
      'user_ids': userId.toString(),
    };
    body.removeWhere(
        (String key, String value) => value == null || value == 'null');
    final Map<String, dynamic> response = await get(
      '/daily-updates',
      params: body,
      useAuthHeaders: true,
    );
    // ignore: avoid_as
    return (response['data'] as List<dynamic>)
        .map((dynamic o) => DailyUpdates.fromJson(o))
        .toList();
  }

  Future<DailyUpdates> postDailyUpdates({String message, String date}) async {
    final Map<String, String> body = {'message': message, 'date': date};

    final Map<String, dynamic> response =
        await post('/daily-updates', body: body);
    return DailyUpdates.fromJson(response['data']);
  }
}
