import 'package:devslane_erp_flutter/models/request.dart';

import 'api_service.dart';

class RequestService extends APIService {
  RequestService._();

  factory RequestService.getInstance() => _instance;
  static final RequestService _instance = RequestService._();

  Future<List<Request>> getRequests(
      {String sortOrder = 'desc',
      String sortBy = 'created_at',
      int pageCount = 20,
      String userId}) async {
    final Map<String, String> body = <String, String>{
      'sort_order': sortOrder,
      'sort_by': sortBy,
      'per_page': pageCount.toString(),
      'include': 'user,approvedBy,rejectedBy',
      'user_ids': userId
    };
    body.removeWhere(
        (String key, String value) => value == null || value == 'null');
    final Map<String, dynamic> response = await get(
      '/requests',
      params: body,
      useAuthHeaders: true,
    );
    return (response['data'] as List<dynamic>)
        .map((dynamic o) => Request.fromJson(o))
        .toList();
  }

  Future<Request> createRequest(
      {String startDate,
      String endDate,
      String description,
      String type}) async {
    final Map<String, String> body = <String, String>{
      'start_date': startDate,
      'description': description,
      'type': type,
      'include': 'user,approvedBy,rejectedBy',
    };
    final Map<String, dynamic> response = await post('/requests', body: body);
    return Request.fromJson(response['data']);
  }

  Future<Request> approve(int requestId) async {
    final Map<String, dynamic> response =
        await put('/requests/$requestId/approve', body: null);
    return Request.fromJson(response['data']);
  }

  Future<Request> reject(int requestId, String note) async {
    Map<String, String> body = {'note': note};
    final Map<String, dynamic> response =
        await put('/requests/$requestId/reject', body: body);
    return Request.fromJson(response['data']);
  }
}
