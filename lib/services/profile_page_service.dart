import '../models/user.dart';
import 'api_service.dart';

class ProfilePageService extends APIService {
  ProfilePageService._();

  factory ProfilePageService.getInstance() => _instance;
  static final ProfilePageService _instance = ProfilePageService._();

  Future<User> getProfileDetails({
    String sortOrder = 'desc',
    String sortBy = 'created_at',
    int pageCount = 2,
  }) async {
    final Map<String, String> body = <String, String>{
      'sort_order': sortOrder,
      'sort_by': sortBy,
      'per_page': pageCount.toString()
    };
    final Map<String, dynamic> response =
        await get('/me', params: body, useAuthHeaders: true);
    return User.fromJson(response['data']);
  }

  Future<User> updateUserProfile({Map<String, dynamic> user}) async {
    final Map<String, String> body = <String, String>{
      'first_name': user['first_name'],
      'last_name': user['last_name'],
      'phone': user['phone'],
      'gender': user['gender'],
      'address': user['address'],
    };
    body.removeWhere(
        (String key, String value) => value == null || value == "null");
    final Map<String, dynamic> response = await post('/me', body: body);
    return User.fromJson(response['data']);
  }
}
