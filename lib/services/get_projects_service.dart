import 'package:devslane_erp_flutter/models/projects.dart';

import 'api_service.dart';

class GetProjectsService extends APIService {
  GetProjectsService._();

  factory GetProjectsService.getInstance() => _instance;
  static final GetProjectsService _instance = GetProjectsService._();

  Future<List<Projects>> getProjects(
      {String sortOrder = 'desc',
      String sortBy = 'created_at',
      int pageCount = 50}) async {
    final Map<String, String> body = {
      'sort_order': sortOrder,
      'sort_by': sortBy,
      'per_page': pageCount.toString(),
      'include': 'tags',
    };
    final Map<String, dynamic> response = await get(
      '/projects',
      params: body,
      useAuthHeaders: true,
    );
    return (response['data'] as List<dynamic>)
        .map((dynamic o) => Projects.fromJson(o))
        .toList();
  }
}
