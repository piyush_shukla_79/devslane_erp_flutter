import 'package:devslane_erp_flutter/models/user.dart';

import 'api_service.dart';

class EmployeeService extends APIService {
  EmployeeService._();

  factory EmployeeService.getInstance() => _instance;
  static final EmployeeService _instance = EmployeeService._();

  Future<List<User>> getEmployeeDetails(
      {String sortOrder = 'desc',
      String sortBy = 'created_at',
      int pageCount = 100}) async {
    final Map<String, String> body = {
      'sort_order': sortOrder,
      'sort_by': sortBy,
      'per_page': pageCount.toString(),
      'include': 'tags',
    };
    final Map<String, dynamic> response = await get(
      '/employees',
      params: body,
      useAuthHeaders: true,
    );
    return (response['data'] as List<dynamic>)
        .map((dynamic o) => User.fromJson(o))
        .toList();
  }
}
