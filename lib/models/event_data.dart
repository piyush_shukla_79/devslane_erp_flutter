import 'package:devslane_erp_flutter/models/request.dart';
import 'package:devslane_erp_flutter/models/user.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:mobx/mobx.dart';

part 'event_data.g.dart';

@JsonSerializable()
class EventData extends _EventData with _$EventData {
  static EventData fromJson(Map<String, dynamic> json) =>
      _$EventDataFromJson(json);

  static Map<String, dynamic> toJson(EventData eventData) =>
      _$EventDataToJson(eventData);
}

abstract class _EventData with Store {
  @observable
  String type;

  @observable
  User user;

  @observable
  @JsonKey(name: 'meta')
  Request request;
}
