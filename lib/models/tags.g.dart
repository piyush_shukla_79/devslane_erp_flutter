// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tags.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Tags _$TagsFromJson(Map<String, dynamic> json) {
  return Tags()
    ..id = json['id'] as int
    ..slug = json['slug'] as String
    ..title = json['title'] as String;
}

Map<String, dynamic> _$TagsToJson(Tags instance) => <String, dynamic>{
      'id': instance.id,
      'slug': instance.slug,
      'title': instance.title,
    };

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$Tags on _Tags, Store {
  final _$idAtom = Atom(name: '_Tags.id');

  @override
  int get id {
    _$idAtom.context.enforceReadPolicy(_$idAtom);
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(int value) {
    _$idAtom.context.conditionallyRunInAction(() {
      super.id = value;
      _$idAtom.reportChanged();
    }, _$idAtom, name: '${_$idAtom.name}_set');
  }

  final _$slugAtom = Atom(name: '_Tags.slug');

  @override
  String get slug {
    _$slugAtom.context.enforceReadPolicy(_$slugAtom);
    _$slugAtom.reportObserved();
    return super.slug;
  }

  @override
  set slug(String value) {
    _$slugAtom.context.conditionallyRunInAction(() {
      super.slug = value;
      _$slugAtom.reportChanged();
    }, _$slugAtom, name: '${_$slugAtom.name}_set');
  }

  final _$titleAtom = Atom(name: '_Tags.title');

  @override
  String get title {
    _$titleAtom.context.enforceReadPolicy(_$titleAtom);
    _$titleAtom.reportObserved();
    return super.title;
  }

  @override
  set title(String value) {
    _$titleAtom.context.conditionallyRunInAction(() {
      super.title = value;
      _$titleAtom.reportChanged();
    }, _$titleAtom, name: '${_$titleAtom.name}_set');
  }
}
