// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'events.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Events _$EventsFromJson(Map<String, dynamic> json) {
  return Events()
    ..dateMap = (json['data'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(
          k,
          (e as List)
              ?.map((e) => e == null
                  ? null
                  : EventData.fromJson(e as Map<String, dynamic>))
              ?.toList()),
    )
    ..leaves = (json['total_leaves_this_month'] as num)?.toDouble()
    ..holidays = (json['total_holidays_this_month'] as num)?.toDouble();
}

Map<String, dynamic> _$EventsToJson(Events instance) => <String, dynamic>{
      'data': instance.dateMap,
      'total_leaves_this_month': instance.leaves,
      'total_holidays_this_month': instance.holidays,
    };

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$Events on _Events, Store {
  final _$dateMapAtom = Atom(name: '_Events.dateMap');

  @override
  Map<String, List<EventData>> get dateMap {
    _$dateMapAtom.context.enforceReadPolicy(_$dateMapAtom);
    _$dateMapAtom.reportObserved();
    return super.dateMap;
  }

  @override
  set dateMap(Map<String, List<EventData>> value) {
    _$dateMapAtom.context.conditionallyRunInAction(() {
      super.dateMap = value;
      _$dateMapAtom.reportChanged();
    }, _$dateMapAtom, name: '${_$dateMapAtom.name}_set');
  }

  final _$leavesAtom = Atom(name: '_Events.leaves');

  @override
  double get leaves {
    _$leavesAtom.context.enforceReadPolicy(_$leavesAtom);
    _$leavesAtom.reportObserved();
    return super.leaves;
  }

  @override
  set leaves(double value) {
    _$leavesAtom.context.conditionallyRunInAction(() {
      super.leaves = value;
      _$leavesAtom.reportChanged();
    }, _$leavesAtom, name: '${_$leavesAtom.name}_set');
  }

  final _$holidaysAtom = Atom(name: '_Events.holidays');

  @override
  double get holidays {
    _$holidaysAtom.context.enforceReadPolicy(_$holidaysAtom);
    _$holidaysAtom.reportObserved();
    return super.holidays;
  }

  @override
  set holidays(double value) {
    _$holidaysAtom.context.conditionallyRunInAction(() {
      super.holidays = value;
      _$holidaysAtom.reportChanged();
    }, _$holidaysAtom, name: '${_$holidaysAtom.name}_set');
  }
}
