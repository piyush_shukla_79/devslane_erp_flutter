// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User()
    ..id = json['id'] as int
    ..gender = json['gender'] as String
    ..firstName = json['first_name'] as String
    ..lastName = json['last_name'] as String
    ..role = json['role'] as String
    ..organizationId = json['organization_id'] as int
    ..email = json['email'] as String
    ..phone = json['phone'] as String
    ..image = json['image'] as String
    ..address = json['address'] as String
    ..leavesLeft = (json['leaves_left'] as num)?.toDouble()
    ..joiningDate = json['joining_date'] as String
    ..leavesResetAt = json['leaves_reset_at'] as String
    ..extraDetails = json['extra_details'] as String
    ..enrollmentId = json['enrollment_id'] as int
    ..status = json['status'] as String
    ..shouldPostDailyUpdates = json['should_post_daily_updates'] as bool
    ..createdAt = json['created_at'] as String
    ..updatedAt = json['updated_at'] as String
    ..data = json['tags'] as Map<String, dynamic>;
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'gender': instance.gender,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'role': instance.role,
      'organization_id': instance.organizationId,
      'email': instance.email,
      'phone': instance.phone,
      'image': instance.image,
      'address': instance.address,
      'leaves_left': instance.leavesLeft,
      'joining_date': instance.joiningDate,
      'leaves_reset_at': instance.leavesResetAt,
      'extra_details': instance.extraDetails,
      'enrollment_id': instance.enrollmentId,
      'status': instance.status,
      'should_post_daily_updates': instance.shouldPostDailyUpdates,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
      'tags': instance.data,
    };

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$User on _User, Store {
  Computed<String> _$fullNameComputed;

  @override
  String get fullName =>
      (_$fullNameComputed ??= Computed<String>(() => super.fullName)).value;

  final _$idAtom = Atom(name: '_User.id');

  @override
  int get id {
    _$idAtom.context.enforceReadPolicy(_$idAtom);
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(int value) {
    _$idAtom.context.conditionallyRunInAction(() {
      super.id = value;
      _$idAtom.reportChanged();
    }, _$idAtom, name: '${_$idAtom.name}_set');
  }

  final _$genderAtom = Atom(name: '_User.gender');

  @override
  String get gender {
    _$genderAtom.context.enforceReadPolicy(_$genderAtom);
    _$genderAtom.reportObserved();
    return super.gender;
  }

  @override
  set gender(String value) {
    _$genderAtom.context.conditionallyRunInAction(() {
      super.gender = value;
      _$genderAtom.reportChanged();
    }, _$genderAtom, name: '${_$genderAtom.name}_set');
  }

  final _$firstNameAtom = Atom(name: '_User.firstName');

  @override
  String get firstName {
    _$firstNameAtom.context.enforceReadPolicy(_$firstNameAtom);
    _$firstNameAtom.reportObserved();
    return super.firstName;
  }

  @override
  set firstName(String value) {
    _$firstNameAtom.context.conditionallyRunInAction(() {
      super.firstName = value;
      _$firstNameAtom.reportChanged();
    }, _$firstNameAtom, name: '${_$firstNameAtom.name}_set');
  }

  final _$lastNameAtom = Atom(name: '_User.lastName');

  @override
  String get lastName {
    _$lastNameAtom.context.enforceReadPolicy(_$lastNameAtom);
    _$lastNameAtom.reportObserved();
    return super.lastName;
  }

  @override
  set lastName(String value) {
    _$lastNameAtom.context.conditionallyRunInAction(() {
      super.lastName = value;
      _$lastNameAtom.reportChanged();
    }, _$lastNameAtom, name: '${_$lastNameAtom.name}_set');
  }

  final _$roleAtom = Atom(name: '_User.role');

  @override
  String get role {
    _$roleAtom.context.enforceReadPolicy(_$roleAtom);
    _$roleAtom.reportObserved();
    return super.role;
  }

  @override
  set role(String value) {
    _$roleAtom.context.conditionallyRunInAction(() {
      super.role = value;
      _$roleAtom.reportChanged();
    }, _$roleAtom, name: '${_$roleAtom.name}_set');
  }

  final _$organizationIdAtom = Atom(name: '_User.organizationId');

  @override
  int get organizationId {
    _$organizationIdAtom.context.enforceReadPolicy(_$organizationIdAtom);
    _$organizationIdAtom.reportObserved();
    return super.organizationId;
  }

  @override
  set organizationId(int value) {
    _$organizationIdAtom.context.conditionallyRunInAction(() {
      super.organizationId = value;
      _$organizationIdAtom.reportChanged();
    }, _$organizationIdAtom, name: '${_$organizationIdAtom.name}_set');
  }

  final _$emailAtom = Atom(name: '_User.email');

  @override
  String get email {
    _$emailAtom.context.enforceReadPolicy(_$emailAtom);
    _$emailAtom.reportObserved();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.context.conditionallyRunInAction(() {
      super.email = value;
      _$emailAtom.reportChanged();
    }, _$emailAtom, name: '${_$emailAtom.name}_set');
  }

  final _$phoneAtom = Atom(name: '_User.phone');

  @override
  String get phone {
    _$phoneAtom.context.enforceReadPolicy(_$phoneAtom);
    _$phoneAtom.reportObserved();
    return super.phone;
  }

  @override
  set phone(String value) {
    _$phoneAtom.context.conditionallyRunInAction(() {
      super.phone = value;
      _$phoneAtom.reportChanged();
    }, _$phoneAtom, name: '${_$phoneAtom.name}_set');
  }

  final _$imageAtom = Atom(name: '_User.image');

  @override
  String get image {
    _$imageAtom.context.enforceReadPolicy(_$imageAtom);
    _$imageAtom.reportObserved();
    return super.image;
  }

  @override
  set image(String value) {
    _$imageAtom.context.conditionallyRunInAction(() {
      super.image = value;
      _$imageAtom.reportChanged();
    }, _$imageAtom, name: '${_$imageAtom.name}_set');
  }

  final _$addressAtom = Atom(name: '_User.address');

  @override
  String get address {
    _$addressAtom.context.enforceReadPolicy(_$addressAtom);
    _$addressAtom.reportObserved();
    return super.address;
  }

  @override
  set address(String value) {
    _$addressAtom.context.conditionallyRunInAction(() {
      super.address = value;
      _$addressAtom.reportChanged();
    }, _$addressAtom, name: '${_$addressAtom.name}_set');
  }

  final _$leavesLeftAtom = Atom(name: '_User.leavesLeft');

  @override
  double get leavesLeft {
    _$leavesLeftAtom.context.enforceReadPolicy(_$leavesLeftAtom);
    _$leavesLeftAtom.reportObserved();
    return super.leavesLeft;
  }

  @override
  set leavesLeft(double value) {
    _$leavesLeftAtom.context.conditionallyRunInAction(() {
      super.leavesLeft = value;
      _$leavesLeftAtom.reportChanged();
    }, _$leavesLeftAtom, name: '${_$leavesLeftAtom.name}_set');
  }

  final _$joiningDateAtom = Atom(name: '_User.joiningDate');

  @override
  String get joiningDate {
    _$joiningDateAtom.context.enforceReadPolicy(_$joiningDateAtom);
    _$joiningDateAtom.reportObserved();
    return super.joiningDate;
  }

  @override
  set joiningDate(String value) {
    _$joiningDateAtom.context.conditionallyRunInAction(() {
      super.joiningDate = value;
      _$joiningDateAtom.reportChanged();
    }, _$joiningDateAtom, name: '${_$joiningDateAtom.name}_set');
  }

  final _$leavesResetAtAtom = Atom(name: '_User.leavesResetAt');

  @override
  String get leavesResetAt {
    _$leavesResetAtAtom.context.enforceReadPolicy(_$leavesResetAtAtom);
    _$leavesResetAtAtom.reportObserved();
    return super.leavesResetAt;
  }

  @override
  set leavesResetAt(String value) {
    _$leavesResetAtAtom.context.conditionallyRunInAction(() {
      super.leavesResetAt = value;
      _$leavesResetAtAtom.reportChanged();
    }, _$leavesResetAtAtom, name: '${_$leavesResetAtAtom.name}_set');
  }

  final _$extraDetailsAtom = Atom(name: '_User.extraDetails');

  @override
  String get extraDetails {
    _$extraDetailsAtom.context.enforceReadPolicy(_$extraDetailsAtom);
    _$extraDetailsAtom.reportObserved();
    return super.extraDetails;
  }

  @override
  set extraDetails(String value) {
    _$extraDetailsAtom.context.conditionallyRunInAction(() {
      super.extraDetails = value;
      _$extraDetailsAtom.reportChanged();
    }, _$extraDetailsAtom, name: '${_$extraDetailsAtom.name}_set');
  }

  final _$enrollmentIdAtom = Atom(name: '_User.enrollmentId');

  @override
  int get enrollmentId {
    _$enrollmentIdAtom.context.enforceReadPolicy(_$enrollmentIdAtom);
    _$enrollmentIdAtom.reportObserved();
    return super.enrollmentId;
  }

  @override
  set enrollmentId(int value) {
    _$enrollmentIdAtom.context.conditionallyRunInAction(() {
      super.enrollmentId = value;
      _$enrollmentIdAtom.reportChanged();
    }, _$enrollmentIdAtom, name: '${_$enrollmentIdAtom.name}_set');
  }

  final _$statusAtom = Atom(name: '_User.status');

  @override
  String get status {
    _$statusAtom.context.enforceReadPolicy(_$statusAtom);
    _$statusAtom.reportObserved();
    return super.status;
  }

  @override
  set status(String value) {
    _$statusAtom.context.conditionallyRunInAction(() {
      super.status = value;
      _$statusAtom.reportChanged();
    }, _$statusAtom, name: '${_$statusAtom.name}_set');
  }

  final _$shouldPostDailyUpdatesAtom =
      Atom(name: '_User.shouldPostDailyUpdates');

  @override
  bool get shouldPostDailyUpdates {
    _$shouldPostDailyUpdatesAtom.context
        .enforceReadPolicy(_$shouldPostDailyUpdatesAtom);
    _$shouldPostDailyUpdatesAtom.reportObserved();
    return super.shouldPostDailyUpdates;
  }

  @override
  set shouldPostDailyUpdates(bool value) {
    _$shouldPostDailyUpdatesAtom.context.conditionallyRunInAction(() {
      super.shouldPostDailyUpdates = value;
      _$shouldPostDailyUpdatesAtom.reportChanged();
    }, _$shouldPostDailyUpdatesAtom,
        name: '${_$shouldPostDailyUpdatesAtom.name}_set');
  }

  final _$createdAtAtom = Atom(name: '_User.createdAt');

  @override
  String get createdAt {
    _$createdAtAtom.context.enforceReadPolicy(_$createdAtAtom);
    _$createdAtAtom.reportObserved();
    return super.createdAt;
  }

  @override
  set createdAt(String value) {
    _$createdAtAtom.context.conditionallyRunInAction(() {
      super.createdAt = value;
      _$createdAtAtom.reportChanged();
    }, _$createdAtAtom, name: '${_$createdAtAtom.name}_set');
  }

  final _$updatedAtAtom = Atom(name: '_User.updatedAt');

  @override
  String get updatedAt {
    _$updatedAtAtom.context.enforceReadPolicy(_$updatedAtAtom);
    _$updatedAtAtom.reportObserved();
    return super.updatedAt;
  }

  @override
  set updatedAt(String value) {
    _$updatedAtAtom.context.conditionallyRunInAction(() {
      super.updatedAt = value;
      _$updatedAtAtom.reportChanged();
    }, _$updatedAtAtom, name: '${_$updatedAtAtom.name}_set');
  }

  final _$dataAtom = Atom(name: '_User.data');

  @override
  Map<String, dynamic> get data {
    _$dataAtom.context.enforceReadPolicy(_$dataAtom);
    _$dataAtom.reportObserved();
    return super.data;
  }

  @override
  set data(Map<String, dynamic> value) {
    _$dataAtom.context.conditionallyRunInAction(() {
      super.data = value;
      _$dataAtom.reportChanged();
    }, _$dataAtom, name: '${_$dataAtom.name}_set');
  }
}
