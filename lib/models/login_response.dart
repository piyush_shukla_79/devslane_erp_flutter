import 'package:devslane_erp_flutter/models/user.dart';
import 'package:json_annotation/json_annotation.dart';

part 'login_response.g.dart';

@JsonSerializable()
class LoginResponse {
  String token;

  @JsonKey(name: 'data')
  User user;

  static LoginResponse fromJson(Map<String, dynamic> json) =>
      _$LoginResponseFromJson(json);

  static Map<String, dynamic> toJson(LoginResponse loginResponse) =>
      _$LoginResponseToJson(loginResponse);
}
