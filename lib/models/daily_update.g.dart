// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'daily_update.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DailyUpdates _$DailyUpdatesFromJson(Map<String, dynamic> json) {
  return DailyUpdates()
    ..message = json['message'] as String
    ..id = json['id'] as int
    ..userId = json['user_id'] as int
    ..createdAt = json['created_at'] as String
    ..data = json['user'] as Map<String, dynamic>
    ..updatedAt = json['updated_at'] as String
    ..date = json['date'] as String;
}

Map<String, dynamic> _$DailyUpdatesToJson(DailyUpdates instance) =>
    <String, dynamic>{
      'message': instance.message,
      'id': instance.id,
      'user_id': instance.userId,
      'created_at': instance.createdAt,
      'user': instance.data,
      'updated_at': instance.updatedAt,
      'date': instance.date,
    };

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$DailyUpdates on _DailyUpdates, Store {
  final _$messageAtom = Atom(name: '_DailyUpdates.message');

  @override
  String get message {
    _$messageAtom.context.enforceReadPolicy(_$messageAtom);
    _$messageAtom.reportObserved();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.context.conditionallyRunInAction(() {
      super.message = value;
      _$messageAtom.reportChanged();
    }, _$messageAtom, name: '${_$messageAtom.name}_set');
  }

  final _$idAtom = Atom(name: '_DailyUpdates.id');

  @override
  int get id {
    _$idAtom.context.enforceReadPolicy(_$idAtom);
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(int value) {
    _$idAtom.context.conditionallyRunInAction(() {
      super.id = value;
      _$idAtom.reportChanged();
    }, _$idAtom, name: '${_$idAtom.name}_set');
  }

  final _$userIdAtom = Atom(name: '_DailyUpdates.userId');

  @override
  int get userId {
    _$userIdAtom.context.enforceReadPolicy(_$userIdAtom);
    _$userIdAtom.reportObserved();
    return super.userId;
  }

  @override
  set userId(int value) {
    _$userIdAtom.context.conditionallyRunInAction(() {
      super.userId = value;
      _$userIdAtom.reportChanged();
    }, _$userIdAtom, name: '${_$userIdAtom.name}_set');
  }

  final _$createdAtAtom = Atom(name: '_DailyUpdates.createdAt');

  @override
  String get createdAt {
    _$createdAtAtom.context.enforceReadPolicy(_$createdAtAtom);
    _$createdAtAtom.reportObserved();
    return super.createdAt;
  }

  @override
  set createdAt(String value) {
    _$createdAtAtom.context.conditionallyRunInAction(() {
      super.createdAt = value;
      _$createdAtAtom.reportChanged();
    }, _$createdAtAtom, name: '${_$createdAtAtom.name}_set');
  }

  final _$dataAtom = Atom(name: '_DailyUpdates.data');

  @override
  Map<String, dynamic> get data {
    _$dataAtom.context.enforceReadPolicy(_$dataAtom);
    _$dataAtom.reportObserved();
    return super.data;
  }

  @override
  set data(Map<String, dynamic> value) {
    _$dataAtom.context.conditionallyRunInAction(() {
      super.data = value;
      _$dataAtom.reportChanged();
    }, _$dataAtom, name: '${_$dataAtom.name}_set');
  }

  final _$updatedAtAtom = Atom(name: '_DailyUpdates.updatedAt');

  @override
  String get updatedAt {
    _$updatedAtAtom.context.enforceReadPolicy(_$updatedAtAtom);
    _$updatedAtAtom.reportObserved();
    return super.updatedAt;
  }

  @override
  set updatedAt(String value) {
    _$updatedAtAtom.context.conditionallyRunInAction(() {
      super.updatedAt = value;
      _$updatedAtAtom.reportChanged();
    }, _$updatedAtAtom, name: '${_$updatedAtAtom.name}_set');
  }

  final _$dateAtom = Atom(name: '_DailyUpdates.date');

  @override
  String get date {
    _$dateAtom.context.enforceReadPolicy(_$dateAtom);
    _$dateAtom.reportObserved();
    return super.date;
  }

  @override
  set date(String value) {
    _$dateAtom.context.conditionallyRunInAction(() {
      super.date = value;
      _$dateAtom.reportChanged();
    }, _$dateAtom, name: '${_$dateAtom.name}_set');
  }
}
