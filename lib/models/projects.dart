import 'package:devslane_erp_flutter/models/tags.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:mobx/mobx.dart';

part 'projects.g.dart';

@JsonSerializable()
class Projects extends _Projects with _$Projects {
  static Projects fromJson(Map<String, dynamic> json) =>
      _$ProjectsFromJson(json);

  static Map<String, dynamic> toJson(Projects projects) =>
      _$ProjectsToJson(projects);
}

abstract class _Projects with Store {
  @observable
  int id;

  @observable
  String title;

  @observable
  String description;

  @observable
  @JsonKey(name: 'incubation_date')
  String incubationDate;

  @observable
  @JsonKey(name: 'logo_url')
  String logoUrl;

  @observable
  @JsonKey(name: 'extraDetails')
  String extraDetails;

  @observable
  String status;

  @observable
  @JsonKey(name: 'created_at')
  String createdAt;

  @observable
  @JsonKey(name: 'updated_at')
  String updatedAt;

  @observable
  @JsonKey(name: 'tags')
  Map<String, dynamic> data;

  @observable
  List<Tags> get tagsMap {
    return (data['data'] as List<dynamic>)
        .map((dynamic o) => Tags.fromJson(o))
        .toList();
  }
}
