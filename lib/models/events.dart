import 'package:devslane_erp_flutter/models/event_data.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:mobx/mobx.dart';

part 'events.g.dart';

@JsonSerializable()
class Events extends _Events with _$Events {
  static Events fromJson(Map<String, dynamic> json) => _$EventsFromJson(json);

  static Map<String, dynamic> toJson(Events events) => _$EventsToJson(events);
}

abstract class _Events with Store {
  @observable
  @JsonKey(name: 'data')
  Map<String, List<EventData>> dateMap;

  @observable
  @JsonKey(name: 'total_leaves_this_month')
  double leaves;

  @observable
  @JsonKey(name: 'total_holidays_this_month')
  double holidays;
}
