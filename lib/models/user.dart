import 'package:devslane_erp_flutter/models/tags.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:mobx/mobx.dart';

part 'user.g.dart';

@JsonSerializable()
class User extends _User with _$User {
  static User fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  static Map<String, dynamic> toJson(User user) => _$UserToJson(user);
}

abstract class _User with Store {
  @observable
  @JsonKey(name: 'id', nullable: false)
  int id;

  @observable
  String gender;

  @observable
  @JsonKey(name: 'first_name')
  String firstName;

  @observable
  @JsonKey(name: 'last_name')
  String lastName;

  @computed
  String get fullName {
    if (lastName == null) {
      return firstName;
    } else {
      return '$firstName $lastName';
    }
  }

  @observable
  String role;

  @observable
  @JsonKey(name: 'organization_id')
  int organizationId;

  @observable
  String email;

  @observable
  String phone;

  @observable
  String image;

  @observable
  String address;

  @observable
  @JsonKey(name: 'leaves_left')
  double leavesLeft;

  @observable
  @JsonKey(name: 'joining_date')
  String joiningDate;

  @observable
  @JsonKey(name: 'leaves_reset_at')
  String leavesResetAt;

  @observable
  @JsonKey(name: 'extra_details')
  String extraDetails;

  @observable
  @JsonKey(name: 'enrollment_id')
  int enrollmentId;

  @observable
  String status;

  @observable
  @JsonKey(name: 'should_post_daily_updates')
  bool shouldPostDailyUpdates;

  @observable
  @JsonKey(name: 'created_at')
  String createdAt;

  @observable
  @JsonKey(name: 'updated_at')
  String updatedAt;

  @observable
  @JsonKey(name: 'tags')
  Map<String, dynamic> data;

  @observable
  List<Tags> get tagsMap {
    return (data['data'] as List<dynamic>)
        .map((dynamic o) => Tags.fromJson(o))
        .toList();
  }
}
