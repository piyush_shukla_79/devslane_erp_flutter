// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventData _$EventDataFromJson(Map<String, dynamic> json) {
  return EventData()
    ..type = json['type'] as String
    ..user = json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>)
    ..request = json['meta'] == null
        ? null
        : Request.fromJson(json['meta'] as Map<String, dynamic>);
}

Map<String, dynamic> _$EventDataToJson(EventData instance) => <String, dynamic>{
      'type': instance.type,
      'user': instance.user,
      'meta': instance.request,
    };

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$EventData on _EventData, Store {
  final _$typeAtom = Atom(name: '_EventData.type');

  @override
  String get type {
    _$typeAtom.context.enforceReadPolicy(_$typeAtom);
    _$typeAtom.reportObserved();
    return super.type;
  }

  @override
  set type(String value) {
    _$typeAtom.context.conditionallyRunInAction(() {
      super.type = value;
      _$typeAtom.reportChanged();
    }, _$typeAtom, name: '${_$typeAtom.name}_set');
  }

  final _$userAtom = Atom(name: '_EventData.user');

  @override
  User get user {
    _$userAtom.context.enforceReadPolicy(_$userAtom);
    _$userAtom.reportObserved();
    return super.user;
  }

  @override
  set user(User value) {
    _$userAtom.context.conditionallyRunInAction(() {
      super.user = value;
      _$userAtom.reportChanged();
    }, _$userAtom, name: '${_$userAtom.name}_set');
  }

  final _$requestAtom = Atom(name: '_EventData.request');

  @override
  Request get request {
    _$requestAtom.context.enforceReadPolicy(_$requestAtom);
    _$requestAtom.reportObserved();
    return super.request;
  }

  @override
  set request(Request value) {
    _$requestAtom.context.conditionallyRunInAction(() {
      super.request = value;
      _$requestAtom.reportChanged();
    }, _$requestAtom, name: '${_$requestAtom.name}_set');
  }
}
