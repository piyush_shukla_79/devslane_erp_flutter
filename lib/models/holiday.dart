import 'package:json_annotation/json_annotation.dart';
import 'package:mobx/mobx.dart';

part 'holiday.g.dart';

@JsonSerializable()
class Holiday extends _Holiday with _$Holiday {
  static Holiday fromJson(Map<String, dynamic> json) => _$HolidayFromJson(json);

  static Map<String, dynamic> toJson(Holiday holiday) =>
      _$HolidayToJson(holiday);
}

abstract class _Holiday with Store {
  @observable
  int id;

  @observable
  String description;

  @observable
  String date;
}
