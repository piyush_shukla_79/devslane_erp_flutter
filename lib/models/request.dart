import 'package:devslane_erp_flutter/models/user.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:mobx/mobx.dart';

part 'request.g.dart';

@JsonSerializable()
class Request extends _Request with _$Request {
  static Request fromJson(Map<String, dynamic> json) => _$RequestFromJson(json);

  static Map<String, dynamic> toJson(Request request) =>
      _$RequestToJson(request);
}

abstract class _Request with Store {
  @observable
  @JsonKey(name: 'rejected_by')
  int rejectedBy;

  @observable
  @JsonKey(name: 'user_id')
  int userId;

  @observable
  @JsonKey(name: 'organization_id')
  int organizationId;

  @observable
  @JsonKey(name: 'rejected_at')
  String rejectedAt;

  @observable
  int id;

  @observable
  String note;

  @observable
  @JsonKey(name: 'type') //,fromJson: (){},toJson: (){})
  String requestType;

  //RequestType requestType;

  @observable
  String description;

  @observable
  @JsonKey(nullable: true)
  String status;

  @observable
  @JsonKey(nullable: true)
  String time;

  @observable
  @JsonKey(name: 'created_at')
  String createdAt;

  @observable
  @JsonKey(name: 'updated_at')
  String updatedAt;

  @observable
  @JsonKey(name: 'start_date')
  String startDate;

  @observable
  @JsonKey(name: 'end_date')
  String endDate;

  @observable
  @JsonKey(name: 'approved_at')
  String approvedAt;

  @observable
  @JsonKey(name: 'user', nullable: true)
  Map<String, dynamic> data;

  @observable
  User get getUser {
    return data != null && data['data'] != null
        ? User.fromJson(data['data'])
        : User();
  }

  @observable
  bool get actionPerformed {
    if (approvedAt == null && rejectedAt == null) {
      return false;
    } else {
      return true;
    }
  }

  @computed
  bool get request {
    if (actionPerformed) {
      if (rejectedAt == null) {
        return true;
      } else {
        return false;
      }
    }
  }

  @computed
  String get requestTypeString {
    switch (requestType) {
      case 'work_from_home':
        return 'Work From Home';
        break;
      case 'leave':
        return 'Leave';
        break;
      case 'half_day':
        return 'Half Day';
        break;
      case 'overtime':
        return 'Overtime';
        break;
    }
  }

  @JsonKey(name: 'approved_by')
  int approvedBy;
}

enum RequestType { leave, half_day, work_from_home, overtime }

//void cases() {
//  switch () {
//    case RequestType.half_day:
//      break;
//  }
//}
