import 'package:json_annotation/json_annotation.dart';
import 'package:mobx/mobx.dart';

part 'tags.g.dart';

@JsonSerializable()
class Tags extends _Tags with _$Tags {
  static Tags fromJson(Map<String, dynamic> json) => _$TagsFromJson(json);

  static Map<String, dynamic> toJson(Tags tags) => _$TagsToJson(tags);
}

abstract class _Tags with Store {
  @observable
  int id;

  @observable
  String slug;

  @observable
  String title;
}
