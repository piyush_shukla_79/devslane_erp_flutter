// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'projects.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Projects _$ProjectsFromJson(Map<String, dynamic> json) {
  return Projects()
    ..id = json['id'] as int
    ..title = json['title'] as String
    ..description = json['description'] as String
    ..incubationDate = json['incubation_date'] as String
    ..logoUrl = json['logo_url'] as String
    ..extraDetails = json['extraDetails'] as String
    ..status = json['status'] as String
    ..createdAt = json['created_at'] as String
    ..updatedAt = json['updated_at'] as String
    ..data = json['tags'] as Map<String, dynamic>;
}

Map<String, dynamic> _$ProjectsToJson(Projects instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'incubation_date': instance.incubationDate,
      'logo_url': instance.logoUrl,
      'extraDetails': instance.extraDetails,
      'status': instance.status,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
      'tags': instance.data,
    };

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$Projects on _Projects, Store {
  final _$idAtom = Atom(name: '_Projects.id');

  @override
  int get id {
    _$idAtom.context.enforceReadPolicy(_$idAtom);
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(int value) {
    _$idAtom.context.conditionallyRunInAction(() {
      super.id = value;
      _$idAtom.reportChanged();
    }, _$idAtom, name: '${_$idAtom.name}_set');
  }

  final _$titleAtom = Atom(name: '_Projects.title');

  @override
  String get title {
    _$titleAtom.context.enforceReadPolicy(_$titleAtom);
    _$titleAtom.reportObserved();
    return super.title;
  }

  @override
  set title(String value) {
    _$titleAtom.context.conditionallyRunInAction(() {
      super.title = value;
      _$titleAtom.reportChanged();
    }, _$titleAtom, name: '${_$titleAtom.name}_set');
  }

  final _$descriptionAtom = Atom(name: '_Projects.description');

  @override
  String get description {
    _$descriptionAtom.context.enforceReadPolicy(_$descriptionAtom);
    _$descriptionAtom.reportObserved();
    return super.description;
  }

  @override
  set description(String value) {
    _$descriptionAtom.context.conditionallyRunInAction(() {
      super.description = value;
      _$descriptionAtom.reportChanged();
    }, _$descriptionAtom, name: '${_$descriptionAtom.name}_set');
  }

  final _$incubationDateAtom = Atom(name: '_Projects.incubationDate');

  @override
  String get incubationDate {
    _$incubationDateAtom.context.enforceReadPolicy(_$incubationDateAtom);
    _$incubationDateAtom.reportObserved();
    return super.incubationDate;
  }

  @override
  set incubationDate(String value) {
    _$incubationDateAtom.context.conditionallyRunInAction(() {
      super.incubationDate = value;
      _$incubationDateAtom.reportChanged();
    }, _$incubationDateAtom, name: '${_$incubationDateAtom.name}_set');
  }

  final _$logoUrlAtom = Atom(name: '_Projects.logoUrl');

  @override
  String get logoUrl {
    _$logoUrlAtom.context.enforceReadPolicy(_$logoUrlAtom);
    _$logoUrlAtom.reportObserved();
    return super.logoUrl;
  }

  @override
  set logoUrl(String value) {
    _$logoUrlAtom.context.conditionallyRunInAction(() {
      super.logoUrl = value;
      _$logoUrlAtom.reportChanged();
    }, _$logoUrlAtom, name: '${_$logoUrlAtom.name}_set');
  }

  final _$extraDetailsAtom = Atom(name: '_Projects.extraDetails');

  @override
  String get extraDetails {
    _$extraDetailsAtom.context.enforceReadPolicy(_$extraDetailsAtom);
    _$extraDetailsAtom.reportObserved();
    return super.extraDetails;
  }

  @override
  set extraDetails(String value) {
    _$extraDetailsAtom.context.conditionallyRunInAction(() {
      super.extraDetails = value;
      _$extraDetailsAtom.reportChanged();
    }, _$extraDetailsAtom, name: '${_$extraDetailsAtom.name}_set');
  }

  final _$statusAtom = Atom(name: '_Projects.status');

  @override
  String get status {
    _$statusAtom.context.enforceReadPolicy(_$statusAtom);
    _$statusAtom.reportObserved();
    return super.status;
  }

  @override
  set status(String value) {
    _$statusAtom.context.conditionallyRunInAction(() {
      super.status = value;
      _$statusAtom.reportChanged();
    }, _$statusAtom, name: '${_$statusAtom.name}_set');
  }

  final _$createdAtAtom = Atom(name: '_Projects.createdAt');

  @override
  String get createdAt {
    _$createdAtAtom.context.enforceReadPolicy(_$createdAtAtom);
    _$createdAtAtom.reportObserved();
    return super.createdAt;
  }

  @override
  set createdAt(String value) {
    _$createdAtAtom.context.conditionallyRunInAction(() {
      super.createdAt = value;
      _$createdAtAtom.reportChanged();
    }, _$createdAtAtom, name: '${_$createdAtAtom.name}_set');
  }

  final _$updatedAtAtom = Atom(name: '_Projects.updatedAt');

  @override
  String get updatedAt {
    _$updatedAtAtom.context.enforceReadPolicy(_$updatedAtAtom);
    _$updatedAtAtom.reportObserved();
    return super.updatedAt;
  }

  @override
  set updatedAt(String value) {
    _$updatedAtAtom.context.conditionallyRunInAction(() {
      super.updatedAt = value;
      _$updatedAtAtom.reportChanged();
    }, _$updatedAtAtom, name: '${_$updatedAtAtom.name}_set');
  }

  final _$dataAtom = Atom(name: '_Projects.data');

  @override
  Map<String, dynamic> get data {
    _$dataAtom.context.enforceReadPolicy(_$dataAtom);
    _$dataAtom.reportObserved();
    return super.data;
  }

  @override
  set data(Map<String, dynamic> value) {
    _$dataAtom.context.conditionallyRunInAction(() {
      super.data = value;
      _$dataAtom.reportChanged();
    }, _$dataAtom, name: '${_$dataAtom.name}_set');
  }
}
