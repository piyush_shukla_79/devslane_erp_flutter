import 'package:devslane_erp_flutter/models/user.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:mobx/mobx.dart';

part 'daily_update.g.dart';

@JsonSerializable()
class DailyUpdates extends _DailyUpdates with _$DailyUpdates {
  static DailyUpdates fromJson(Map<String, dynamic> json) =>
      _$DailyUpdatesFromJson(json);

  static Map<String, dynamic> toJson(DailyUpdates dailyUpdates) =>
      _$DailyUpdatesToJson(dailyUpdates);
}

abstract class _DailyUpdates with Store {
  @observable
  String message;

  @observable
  int id;

  @observable
  @JsonKey(name: 'user_id')
  int userId;

  @observable
  @JsonKey(name: 'created_at')
  String createdAt;

  @observable
  @JsonKey(name: 'user')
  Map<String, dynamic> data;

  @observable
  User get getUser {
    return User.fromJson(data['data']);
  }

  @observable
  @JsonKey(name: 'updated_at')
  String updatedAt;

  @observable
  String date;
}
