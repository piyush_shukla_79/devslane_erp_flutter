import 'package:json_annotation/json_annotation.dart';
import 'package:mobx/mobx.dart';

part 'notifications.g.dart';

@JsonSerializable()
class Notifications extends _Notifications with _$Notifications {
  static Notifications fromJson(Map<String, dynamic> json) =>
      _$NotificationsFromJson(json);

  static Map<String, dynamic> toJson(Notifications user) =>
      _$NotificationsToJson(user);
}

abstract class _Notifications with Store {}
