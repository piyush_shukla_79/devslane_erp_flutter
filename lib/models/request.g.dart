// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Request _$RequestFromJson(Map<String, dynamic> json) {
  return Request()
    ..rejectedBy = json['rejected_by'] as int
    ..userId = json['user_id'] as int
    ..organizationId = json['organization_id'] as int
    ..rejectedAt = json['rejected_at'] as String
    ..id = json['id'] as int
    ..note = json['note'] as String
    ..requestType = json['type'] as String
    ..description = json['description'] as String
    ..status = json['status'] as String
    ..time = json['time'] as String
    ..createdAt = json['created_at'] as String
    ..updatedAt = json['updated_at'] as String
    ..startDate = json['start_date'] as String
    ..endDate = json['end_date'] as String
    ..approvedAt = json['approved_at'] as String
    ..data = json['user'] as Map<String, dynamic>
    ..approvedBy = json['approved_by'] as int;
}

Map<String, dynamic> _$RequestToJson(Request instance) => <String, dynamic>{
      'rejected_by': instance.rejectedBy,
      'user_id': instance.userId,
      'organization_id': instance.organizationId,
      'rejected_at': instance.rejectedAt,
      'id': instance.id,
      'note': instance.note,
      'type': instance.requestType,
      'description': instance.description,
      'status': instance.status,
      'time': instance.time,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
      'start_date': instance.startDate,
      'end_date': instance.endDate,
      'approved_at': instance.approvedAt,
      'user': instance.data,
      'approved_by': instance.approvedBy,
    };

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$Request on _Request, Store {
  Computed<bool> _$requestComputed;

  @override
  bool get request =>
      (_$requestComputed ??= Computed<bool>(() => super.request)).value;

  final _$rejectedByAtom = Atom(name: '_Request.rejectedBy');

  @override
  int get rejectedBy {
    _$rejectedByAtom.context.enforceReadPolicy(_$rejectedByAtom);
    _$rejectedByAtom.reportObserved();
    return super.rejectedBy;
  }

  @override
  set rejectedBy(int value) {
    _$rejectedByAtom.context.conditionallyRunInAction(() {
      super.rejectedBy = value;
      _$rejectedByAtom.reportChanged();
    }, _$rejectedByAtom, name: '${_$rejectedByAtom.name}_set');
  }

  final _$userIdAtom = Atom(name: '_Request.userId');

  @override
  int get userId {
    _$userIdAtom.context.enforceReadPolicy(_$userIdAtom);
    _$userIdAtom.reportObserved();
    return super.userId;
  }

  @override
  set userId(int value) {
    _$userIdAtom.context.conditionallyRunInAction(() {
      super.userId = value;
      _$userIdAtom.reportChanged();
    }, _$userIdAtom, name: '${_$userIdAtom.name}_set');
  }

  final _$organizationIdAtom = Atom(name: '_Request.organizationId');

  @override
  int get organizationId {
    _$organizationIdAtom.context.enforceReadPolicy(_$organizationIdAtom);
    _$organizationIdAtom.reportObserved();
    return super.organizationId;
  }

  @override
  set organizationId(int value) {
    _$organizationIdAtom.context.conditionallyRunInAction(() {
      super.organizationId = value;
      _$organizationIdAtom.reportChanged();
    }, _$organizationIdAtom, name: '${_$organizationIdAtom.name}_set');
  }

  final _$rejectedAtAtom = Atom(name: '_Request.rejectedAt');

  @override
  String get rejectedAt {
    _$rejectedAtAtom.context.enforceReadPolicy(_$rejectedAtAtom);
    _$rejectedAtAtom.reportObserved();
    return super.rejectedAt;
  }

  @override
  set rejectedAt(String value) {
    _$rejectedAtAtom.context.conditionallyRunInAction(() {
      super.rejectedAt = value;
      _$rejectedAtAtom.reportChanged();
    }, _$rejectedAtAtom, name: '${_$rejectedAtAtom.name}_set');
  }

  final _$idAtom = Atom(name: '_Request.id');

  @override
  int get id {
    _$idAtom.context.enforceReadPolicy(_$idAtom);
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(int value) {
    _$idAtom.context.conditionallyRunInAction(() {
      super.id = value;
      _$idAtom.reportChanged();
    }, _$idAtom, name: '${_$idAtom.name}_set');
  }

  final _$noteAtom = Atom(name: '_Request.note');

  @override
  String get note {
    _$noteAtom.context.enforceReadPolicy(_$noteAtom);
    _$noteAtom.reportObserved();
    return super.note;
  }

  @override
  set note(String value) {
    _$noteAtom.context.conditionallyRunInAction(() {
      super.note = value;
      _$noteAtom.reportChanged();
    }, _$noteAtom, name: '${_$noteAtom.name}_set');
  }

  final _$requestTypeAtom = Atom(name: '_Request.requestType');

  @override
  String get requestType {
    _$requestTypeAtom.context.enforceReadPolicy(_$requestTypeAtom);
    _$requestTypeAtom.reportObserved();
    return super.requestType;
  }

  @override
  set requestType(String value) {
    _$requestTypeAtom.context.conditionallyRunInAction(() {
      super.requestType = value;
      _$requestTypeAtom.reportChanged();
    }, _$requestTypeAtom, name: '${_$requestTypeAtom.name}_set');
  }

  final _$descriptionAtom = Atom(name: '_Request.description');

  @override
  String get description {
    _$descriptionAtom.context.enforceReadPolicy(_$descriptionAtom);
    _$descriptionAtom.reportObserved();
    return super.description;
  }

  @override
  set description(String value) {
    _$descriptionAtom.context.conditionallyRunInAction(() {
      super.description = value;
      _$descriptionAtom.reportChanged();
    }, _$descriptionAtom, name: '${_$descriptionAtom.name}_set');
  }

  final _$statusAtom = Atom(name: '_Request.status');

  @override
  String get status {
    _$statusAtom.context.enforceReadPolicy(_$statusAtom);
    _$statusAtom.reportObserved();
    return super.status;
  }

  @override
  set status(String value) {
    _$statusAtom.context.conditionallyRunInAction(() {
      super.status = value;
      _$statusAtom.reportChanged();
    }, _$statusAtom, name: '${_$statusAtom.name}_set');
  }

  final _$timeAtom = Atom(name: '_Request.time');

  @override
  String get time {
    _$timeAtom.context.enforceReadPolicy(_$timeAtom);
    _$timeAtom.reportObserved();
    return super.time;
  }

  @override
  set time(String value) {
    _$timeAtom.context.conditionallyRunInAction(() {
      super.time = value;
      _$timeAtom.reportChanged();
    }, _$timeAtom, name: '${_$timeAtom.name}_set');
  }

  final _$createdAtAtom = Atom(name: '_Request.createdAt');

  @override
  String get createdAt {
    _$createdAtAtom.context.enforceReadPolicy(_$createdAtAtom);
    _$createdAtAtom.reportObserved();
    return super.createdAt;
  }

  @override
  set createdAt(String value) {
    _$createdAtAtom.context.conditionallyRunInAction(() {
      super.createdAt = value;
      _$createdAtAtom.reportChanged();
    }, _$createdAtAtom, name: '${_$createdAtAtom.name}_set');
  }

  final _$updatedAtAtom = Atom(name: '_Request.updatedAt');

  @override
  String get updatedAt {
    _$updatedAtAtom.context.enforceReadPolicy(_$updatedAtAtom);
    _$updatedAtAtom.reportObserved();
    return super.updatedAt;
  }

  @override
  set updatedAt(String value) {
    _$updatedAtAtom.context.conditionallyRunInAction(() {
      super.updatedAt = value;
      _$updatedAtAtom.reportChanged();
    }, _$updatedAtAtom, name: '${_$updatedAtAtom.name}_set');
  }

  final _$startDateAtom = Atom(name: '_Request.startDate');

  @override
  String get startDate {
    _$startDateAtom.context.enforceReadPolicy(_$startDateAtom);
    _$startDateAtom.reportObserved();
    return super.startDate;
  }

  @override
  set startDate(String value) {
    _$startDateAtom.context.conditionallyRunInAction(() {
      super.startDate = value;
      _$startDateAtom.reportChanged();
    }, _$startDateAtom, name: '${_$startDateAtom.name}_set');
  }

  final _$endDateAtom = Atom(name: '_Request.endDate');

  @override
  String get endDate {
    _$endDateAtom.context.enforceReadPolicy(_$endDateAtom);
    _$endDateAtom.reportObserved();
    return super.endDate;
  }

  @override
  set endDate(String value) {
    _$endDateAtom.context.conditionallyRunInAction(() {
      super.endDate = value;
      _$endDateAtom.reportChanged();
    }, _$endDateAtom, name: '${_$endDateAtom.name}_set');
  }

  final _$approvedAtAtom = Atom(name: '_Request.approvedAt');

  @override
  String get approvedAt {
    _$approvedAtAtom.context.enforceReadPolicy(_$approvedAtAtom);
    _$approvedAtAtom.reportObserved();
    return super.approvedAt;
  }

  @override
  set approvedAt(String value) {
    _$approvedAtAtom.context.conditionallyRunInAction(() {
      super.approvedAt = value;
      _$approvedAtAtom.reportChanged();
    }, _$approvedAtAtom, name: '${_$approvedAtAtom.name}_set');
  }

  final _$dataAtom = Atom(name: '_Request.data');

  @override
  Map<String, dynamic> get data {
    _$dataAtom.context.enforceReadPolicy(_$dataAtom);
    _$dataAtom.reportObserved();
    return super.data;
  }

  @override
  set data(Map<String, dynamic> value) {
    _$dataAtom.context.conditionallyRunInAction(() {
      super.data = value;
      _$dataAtom.reportChanged();
    }, _$dataAtom, name: '${_$dataAtom.name}_set');
  }
}
