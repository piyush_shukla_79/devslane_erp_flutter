import 'package:devslane_erp_flutter/presentations/calendar_page.dart';
import 'package:devslane_erp_flutter/presentations/daily_updates_page.dart';
import 'package:devslane_erp_flutter/presentations/employees_profile_page.dart';
import 'package:devslane_erp_flutter/presentations/holiday_page.dart';
import 'package:devslane_erp_flutter/presentations/home_page.dart';
import 'package:devslane_erp_flutter/presentations/requests_page.dart';
import 'package:devslane_erp_flutter/presentations/profile_page.dart';
import 'package:devslane_erp_flutter/presentations/projects_page.dart';
import 'package:flutter/material.dart';

class NavigationProvider with ChangeNotifier {
  String currentNavigation = ProfilePage.routeNamed;

  Widget get getNavigation {
    switch (currentNavigation) {
      case ProfilePage.routeNamed:
        return ProfilePage();
        break;
      case DailyUpdatesPage.routeNamed:
        return DailyUpdatesPage();
        break;
      case RequestsPage.routeNamed:
        return RequestsPage();
        break;
      case EmployeesProfilePage.routeNamed:
        return EmployeesProfilePage();
        break;
      case ProjectsPage.routeNamed:
        return ProjectsPage();
        break;
      case CalendarPage.routeNamed:
        return CalendarPage();
        break;
      case HolidayPage.routeNamed:
        return HolidayPage();
        break;
      default:
        return HomePage();
    }
  }

  void updateNavigation(String navigation, BuildContext context) {
    currentNavigation = navigation;
    notifyListeners();
  }
}
