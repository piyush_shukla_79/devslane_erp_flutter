import 'dart:async';

import 'package:devslane_erp_flutter/app_config.dart';
import 'package:devslane_erp_flutter/main.dart';
import 'package:flutter/material.dart';
import 'package:sentry/sentry.dart';

final SentryClient sentry = SentryClient(dsn: sentryDSNUrl);

void main() {
  setupEnv(Environment.prod);

  FlutterError.onError = (FlutterErrorDetails details) {
    FlutterError.dumpErrorToConsole(details);
    _reportError(details, details.stack);
  };

  runZoned<Future<Null>>(() async {
    runApp(DevsLaneErpApp());
  }, onError: (dynamic error, dynamic stackTrace) async {
    await _reportError(error, stackTrace);
  });
}

bool get isInDebugMode {
  bool inDebugMode = false;
  assert(inDebugMode = true);
  return inDebugMode;
}

Future<void> _reportError(dynamic error, dynamic stackTrace) async {
  if (isInDebugMode) {
    return;
  } else {
    // Send the Exception and Stacktrace to Sentry in Production mode
    sentry.captureException(
      exception: error,
      stackTrace: stackTrace,
    );
  }
}
