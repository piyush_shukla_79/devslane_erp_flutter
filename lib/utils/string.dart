class StringValue {
  static const String FILTER = 'Filter';
  static const String ALERT_BOX_FIRST_TITLE = 'Employee';
  static const String ALERT_BOX_START_DATE = 'Start Date';
  static const String ALERT_BOX_END_DATE = 'End Date';
  static const String DONE_TEXT = 'Done';
  static const String CLEAR_FIELD_TEXT = 'Clear Field';
  static const String ADD = 'Add';
  static const String CANCEL = 'Cancel';
  static const String REQUEST = 'Create Request';
  static const String ACCEPT = 'Accept';
  static const String REJECT = 'Reject';
  static const String MY_REQUEST = 'My Request';
  static const String ALL = 'All';
  static const String NOTE = 'Note';
  static const String SEND_ME_LINK = 'Send Me Link';
  static const String EMAIL = 'Email';
  static const String PASSWORD = 'Password';
}
