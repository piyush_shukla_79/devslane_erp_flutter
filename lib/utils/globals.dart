import 'package:devslane_erp_flutter/services/auth_service.dart';
import 'package:devslane_erp_flutter/services/calendar_service.dart';
import 'package:devslane_erp_flutter/services/daily_update_service.dart';
import 'package:devslane_erp_flutter/services/employee_service.dart';
import 'package:devslane_erp_flutter/services/get_projects_service.dart';
import 'package:devslane_erp_flutter/services/holiday_service.dart';
import 'package:devslane_erp_flutter/services/preferences_service.dart';
import 'package:devslane_erp_flutter/services/profile_page_service.dart';
import 'package:devslane_erp_flutter/services/request_service.dart';

final DailyUpdateService dailyUpdateService = DailyUpdateService.getInstance();
final EmployeeService employeeService = EmployeeService.getInstance();
final HolidayService holidayService = HolidayService.getInstance();
final ProfilePageService profilePageService = ProfilePageService.getInstance();
final GetProjectsService getProjectsService = GetProjectsService.getInstance();
final AuthService authService = AuthService.getInstance();
final CalendarService calendarService = CalendarService.getInstance();
final PreferencesService preferencesService = PreferencesService.getInstance();
final RequestService requestService = RequestService.getInstance();
