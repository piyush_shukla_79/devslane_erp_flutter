import 'package:devslane_erp_flutter/presentations/customs/store_observer.dart';
import 'package:devslane_erp_flutter/stores/holiday_store.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_section_table_view/flutter_section_table_view.dart';
import 'package:flutter_section_table_view/pullrefresh/src/smart_refresher.dart';
import 'package:intl/intl.dart';

import 'customs/page_icon_chipset.dart';

class HolidayPage extends StatelessWidget {
  static const String routeNamed = 'Holiday';

  final SectionTableController controller = SectionTableController();
  final RefreshController refreshController = RefreshController();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Stack(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 55.0),
            child: StoreObserver<HolidayStore>(
              builder: (HolidayStore holidayStore, BuildContext context) {
                if (holidayStore.holidays.isEmpty) {
                  holidayStore.fetchData();
                }
                return holidayStore.isLoading
                    ? const Center(
                        child: CircularProgressIndicator(),
                      )
                    : holidayStore.overallHoliday().isEmpty &&
                            holidayStore.todayHoliday().isEmpty
                        ? const Center(
                            child: Text('There are no holidays'),
                          )
                        : SectionTableView(
                            enablePullDown: false,
                            enablePullUp: false,
//                            onRefresh: (bool up) {
//                              Future<dynamic>.delayed(
//                                      const Duration(milliseconds: 2009))
//                                  .then((dynamic val) {
//                                refreshController.sendBack(
//                                    up, RefreshStatus.completed);
//                                holidayStore.fetchData();
//                              });
//                            },
                            refreshController: refreshController,
                            sectionCount:
                                holidayStore.todayHoliday().isEmpty ? 1 : 2,
                            numOfRowInSection: (int section) {
                              return section == 1
                                  ? holidayStore.overallHoliday().length
                                  : holidayStore.todayHoliday().length;
                            },
                            cellAtIndexPath: (int section, int row) {
                              return Card(
                                margin: const EdgeInsets.symmetric(
                                  vertical: 5.0,
                                  horizontal: 10.0,
                                ),
                                shape: RoundedRectangleBorder(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10)),
                                ),
                                child: Container(
                                  padding: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(10)),
                                  ),
                                  child: Center(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            const Text(
                                              'Date : ',
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              section == 1
                                                  ? DateFormat.yMMMMd().format(
                                                      DateTime.parse(
                                                          holidayStore
                                                              .overallHoliday()[
                                                                  row]
                                                              .date))
                                                  : DateFormat.yMMMMd().format(
                                                      DateTime.parse(
                                                          holidayStore
                                                              .todayHoliday()[
                                                                  row]
                                                              .date)),
                                              style:
                                                  const TextStyle(fontSize: 20),
                                            ),
                                          ],
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          children: <Widget>[
                                            const Text(
                                              'Reason : ',
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Flexible(
                                              flex: 1,
                                              child: Text(
                                                section == 1
                                                    ? holidayStore
                                                        .overallHoliday()[row]
                                                        .description
                                                    : holidayStore
                                                        .todayHoliday()[row]
                                                        .description,
                                                style: const TextStyle(
                                                    fontSize: 20),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                            headerInSection: (int section) {
                              return sectionHeader(
                                section: section,
                                value: holidayStore.todayHoliday().isEmpty
                                    ? 'All'
                                    : section == 1
                                        ? 'All'
                                        : 'Upcoming Holidays',
                              );
                            },
                            divider: Container(
                              height: 5.0,
                            ),
                            controller: controller,
                            sectionHeaderHeight: (int section) =>
                                SizeConfig.screenHeight * 0.05,
                            dividerHeight: () => 1.0,
                            cellHeightAtIndexPath: (int section, int row) =>
                                44.0,
                          );
              },
            ),
          ),
          PageIcon(
            iconData: Icons.no_encryption,
          )
        ],
      ),
    );
  }

  Widget sectionHeader({int section, String value}) {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.only(
          left: 15.0, right: 15.0, bottom: 10.0, top: 35.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                value,
                style: const TextStyle(
                    color: Colors.blue,
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          const SizedBox(
            height: 10.0,
          ),
          const Divider(
            color: Colors.blue,
          )
        ],
      ),
    );
  }
}
