import 'package:connectivity/connectivity.dart';
import 'package:devslane_erp_flutter/models/provider.dart';
import 'package:devslane_erp_flutter/presentations/calendar_page.dart';
import 'package:devslane_erp_flutter/presentations/customs/store_observer.dart';
import 'package:devslane_erp_flutter/presentations/function/add_request.dart';
import 'package:devslane_erp_flutter/presentations/holiday_page.dart';
import 'package:devslane_erp_flutter/presentations/login_page.dart';
import 'package:devslane_erp_flutter/presentations/profile_page.dart';
import 'package:devslane_erp_flutter/presentations/projects_page.dart';
import 'package:devslane_erp_flutter/stores/calendar_store.dart';
import 'package:devslane_erp_flutter/stores/daily_update_store.dart';
import 'package:devslane_erp_flutter/stores/employee_store.dart';
import 'package:devslane_erp_flutter/stores/holiday_store.dart';
import 'package:devslane_erp_flutter/stores/projects_store.dart';
import 'package:devslane_erp_flutter/stores/request_store.dart';
import 'package:devslane_erp_flutter/stores/user_store.dart';
import 'package:devslane_erp_flutter/utils/globals.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'customs/page_icon_chipset.dart';
import 'daily_updates_page.dart';
import 'employees_profile_page.dart';
import 'requests_page.dart';

class HomePage extends StatefulWidget {
  static const String routeNamed = '/';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  bool hasConnection = true;
  Connectivity connectivity = Connectivity();

  @override
  void initState() {
    super.initState();
    connectivity.checkConnectivity().then((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        hasConnection = false;
      } else {
        hasConnection = true;
      }
    });
    connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        setState(() {
          hasConnection = false;
        });
        noConnection();
      } else {
        setState(() {
          hasConnection = true;
        });
      }
    });
    tabController = TabController(length: 2, initialIndex: 1, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    final NavigationProvider navigation =
        Provider.of<NavigationProvider>(context);
    SizeConfig().init(context);
    return SafeArea(
      child: WillPopScope(
        onWillPop: () {
          if (navigation.currentNavigation == ProfilePage.routeNamed) {
            return Future.value(true);
          } else {
            navigation.updateNavigation(ProfilePage.routeNamed, context);
            return Future.value(false);
          }
        },
        child: Scaffold(
          drawer: Drawer(
            child: Container(
              color: const Color.fromRGBO(53, 124, 184, 1),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        padding: const EdgeInsets.all(20),
                        child: InkWell(
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.screenHeight * 0.05,
                    ),
                    drawerOptions(
                      context,
                      navigation,
                      ProfilePage.routeNamed,
                      'Home',
                      navigation.currentNavigation == ProfilePage.routeNamed,
                    ),
                    drawerOptions(
                      context,
                      navigation,
                      DailyUpdatesPage.routeNamed,
                      'Daily Updates',
                      navigation.currentNavigation ==
                          DailyUpdatesPage.routeNamed,
                    ),
                    drawerOptions(
                      context,
                      navigation,
                      RequestsPage.routeNamed,
                      'Requests',
                      navigation.currentNavigation == RequestsPage.routeNamed,
                    ),
                    drawerOptions(
                      context,
                      navigation,
                      EmployeesProfilePage.routeNamed,
                      'Employees',
                      navigation.currentNavigation ==
                          EmployeesProfilePage.routeNamed,
                    ),
                    drawerOptions(
                      context,
                      navigation,
                      ProjectsPage.routeNamed,
                      'Projects',
                      navigation.currentNavigation == ProjectsPage.routeNamed,
                    ),
                    drawerOptions(
                      context,
                      navigation,
                      HolidayPage.routeNamed,
                      'Holidays',
                      navigation.currentNavigation == HolidayPage.routeNamed,
                    ),
                    drawerOptions(
                      context,
                      navigation,
                      CalendarPage.routeNamed,
                      'Calendar',
                      navigation.currentNavigation == CalendarPage.routeNamed,
                    ),
                    drawerOptions(
                      context,
                      navigation,
                      null,
                      'Logout',
                      false,
                    )
                  ],
                ),
              ),
            ),
          ),
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Container(
              margin: const EdgeInsets.symmetric(vertical: 5.0),
              child: Center(
                child: Image.asset(
                  'assets/devslaneIcon.jpg',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            bottom: navigation.currentNavigation == RequestsPage.routeNamed &&
                    Provider.of<UserStore>(context).loggedInUser != null
                ? Provider.of<UserStore>(context).loggedInUser.role ==
                            'admin' ||
                        Provider.of<UserStore>(context).loggedInUser.role ==
                            'owner'
                    ? TabBar(
                        controller: tabController,
                        tabs: const <Widget>[
                          Tab(
                            child: Text('My Request'),
                          ),
                          Tab(
                            child: Text('All'),
                          ),
                        ],
                      )
                    : PreferredSize(
                        child: Container(),
                        preferredSize: Size.fromHeight(0),
                      )
                : PreferredSize(
                    child: Container(),
                    preferredSize: Size.fromHeight(0),
                  ),
          ),
          body: Stack(
            children: <Widget>[
              navigation.currentNavigation == RequestsPage.routeNamed
                  ? Stack(
                      children: <Widget>[
                        _getPageView(),
                        Positioned(
                          bottom: 10,
                          right: 10,
                          child: FloatingActionButton(
                            child: Icon(
                              Icons.add,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              showDialog<void>(
                                context: context,
                                builder: (BuildContext context) {
                                  return AddRequest();
                                },
                              );
                            },
                          ),
                        ),
                      ],
                    )
                  : Consumer<NavigationProvider>(
                      builder: (BuildContext context,
                              NavigationProvider navigationProvider, _) =>
                          navigationProvider.getNavigation),
              Positioned(
                bottom: 0,
                child: AnimatedContainer(
                  duration: Duration(milliseconds: 600),
                  curve: Curves.linear,
                  width: MediaQuery.of(context).size.width,
                  height: hasConnection ? 0 : 30,
                  child: Center(
                    child: Text(
                      _getErrorMessage(),
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  color: Theme.of(context).errorColor,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String _getErrorMessage() {
    return 'You are not connected to internet';
  }

  Widget _getPageView({int index = 0}) {
    return Stack(
      children: <Widget>[
        StoreObserver<RequestStore>(
          builder: (RequestStore requestStore, BuildContext context) {
            if (Provider.of<UserStore>(context).loggedInUser == null) {
              Provider.of<UserStore>(context).fetchUserDetails();
            }
            if (Provider.of<UserStore>(context).isLoggedIn) {
              if (requestStore.requestList.isEmpty & requestStore.hasMore) {
                requestStore.fetchData(length: 20);
              }
              if (requestStore.myRequestList.isEmpty &
                  requestStore.hasMoreMyRequest) {
                requestStore.fetchMyData(length: 20);
              }
            }
            return (requestStore.isLoading) ||
                    Provider.of<UserStore>(context).loggedInUser == null
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : requestStore.adminUser.role == 'admin' ||
                        requestStore.adminUser.role == 'owner'
                    ? TabBarView(
                        controller: tabController,
                        children: <Widget>[
                          requestStore.isMyRequestLoading &&
                                  requestStore.myRequestList.isEmpty
                              ? const Center(
                                  child: CircularProgressIndicator(),
                                )
                              : RequestsPage(
                                  isAll: false,
                                  hasMore: requestStore.hasMoreMyRequest,
                                  isLoading: requestStore.isMyRequestLoading,
                                  requestList: requestStore.myRequestList,
                                  pendingRequests:
                                      requestStore.myPendingRequestList,
                                  approvedRequests:
                                      requestStore.myApprovedRequestList,
                                  onFetch: () {
                                    requestStore.fetchMyData(
                                        length:
                                            requestStore.myRequestList.length +
                                                10);
                                  },
                                ),
                          requestStore.isLoading &&
                                  requestStore.requestList.isEmpty
                              ? const Center(
                                  child: CircularProgressIndicator(),
                                )
                              : RequestsPage(
                                  isAll: true,
                                  hasMore: requestStore.hasMore,
                                  isLoading: requestStore.isLoading,
                                  requestList: requestStore.getRequestMap.values
                                      .toList(),
                                  pendingRequests:
                                      requestStore.pendingRequestList,
                                  approvedRequests:
                                      requestStore.approvedRequestList,
                                  onFetch: () {
                                    requestStore.fetchData(
                                        length:
                                            requestStore.getRequestMap.length +
                                                20);
                                  },
                                )
                        ],
                      )
                    : requestStore.isMyRequestLoading &&
                            requestStore.myRequestList.isEmpty
                        ? const Center(
                            child: CircularProgressIndicator(),
                          )
                        : RequestsPage(
                            hasMore: requestStore.hasMoreMyRequest,
                            isLoading: requestStore.isMyRequestLoading,
                            requestList: requestStore.myRequestList,
                            pendingRequests: requestStore.myPendingRequestList,
                            approvedRequests:
                                requestStore.myApprovedRequestList,
                            onFetch: () {
                              requestStore.fetchMyData(
                                  length:
                                      requestStore.myRequestList.length + 10);
                            },
                          );
          },
        ),
        PageIcon(
          iconData: Icons.call_merge,
        )
      ],
    );
  }

  Widget drawerOptions(BuildContext context, NavigationProvider navigation,
      String routeNamed, String title, bool isSelected) {
    return InkWell(
      onTap: () {
        if (routeNamed != null) {
          Navigator.of(context).pop();
          navigation.updateNavigation(routeNamed, context);
        } else {
          Navigator.of(context).pop();
          showLogOutDialog(context, navigation);
        }
      },
      child: Center(
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 10.0),
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15.0),
          decoration: BoxDecoration(
            color: isSelected ? Colors.white : Colors.transparent,
            borderRadius: const BorderRadius.all(
              Radius.circular(15.0),
            ),
          ),
          child: Text(
            title,
            style: TextStyle(
              color: isSelected
                  ? const Color.fromRGBO(53, 124, 184, 1)
                  : Colors.white,
              fontSize: 18.0,
            ),
          ),
        ),
      ),
    );
  }

  Future<Widget> showLogOutDialog(
      BuildContext context, NavigationProvider navigationProvider) {
    return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Container(
            margin:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
            child: Text(
              'Are you sure you want to log out?',
              style: TextStyle(
                fontSize: 18.0,
                color: Colors.black,
              ),
            ),
          ),
          actions: <Widget>[
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                margin: const EdgeInsets.only(
                    left: 12.0, right: 12.0, bottom: 15.0, top: 10.0),
                child: const Text(
                  'No',
                  style: TextStyle(
                    fontSize: 15.0,
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.popUntil(context, (route) => route.isFirst);
                Navigator.pushReplacementNamed(context, LoginPage.routeNamed);
                preferencesService.setIsFirstLaunch(true);
                preferencesService.setAuthUser(null);
                preferencesService.setAuthToken(null);
                navigationProvider.updateNavigation(
                    ProfilePage.routeNamed, context);
                clearStore();
              },
              child: Container(
                margin: const EdgeInsets.only(
                    left: 12.0, right: 12.0, bottom: 15.0, top: 10.0),
                child: const Text(
                  'Yes',
                  style: TextStyle(
                    fontSize: 15.0,
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            )
          ],
        );
      },
    );
  }

  void noConnection() {
    Provider.of<CalendarStore>(context).stopFetch();
    Provider.of<DailyUpdateStore>(context).stopFetch();
    Provider.of<EmployeeStore>(context).stopFetch();
    Provider.of<HolidayStore>(context).stopFetch();
    Provider.of<ProjectsStore>(context).stopFetch();
    Provider.of<RequestStore>(context).stopFetch();
  }

  void clearStore() {
    Provider.of<CalendarStore>(context).clearCalenderStore();
    Provider.of<DailyUpdateStore>(context).clearDailyUpdateStore();
    Provider.of<EmployeeStore>(context).clearEmployeeStore();
    Provider.of<HolidayStore>(context).clearHolidayStore();
    Provider.of<ProjectsStore>(context).clearProjectsStore();
    Provider.of<RequestStore>(context).clearRequestStore();
    Provider.of<UserStore>(context).clearUserStore();
  }
}
