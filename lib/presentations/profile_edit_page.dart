import 'package:devslane_erp_flutter/presentations/customs/store_observer.dart';
import 'package:devslane_erp_flutter/stores/user_store.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

class ProfileEditPage extends StatefulWidget {
  static const String routeNamed = 'ProfileEditPage';

  @override
  _ProfileEditPageState createState() => _ProfileEditPageState();
}

class _ProfileEditPageState extends State<ProfileEditPage> {
  final GlobalKey<FormBuilderState> _key = GlobalKey<FormBuilderState>();
  String gender;
  bool onChanged = false;
  bool isDigit = false;

  @override
  Widget build(BuildContext context) {
    gender = Provider.of<UserStore>(context).loggedInUser.gender;
    SizeConfig().init(context);
    return StoreObserver<UserStore>(
      builder: (UserStore profilePageStore, BuildContext context) {
        return Scaffold(
          floatingActionButton: onChanged
              ? FloatingActionButton(
                  onPressed: () {
                    _key.currentState.save();
                    if (_key.currentState.validate() && onChanged) {
                      profilePageStore
                          .updateUserDetails(_key.currentState.value);
                      autorun(
                        (Reaction e) {
                          if (!profilePageStore.isUpdating) {
                            Navigator.pop(context);
                          }
                        },
                      );
                    }
                  },
                  child: profilePageStore.isUpdating
                      ? CupertinoActivityIndicator()
                      : Icon(
                          Icons.done,
                          color: Colors.white,
                        ),
                )
              : Container(),
//=======
//                );
//              } else {
//                Navigator.pop(context);
//              }
//            },
//            child: profilePageStore.isUpdating
//                ? CupertinoActivityIndicator()
//                : Icon(
//                    Icons.done,
//                    color: Colors.white,
//                  ),
//          ),
//>>>>>>> 55463018e5399dfa43492ea6b6a5f437ce67f643
          appBar: AppBar(
            title: const Text(
              'Edit Profile',
            ),
          ),
          body: Container(
            height: SizeConfig.screenHeight,
            width: SizeConfig.screenWidth,
            margin:
                const EdgeInsets.symmetric(horizontal: 25.0, vertical: 20.0),
            child: SingleChildScrollView(
              child: FormBuilder(
                key: _key,
                autovalidate: false,
                onChanged: (dynamic value) {
                  setState(() {
                    onChanged = true;
                  });
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const SizedBox(
                      height: 30.0,
                    ),
                    Container(
                      child: const Text(
                        'First Name',
                        style: TextStyle(),
                      ),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    FormBuilderTextField(
                      attribute: 'first_name',
                      validators: [
                        (dynamic value) {
                          RegExp re =
                              new RegExp(r'[!@#<>?":_`~;[\]\\|=+)(*&^%0-9-]');
                          if (value.toString().trim().length < 3) {
                            return 'Name should contain more than 2 alphabets';
                          }
                          if (re.hasMatch(value)) {
                            return 'Name should not contain numbers or special characters';
                          }
                        }
                      ],
                      autovalidate: true,
                      initialValue: profilePageStore.loggedInUser.firstName,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 30.0,
                    ),
                    Container(
                      child: const Text(
                        'Last Name',
                        style: TextStyle(),
                      ),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    FormBuilderTextField(
                      attribute: 'last_name',
                      validators: [
                        (dynamic value) {
                          RegExp re =
                              new RegExp(r'[!@#<>?":_`~;[\]\\|=+)(*&^%0-9-]');
                          if (!value.isEmpty &&
                              value.toString().trim().length < 3) {
                            return 'Name should contain more than 2 alphabets';
                          }
                          if (re.hasMatch(value)) {
                            return 'Name should not contain numbers or special characters';
                          }
                        }
                      ],
                      autovalidate: true,
                      initialValue: profilePageStore.loggedInUser.lastName,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 30.0,
                    ),
                    Container(
                      child: const Text(
                        'Phone',
                        style: TextStyle(),
                      ),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    FormBuilderTextField(
                      validators: [
                        (dynamic value) {
                          if (value.length < 10) {
                            return 'Invalid Number';
                          }
                        }
                      ],
                      autovalidate: true,
                      attribute: 'phone',
                      initialValue: profilePageStore.loggedInUser.phone,
                      maxLength: 10,
                      keyboardType:
                          const TextInputType.numberWithOptions(decimal: false),
                      decoration: InputDecoration(
                        counterText: '',
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 30.0,
                    ),
                    Container(
                      child: const Text(
                        'Gender',
                        style: TextStyle(),
                      ),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    FormBuilderDropdown(
                      attribute: 'gender',
                      initialValue: gender,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue),
                        ),
                      ),
                      items: const <DropdownMenuItem<String>>[
                        DropdownMenuItem<String>(
                          value: 'male',
                          child: Text('Male'),
                        ),
                        DropdownMenuItem<String>(
                          value: 'female',
                          child: Text('Female'),
                        )
                      ],
                      onChanged: (dynamic value) {
                        setState(() {
                          gender = value.toString();
                        });
                      },
                    ),
                    const SizedBox(
                      height: 30.0,
                    ),
                    Container(
                      child: const Text(
                        'Address',
                        style: TextStyle(),
                      ),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    FormBuilderTextField(
                      attribute: 'address',
                      maxLength: 50,
                      maxLines: 4,
                      validators: [
                        (dynamic value) {
                          if (value.toString().trim().isEmpty) {
                            return 'It cannot be empty';
                          }
                        }
                      ],
                      autovalidate: true,
                      initialValue: profilePageStore.loggedInUser.address,
                      decoration: InputDecoration(
                        counterText: '',
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 50.0,
                    ),
                    const SizedBox(
                      height: 30.0,
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
