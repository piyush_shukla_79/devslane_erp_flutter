import 'package:devslane_erp_flutter/models/user.dart';
import 'package:devslane_erp_flutter/presentations/customs/store_observer.dart';
import 'package:devslane_erp_flutter/stores/calendar_store.dart';
import 'package:devslane_erp_flutter/stores/user_store.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class EventsDialog extends StatelessWidget {
  EventsDialog({
    this.dateTime,
  });

  final DateTime dateTime;

  final DateFormat dateFormat = DateFormat('yyyy-MM-dd');

  void fetchEvents(BuildContext context) {
    final User user = Provider.of<UserStore>(context).loggedInUser;
    final CalendarStore calendarStore = Provider.of<CalendarStore>(context);
    if (user != null) {
      if (calendarStore.eventsMap
          .containsKey(DateFormat('yyyy-MM').format(dateTime))) {
        calendarStore.getAllEvents(dateTime, user.id);
      } else {
        calendarStore.fetchEventsData(month: dateTime, id: user.id);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    fetchEvents(context);
    return StoreObserver<CalendarStore>(
      builder: (CalendarStore calendarStore, BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          child: Container(
            height: SizeConfig.screenHeight * 0.6,
            margin: const EdgeInsets.all(5.0),
            child: Column(
              children: <Widget>[
                Container(
                  height: 50.0,
                  alignment: Alignment.center,
                  child: Text(
                    (dateTime.day == DateTime.now().day &&
                            dateTime.month == DateTime.now().month &&
                            dateTime.year == DateTime.now().year)
                        ? 'Today'
                        : DateFormat.yMMMMd().format(dateTime),
                    style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const Divider(),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.all(10.0),
                    child: calendarStore.isLoading ||
                            calendarStore.eventsMap[DateFormat('yyyy-MM').format(dateTime)] ==
                                null
                        ? const Center(
                            child: CircularProgressIndicator(),
                          )
                        : calendarStore.eventsMap[DateFormat('yyyy-MM').format(dateTime)].dateMap ==
                                    null ||
                                calendarStore
                                    .eventsMap[
                                        DateFormat('yyyy-MM').format(dateTime)]
                                    .dateMap
                                    .isEmpty ||
                                calendarStore.eventsMap[DateFormat('yyyy-MM').format(dateTime)].dateMap[dateFormat.format(dateTime)] ==
                                    null
                            ? const Center(
                                child: Text('No events for this day'),
                              )
                            : calendarStore
                                        .eventsMap[DateFormat('yyyy-MM')
                                            .format(dateTime)]
                                        .dateMap[dateFormat.format(dateTime)][0]
                                        .type ==
                                    'holiday'
                                ? _holidayColumn(calendarStore
                                    .eventsMap[DateFormat('yyyy-MM').format(dateTime)]
                                    .dateMap[dateFormat.format(dateTime)][0]
                                    .request
                                    .description)
                                : ListView.builder(
                                    itemCount: calendarStore
                                        .eventsMap[DateFormat('yyyy-MM')
                                            .format(dateTime)]
                                        .dateMap[dateFormat.format(dateTime)]
                                        .length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return calendarStore
                                                  .eventsMap[
                                                      DateFormat('yyyy-MM')
                                                          .format(dateTime)]
                                                  .dateMap[dateFormat
                                                      .format(dateTime)][index]
                                                  .type ==
                                              'holiday'
                                          ? _holidayColumn(calendarStore
                                              .eventsMap[DateFormat('yyyy-MM')
                                                  .format(dateTime)]
                                              .dateMap[dateFormat
                                                  .format(dateTime)][index]
                                              .request
                                              .description)
                                          : Column(
                                              children: <Widget>[
                                                _dataRow(
                                                    'Request Type',
                                                    calendarStore
                                                        .eventsMap[DateFormat(
                                                                'yyyy-MM')
                                                            .format(dateTime)]
                                                        .dateMap[dateFormat
                                                                .format(
                                                                    dateTime)]
                                                            [index]
                                                        .type),
                                                _dataRow(
                                                    'Reason',
                                                    calendarStore
                                                        .eventsMap[DateFormat(
                                                                'yyyy-MM')
                                                            .format(dateTime)]
                                                        .dateMap[dateFormat
                                                                .format(
                                                                    dateTime)]
                                                            [index]
                                                        .request
                                                        .description),
                                                _dataRow(
                                                    'Status',
                                                    calendarStore
                                                        .eventsMap[DateFormat(
                                                                'yyyy-MM')
                                                            .format(dateTime)]
                                                        .dateMap[dateFormat
                                                                .format(
                                                                    dateTime)]
                                                            [index]
                                                        .request
                                                        .status),
                                                calendarStore
                                                            .eventsMap[DateFormat(
                                                                    'yyyy-MM')
                                                                .format(
                                                                    dateTime)]
                                                            .dateMap[dateFormat
                                                                    .format(dateTime)]
                                                                [index]
                                                            .request
                                                            .time ==
                                                        null
                                                    ? const SizedBox()
                                                    : _dataRow(
                                                        'Time',
                                                        DateFormat.jm().format(
                                                            DateTime.parse(calendarStore
                                                                        .eventsMap[DateFormat('yyyy-MM').format(dateTime)]
                                                                        .dateMap[dateFormat.format(dateTime)][index]
                                                                        .request
                                                                        .time +
                                                                    'Z')
                                                                .toLocal())),
                                                const Divider()
                                              ],
                                            );
                                    },
                                  ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _dataRow(String header, String response) {
    return response == null
        ? const SizedBox()
        : Container(
            margin: const EdgeInsets.all(10.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Center(
                    child: Text(
                      header + ':',
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Center(
                    child: Text(
                      response,
                      style: const TextStyle(fontSize: 14),
                    ),
                  ),
                )
              ],
            ),
          );
  }

  Widget _holidayColumn(String response) {
    return Column(
      children: <Widget>[
        const Center(
          child: Text(
            'Holiday',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        const SizedBox(
          height: 10.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Container(
                margin: const EdgeInsets.all(10.0),
                child: const Center(
                  child: Text(
                    'Reason:',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                margin: const EdgeInsets.all(10.0),
                child: Center(
                  child: Text(
                    response,
                  ),
                ),
              ),
            )
          ],
        ),
        const Divider()
      ],
    );
  }
}
