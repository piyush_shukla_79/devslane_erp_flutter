import 'package:devslane_erp_flutter/models/request.dart';
import 'package:devslane_erp_flutter/presentations/customs/store_observer.dart';
import 'package:devslane_erp_flutter/stores/request_store.dart';
import 'package:devslane_erp_flutter/stores/user_store.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:devslane_erp_flutter/utils/string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_section_table_view/flutter_section_table_view.dart';
import 'package:intl/intl.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';
import 'package:timeago/timeago.dart' as timeago;

class RequestsPage extends StatefulWidget {
  const RequestsPage(
      {this.requestList,
      this.approvedRequests,
      this.pendingRequests,
      this.onFetch,
      this.hasMore,
      this.isAll,
      this.isLoading});

  static const String routeNamed = 'NotificationPage';
  final List<Request> pendingRequests;
  final List<Request> approvedRequests;
  final List<Request> requestList;
  final Function onFetch;
  final bool hasMore;
  final bool isLoading;
  final bool isAll;

  @override
  _RequestsPageState createState() => _RequestsPageState();
}

class _RequestsPageState extends State<RequestsPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  RefreshController refreshController = RefreshController();
  String description;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Stack(
      children: <Widget>[
        Container(
          width: SizeConfig.screenWidth,
          height: SizeConfig.screenHeight,
          margin: const EdgeInsets.only(left: 15.0, right: 15.0, top: 20.0),
          child: StoreObserver<RequestStore>(
            builder: (RequestStore requestStore, BuildContext context) {
              return widget.requestList.isEmpty
                  ? const Center(
                      child: Text('No Requests Found'),
                    )
                  : SectionTableView(
                      refreshController: refreshController,
                      enablePullDown: false,
                      enablePullUp: widget.hasMore,
                      onRefresh: (bool b) {
                        if (!widget.isLoading) {
                          widget.onFetch();
                        }
                        when((_) => !(requestStore.isFetching), () {
                          refreshController.sendBack(
                              b, RefreshStatus.completed);
                        });
                      },
                      cellAtIndexPath: (int p, int q) {
                        return requestCard(
                          request: p == 0
                              ? widget.pendingRequests.toList()[q]
                              : widget.approvedRequests.toList()[q],
                          requestStore: requestStore,
                        );
                      },
                      sectionCount: 2,
                      headerInSection: (int section) {
                        if (section == 0 && widget.pendingRequests.isNotEmpty)
                          return _getSectionHeader('Pending Requests');
                        if (section == 1 && widget.approvedRequests.isNotEmpty)
                          return _getSectionHeader('Attended Requests');
                        return Container();
                      },
                      numOfRowInSection: (int row) {
                        if (row == 0)
                          return widget.pendingRequests.length;
                        else
                          return widget.approvedRequests.length;
                      },
                    );
            },
          ),
        ),
      ],
    );
  }

  Widget showInOutTime({String time, String inOutText}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          inOutText,
          style: const TextStyle(fontSize: 15.0),
        ),
        const SizedBox(
          height: 10.0,
        ),
        Text(
          // ignore: prefer_if_null_operators
          time == null ? '' : DateFormat.yMd().format(DateTime.parse(time)),
          style: const TextStyle(
            fontSize: 15.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }

  Widget requestCard({Request request, RequestStore requestStore}) {
    return Card(
      elevation: 6,
      margin: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
      child: Stack(
        children: <Widget>[
          Container(
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      const CircleAvatar(
                        backgroundImage: AssetImage(
                          'assets/user.png',
                        ),
                        radius: 35.0,
                      ),
                      const SizedBox(
                        height: 15.0,
                      ),
                      Text(
                        _test(
                          request.createdAt.toString(),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  width: 10.0,
                ),
                Expanded(
                  flex: 8,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "${request.userId == requestStore.adminUser.id ? 'You' : request.getUser.fullName}" +
                              _getText(request, requestStore) +
                              request.requestTypeString,
                          style: const TextStyle(
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          request.requestType == 'half_day'
                              ? Row(
                                  children: <Widget>[
                                    showInOutTime(
                                        time: request.startDate,
                                        inOutText: 'In'),
                                    const SizedBox(
                                      width: 15.0,
                                    ),
                                    showInOutTime(
                                        time: request.endDate,
                                        inOutText: 'Out'),
                                  ],
                                )
                              : Row(
                                  children: <Widget>[
                                    showInOutTime(
                                        time: request.startDate,
                                        inOutText: 'from'),
                                    const SizedBox(
                                      width: 15.0,
                                    ),
                                    showInOutTime(
                                        time: request.endDate, inOutText: 'to'),
                                  ],
                                ),
                          Container(
                            child: Text(
                              'Description : ${request.description}',
                              style: const TextStyle(
                                fontSize: 15.0,
                              ),
                            ),
                          ),
                          showActionButtons(request)
                              ? Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Expanded(
                                      flex: 5,
                                      child: acceptRejectButton(
                                        text: 'Accept',
                                        request: request,
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 10.0,
                                    ),
                                    Expanded(
                                      flex: 5,
                                      child: acceptRejectButton(
                                        text: 'Reject',
                                        request: request,
                                      ),
                                    ),
                                  ],
                                )
                              : Container(),
                          const SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          request.actionPerformed
              ? Positioned(
                  bottom: 0,
                  right: 0,
                  child: Container(
                    height: 30.0,
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    decoration: BoxDecoration(
                      color: request.status == 'approved'
                          ? Colors.blue
                          : Colors.red,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10.0),
                      ),
                    ),
                    child: Center(
                      child: Text(
                        request.status == 'approved' ? 'approved' : 'rejected',
                        style: const TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                )
              : const SizedBox()
        ],
      ),
    );
  }

  bool showActionButtons(Request request) {
    final UserStore userStore = Provider.of<UserStore>(context);
    return !request.actionPerformed &&
        (userStore.loggedInUser.role == 'admin' ||
            userStore.loggedInUser.role == 'owner') &&
        request.userId != userStore.loggedInUser.id;
  }

  void rejectRequest(Request request) {
    showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return Form(
          key: _formKey,
          child: SimpleDialog(
            titlePadding: const EdgeInsets.only(left: 20.0, right: 10.0),
            contentPadding: const EdgeInsets.all(20),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                const Text(StringValue.NOTE),
                IconButton(
                  icon: Icon(Icons.cancel),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )
              ],
            ),
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 1),
                    borderRadius: BorderRadius.circular(5.0)),
                child: TextFormField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: StringValue.NOTE,
                  ),
                  maxLength: 100,
                  onSaved: (String value) {
                    setState(() {
                      description = value;
                    });
                  },
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Reason required';
                    }
                    return null;
                  },
                  onEditingComplete: () {},
                ),
              ),
              const SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  FlatButton(
                    child: Text(
                      StringValue.CANCEL,
                      style: TextStyle(color: Colors.black26),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  StoreObserver<RequestStore>(
                    builder: (RequestStore requestStore, BuildContext context) {
                      return FlatButton(
                        child: requestStore.processRequest[request.id]
                            ? const SizedBox(
                                height: 20.0,
                                width: 20.0,
                                child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.blue),
                                ),
                              )
                            : Text(
                                StringValue.REJECT,
                                style: TextStyle(
                                    color: Theme.of(context).errorColor),
                              ),
                        onPressed: () {
                          _formKey.currentState.save();
                          if (_formKey.currentState.validate()) {
                            Provider.of<RequestStore>(context).rejectRequest(
                                request: request, note: description);
                            when(
                              (Reaction e) =>
                                  !requestStore.processRequest[request.id],
                              () {
                                Navigator.pop(context);
                              },
                            );
                          }
                        },
                      );
                    },
                  )
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  String _getText(Request request, RequestStore requestStore) {
    return request.actionPerformed
        ? ' had requested for '
        : request.userId == Provider.of<UserStore>(context).loggedInUser.id
            ? ' are requesting for '
            : ' is requesting for ';
  }

  Widget acceptRejectButton({
    String text,
    Request request,
  }) {
    bool isAccepting = false;
    return StoreObserver<RequestStore>(
      builder: (RequestStore requestStore, BuildContext context) {
        return RaisedButton(
          onPressed: () {
            isAccepting = true;
            if (text == 'Accept') {
              Provider.of<RequestStore>(context)
                  .acceptRequest(request: request);
            } else {
              if (!requestStore.processRequest[request.id])
                rejectRequest(request);
            }
          },
          color: const Color.fromRGBO(53, 124, 184, 1),
          shape: RoundedRectangleBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          child: Container(
            height: 30.0,
            child: Center(
              child: requestStore.processRequest[request.id] && isAccepting
                  ? const SizedBox(
                      height: 20.0,
                      width: 20.0,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                      ),
                    )
                  : Text(
                      text,
                      style: const TextStyle(color: Colors.white),
                    ),
            ),
          ),
        );
      },
    );
  }

  String _test(String time) {
    return timeago
        .format(DateTime.parse(time.replaceAll('/', '-') + 'Z').toLocal());
  }

  Widget _getSectionHeader(String headerText) {
    return Container(
      height: 60.0,
      margin: const EdgeInsets.only(left: 15.0, top: 20.0),
      alignment: Alignment.centerLeft,
      child: Text(
        headerText,
        style: const TextStyle(
          color: Colors.blue,
          fontSize: 22,
        ),
      ),
    );
  }
}
