import 'package:devslane_erp_flutter/presentations/forget_password_page.dart';
import 'package:devslane_erp_flutter/stores/user_store.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:devslane_erp_flutter/utils/string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

import 'customs/store_observer.dart';
import 'home_page.dart';

class LoginPage extends StatefulWidget {
  static const String routeNamed = 'SignUpPage';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  AppBar appBar;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool showObscureText = true;
  double appBarHeight;
  ReactionDisposer reactionDisposer;

  @override
  void initState() {
    super.initState();
    initReactionFunction(context);
    appBar = AppBar(
      backgroundColor: Colors.white,
      title: Container(
        margin: const EdgeInsets.symmetric(vertical: 5.0),
        child: Center(
          child: Image.asset(
            'assets/devslaneIcon.jpg',
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
    appBarHeight = appBar.preferredSize.height;
  }

  void initReactionFunction(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback(
      (Duration v) {
        final UserStore userStore = Provider.of<UserStore>(context);
        when((Reaction e) => userStore.isLoggedIn, () {
          _navigateToHomePage(context);
        });
        reactionDisposer = autorun(
          (Reaction e) {
            final UserStore userStore = Provider.of<UserStore>(context);
            if (userStore.isError) {
              _scaffoldKey.currentState
                ..removeCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: Text(userStore.errorDetail),
                    duration: Duration(seconds: 5),
                  ),
                );
            }
          },
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    reactionDisposer.call();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: true,
        appBar: appBar,
        body: SingleChildScrollView(
          child: Container(
            width: SizeConfig.screenWidth,
            height: SizeConfig.safeHeight - appBarHeight,
            margin: const EdgeInsets.symmetric(horizontal: 25.0),
            child: FormBuilder(
              key: _fbKey,
              autovalidate: false,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: SizeConfig.screenWidth,
                    child: const Text(
                      StringValue.EMAIL,
                      style: TextStyle(color: Colors.blue, fontSize: 18.0),
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    decoration: BoxDecoration(
                      color: Colors.black12,
                      borderRadius:
                          const BorderRadius.all(Radius.circular(10.0)),
                    ),
                    child: Center(
                      child: FormBuilderTextField(
                        attribute: 'email',
                        keyboardType: TextInputType.emailAddress,
                        validators: [
                          FormBuilderValidators.required(),
                          FormBuilderValidators.email()
                        ],
                        decoration: InputDecoration(border: InputBorder.none),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    width: SizeConfig.screenWidth,
                    child: const Text(
                      StringValue.PASSWORD,
                      style: TextStyle(color: Colors.blue, fontSize: 18.0),
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    decoration: BoxDecoration(
                      color: Colors.black12,
                      borderRadius:
                          const BorderRadius.all(Radius.circular(10.0)),
                    ),
                    child: Center(
                      child: FormBuilderTextField(
                        attribute: 'password',
                        obscureText: showObscureText,
                        textInputAction: TextInputAction.done,
                        onFieldSubmitted: (n) {
                          onLogin(Provider.of<UserStore>(context));
                        },
                        validators: [
                          FormBuilderValidators.required(
                              errorText: 'This field is required')
                        ],
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                if (showObscureText) {
                                  showObscureText = false;
                                } else
                                  showObscureText = true;
                              });
                            },
                            icon: Icon(
                              Icons.remove_red_eye,
                              color:
                                  showObscureText ? Colors.white : Colors.blue,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  GestureDetector(
                    onTap: () {
                      Provider.of<UserStore>(context).changePassword();
                      Navigator.pushNamed(
                          context, ForgetPasswordPage.routeNamed);
                    },
                    child: Container(
                      child: const Text(
                        'Forgot Password?',
                        style: TextStyle(color: Colors.blue),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  StoreObserver<UserStore>(
                    builder: (UserStore userStore, BuildContext context) {
                      return RaisedButton(
                        color: const Color.fromRGBO(50, 152, 219, 1),
                        shape: RoundedRectangleBorder(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(10.0),
                          ),
                        ),
                        onPressed: () {
                          onLogin(userStore);
                        },
                        child: Container(
                          height: 50.0,
                          child: Center(
                            child: userStore.isLoggingIn
                                ? CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white),
                                  )
                                : Text(
                                    'LOGIN',
                                    style: TextStyle(color: Colors.white),
                                  ),
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget bubbleContainer(double size) {
    return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(
        color: const Color.fromRGBO(154, 204, 237, 1),
        borderRadius: BorderRadius.circular(size / 2),
      ),
    );
  }

  void onLogin(UserStore userStore) {
    _fbKey.currentState.save();
    if (_fbKey.currentState.validate() && !userStore.isLoggingIn) {
      userStore.setIsLoggingIn(loginDetails: _fbKey.currentState.value);
    }
  }

  void _navigateToHomePage(BuildContext context) {
    Navigator.pushReplacementNamed(context, HomePage.routeNamed);
  }
}
