import 'package:devslane_erp_flutter/models/daily_update.dart';
import 'package:devslane_erp_flutter/presentations/function/add_dailyupdate.dart';
import 'package:devslane_erp_flutter/presentations/function/dialog_filter.dart';
import 'package:devslane_erp_flutter/stores/daily_update_store.dart';
import 'package:devslane_erp_flutter/stores/employee_store.dart';
import 'package:devslane_erp_flutter/stores/user_store.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:devslane_erp_flutter/utils/string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html_view/flutter_html_view.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'customs/page_icon_chipset.dart';
import 'customs/store_observer.dart';

class DailyUpdatesPage extends StatefulWidget {
  static const String routeNamed = 'DailyUpdate';

  @override
  _DailyUpdatesPageState createState() => _DailyUpdatesPageState();
}

class _DailyUpdatesPageState extends State<DailyUpdatesPage> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((v) {
      final DailyUpdateStore dailyUpdateStore =
          Provider.of<DailyUpdateStore>(context);
      if (Provider.of<UserStore>(context).isLoggedIn &&
          dailyUpdateStore.getGroupedDailyUpdates.isEmpty &&
          dailyUpdateStore.hasMore) {
        dailyUpdateStore.fetchData();
      }
    });
  }

  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      height: SizeConfig.screenHeight,
      width: SizeConfig.screenWidth,
      color: Colors.white,
      child: Stack(
        children: <Widget>[
          StoreObserver<DailyUpdateStore>(
            builder: (DailyUpdateStore dailyUpdateStore, BuildContext context) {
              return Container(
                margin: const EdgeInsets.only(top: 40.0),
                width: SizeConfig.screenWidth,
                child: dailyUpdateStore.isLoading
                    ? const Center(
                        child: CircularProgressIndicator(),
                      )
                    : dailyUpdateStore.getGroupedDailyUpdates.isEmpty
                        ? const Center(
                            child: Text(
                              'No updates Found',
                              style: TextStyle(fontSize: 16),
                            ),
                          )
                        : Container(
                            height: SizeConfig.screenHeight,
                            child: ListView.builder(
                              controller: scrollController,
                              itemCount: dailyUpdateStore
                                  .getGroupedDailyUpdates.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Column(
                                  children: <Widget>[
                                    sectionHeader(
                                        section: index,
                                        dailyUpdateStore: dailyUpdateStore),
                                    ListView.builder(
                                      controller: scrollController,
                                      shrinkWrap: true,
                                      itemCount: dailyUpdateStore
                                          .getGroupedDailyUpdates.values
                                          .toList()[index]
                                          .length,
                                      itemBuilder:
                                          (BuildContext context, int row) {
                                        if (index ==
                                                dailyUpdateStore
                                                        .getGroupedDailyUpdates
                                                        .length -
                                                    1 &&
                                            row ==
                                                dailyUpdateStore
                                                        .getGroupedDailyUpdates
                                                        .values
                                                        .toList()[index]
                                                        .length -
                                                    1) {
                                          _fetchMore(context, dailyUpdateStore);
                                        }
                                        return Column(
                                          children: <Widget>[
                                            sectionRow(
                                                dailyUpdates: dailyUpdateStore
                                                    .getGroupedDailyUpdates
                                                    .values
                                                    .toList()[index][row]),
                                            showLoader(row, index,
                                                    dailyUpdateStore)
                                                ? Container(
                                                    height: 40.0,
                                                    child: const Center(
                                                      child:
                                                          CircularProgressIndicator(),
                                                    ),
                                                  )
                                                : const SizedBox()
                                          ],
                                        );
                                      },
                                    ),
                                  ],
                                );
                              },
                            ),
                          ),
              );
            },
          ),
          PageIcon(
            iconData: Icons.access_time,
          ),
          StoreObserver<DailyUpdateStore>(
            builder: (DailyUpdateStore dailyUpdateStore, BuildContext context) {
              return dailyUpdateStore.showFilter
                  ? Align(
                      alignment: Alignment.topRight,
                      child: GestureDetector(
                        onTap: () => _filterUpdate(context),
                        child: Container(
                          margin: EdgeInsets.only(
                              top: SizeConfig.screenHeight * 0.05, right: 10),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                            color: Colors.white,
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                color: Colors.black.withOpacity(0.1),
                                offset: const Offset(1.0, 6.0),
                                blurRadius: 10.0,
                              ),
                            ],
                          ),
                          height: SizeConfig.screenHeight * 0.045,
                          width: SizeConfig.screenWidth * 0.32,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              const Text(
                                StringValue.FILTER,
                                style: TextStyle(
                                  color: Colors.blue,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                              Icon(
                                Icons.filter_list,
                                color: const Color.fromRGBO(53, 124, 184, 1),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  : Container();
            },
          ),
          Positioned(
            bottom: 20,
            right: 20,
            child: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () {
                Navigator.pushNamed(context, AddDailyUpdates.routeNamed);
              },
            ),
          ),
        ],
      ),
    );
  }

  void _filterUpdate(BuildContext context) {
    showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return StoreObserver<EmployeeStore>(
          builder: (EmployeeStore employeeStore, BuildContext context) {
            if (employeeStore.employeeList.isEmpty ||
                employeeStore.employeeList == null) {
              employeeStore.fetchData();
            }
            return FilterDialog(
              employeeStore.employeeList.toList(),
              employeeStore.isLoading,
            );
          },
        );
      },
    );
  }

  Widget sectionHeader({int section, DailyUpdateStore dailyUpdateStore}) {
    final bool isToday = DateTime.now()
            .toUtc()
            .difference(DateTime.parse(dailyUpdateStore
                    .getGroupedDailyUpdates.keys
                    .toList()[section])
                .toUtc())
            .inDays ==
        0;
    final bool isYesterday = DateTime.now()
            .toUtc()
            .difference(DateTime.parse(dailyUpdateStore
                    .getGroupedDailyUpdates.keys
                    .toList()[section])
                .toUtc())
            .inDays ==
        1;
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.only(
        left: 15.0,
        right: 15.0,
        bottom: 10.0,
        top: 35.0,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                isToday
                    ? 'Today'
                    : isYesterday
                        ? 'Yesterday'
                        : _test(dailyUpdateStore.getGroupedDailyUpdates.keys
                            .toList()[section]),
                style: const TextStyle(
                    color: Colors.blue,
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          const SizedBox(
            height: 10.0,
          ),
          const Divider(
            color: Colors.blue,
          )
        ],
      ),
    );
  }

  Widget sectionRow({DailyUpdates dailyUpdates, int section, int row}) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 15.0),
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15.0),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              dailyUpdates.getUser.fullName,
              style: const TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            width: SizeConfig.screenWidth,
            child: HtmlView(
              scrollable: false,
              data: dailyUpdates.message,
            ),
          ),
        ],
      ),
    );
  }

  String _test(String time) {
    String result;
    if (DateTime.parse(time).difference(DateTime.now()).inDays == 0) {
      result = 'Today';
    } else if (DateTime.parse(time).difference(DateTime.now()).inDays == 1) {
      result = 'Yesterday';
    } else {
      result = DateFormat.yMMMMd().format(DateTime.parse(time));
    }
    return result;
  }

  void _fetchMore(BuildContext context, DailyUpdateStore dailyUpdateStore) {
    if (!dailyUpdateStore.isFetching) {
      if (dailyUpdateStore.isFilterApplied) {
        dailyUpdateStore.fetchData(
          length: dailyUpdateStore.filteredDailyUpdates.length + 20,
        );
      } else {
        dailyUpdateStore.fetchData(
          length: dailyUpdateStore.dailyUpdates.length + 20,
        );
      }
    }
  }

  bool showLoader(int row, int section, DailyUpdateStore dailyUpdateStore) {
    return (dailyUpdateStore.isFilterApplied
            ? dailyUpdateStore.hasMoreFilterUpdates
            : dailyUpdateStore.hasMore) &&
        dailyUpdateStore.getGroupedDailyUpdates.isNotEmpty &&
        dailyUpdateStore.isFetching &&
        section == dailyUpdateStore.getGroupedDailyUpdates.length - 1 &&
        row ==
            dailyUpdateStore.getGroupedDailyUpdates.values
                    .toList()[section]
                    .length -
                1;
  }
}
