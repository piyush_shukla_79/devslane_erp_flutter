import 'package:flutter/material.dart';

class ChipSet extends StatelessWidget {
  const ChipSet({
    @required this.child,
    this.color = Colors.grey,
    this.borderRadius = const BorderRadius.all(Radius.circular(20.0)),
  });

  final Widget child;
  final Color color;
  final BorderRadius borderRadius;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      decoration: BoxDecoration(
        color: Colors.black12,
        borderRadius: borderRadius,
      ),
      child: child,
    );
  }
}
