import 'package:flutter/material.dart';

class PageIcon extends StatelessWidget {
  const PageIcon({this.iconData});

  final IconData iconData;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Container(
        height: 60.0,
        width: 55.0,
        padding: const EdgeInsets.all(5.0),
        decoration: const BoxDecoration(
          color: Color.fromRGBO(53, 124, 184, 1),
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(30.0),
          ),
        ),
        child: Icon(
          iconData,
          color: Colors.white,
        ),
      ),
    );
  }
}
