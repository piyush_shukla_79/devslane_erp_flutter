import 'package:devslane_erp_flutter/models/user.dart';
import 'package:devslane_erp_flutter/presentations/customs/store_observer.dart';
import 'package:devslane_erp_flutter/presentations/events_dialog.dart';
import 'package:devslane_erp_flutter/stores/calendar_store.dart';
import 'package:devslane_erp_flutter/stores/user_store.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:provider/provider.dart';

import 'customs/page_icon_chipset.dart';

class CalendarPage extends StatefulWidget {
  static const String routeNamed = 'CalendarPage';

  @override
  CalendarPageState createState() => CalendarPageState();
}

class CalendarPageState extends State<CalendarPage> {
  CalendarPageState() {
    _dateTime = DateTime.now();
    _setMonthDate();
  }

  DateTime _dateTime;
  DateTime _selectedDate;
  final DateFormat _dateFormat = DateFormat.yMMMM();
  int _beginMonthDate = 0;
  final int weekDays = 7;
  final ScrollController _scrollController = ScrollController();
  DateFormat newDateFormat;

  void fetchEvents(BuildContext context) {
    final CalendarStore calendarStore = Provider.of<CalendarStore>(context);
    final User user = Provider.of<UserStore>(context).loggedInUser;
    if (user != null) {
      if (calendarStore.eventsMap
          .containsKey(DateFormat('yyyy-MM').format(_dateTime))) {
        calendarStore.getAllEvents(_dateTime, user.id);
      } else {
        calendarStore.fetchEventsData(month: _dateTime, id: user.id);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    fetchEvents(context);
    SizeConfig().init(context);
    return Stack(
      children: <Widget>[
        Provider.of<UserStore>(context).loggedInUser == null
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  margin: const EdgeInsets.all(10.0),
                  child: Column(
                    children: <Widget>[
                      const SizedBox(
                        height: 20.0,
                      ),
                      Container(
                        margin: const EdgeInsets.all(5.0),
                        child: Column(
                          children: <Widget>[
                            const Text(
                              'Employee:',
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            ),
                            Container(
                              margin: const EdgeInsets.all(5.0),
                              padding: const EdgeInsets.all(5.0),
                              child: Text(
                                Provider.of<UserStore>(context)
                                    .loggedInUser
                                    .fullName,
                                maxLines: null,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(53, 124, 184, 1),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.all(5.0),
                        child: Column(
                          children: <Widget>[
                            const Text(
                              'Select Month:',
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            ),
                            GestureDetector(
                              onTap: _selectMonth,
                              child: Container(
                                margin: const EdgeInsets.all(5.0),
                                width: SizeConfig.screenWidth * 0.50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(color: Colors.black),
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        margin: const EdgeInsets.all(5.0),
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 5.0),
                                        child:
                                            Text(_dateFormat.format(_dateTime)),
                                      ),
                                    ),
                                    IconButton(
                                      icon: Icon(Icons.calendar_today),
                                      onPressed: _selectMonth,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 10.0,
                            ),
                            const Divider(
                              height: 1.0,
                            ),
                            Container(
                              margin: const EdgeInsets.all(10.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    _dateFormat.format(_dateTime),
                                    style: const TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      InkWell(
                                          child: const Text(
                                            'Previous',
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          onTap: () => _showPreviousMonth()),
                                      InkWell(
                                          child: const Text(
                                            'Today',
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          onTap: () => _showToday()),
                                      InkWell(
                                          child: const Text(
                                            'Next',
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          onTap: () => _showNextMonth())
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      const Divider(
                        height: 1.0,
                      ),
                      StoreObserver<CalendarStore>(
                        builder: (
                          CalendarStore calendarStore,
                          BuildContext context,
                        ) {
                          return calendarStore.isLoading ||
                                  calendarStore.eventsMap[DateFormat('yyyy-MM')
                                          .format(_dateTime)] ==
                                      null
                              ? Container(
                                  height: 80.0,
                                  child: const Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                )
                              : Container(
                                  margin: const EdgeInsets.only(
                                      top: 10.0, bottom: 10.0),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: const <Widget>[
                                          Text('Sun'),
                                          Text('Mon'),
                                          Text('Tue'),
                                          Text('Wed'),
                                          Text('Thu'),
                                          Text('Fri'),
                                          Text('Sat')
                                        ],
                                      ),
                                      const Divider(
                                        height: 1.0,
                                      ),
                                      const SizedBox(
                                        height: 10.0,
                                      ),
                                      GridView.count(
                                        mainAxisSpacing: 5,
                                        crossAxisSpacing: 5,
                                        controller: _scrollController,
                                        crossAxisCount: weekDays,
                                        scrollDirection: Axis.vertical,
                                        shrinkWrap: true,
                                        // ignore: always_specify_types
                                        children: List.generate(
                                          _getNumOfDaysInMonth(_dateTime.month),
                                          (int index) {
                                            final int dayNumber = index + 1;

                                            return InkWell(
                                              onTap: () =>
                                                  _showUsersDialog(dayNumber),
                                              child: Container(
                                                child: _getDayCount(dayNumber),
                                              ),
                                            );
                                          },
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 20.0,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            width:
                                                SizeConfig.screenWidth * 0.60,
                                            child: const Text(
                                              'Total leaves this month:',
                                              style: TextStyle(fontSize: 16),
                                            ),
                                          ),
                                          Text(
                                            calendarStore
                                                .eventsMap[DateFormat('yyyy-MM')
                                                    .format(_dateTime)]
                                                .leaves
                                                .toString(),
                                            style: const TextStyle(
                                              fontSize: 16,
                                            ),
                                          )
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10.0,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            width:
                                                SizeConfig.screenWidth * 0.60,
                                            child: const Text(
                                              'Total holidays this month:',
                                              style: TextStyle(fontSize: 16),
                                            ),
                                          ),
                                          Text(
                                            calendarStore
                                                .eventsMap[DateFormat('yyyy-MM')
                                                    .format(_dateTime)]
                                                .holidays
                                                .toString(),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                );
                        },
                      )
                    ],
                  ),
                ),
              ),
        PageIcon(
          iconData: Icons.calendar_today,
        )
      ],
    );
  }

  void _setMonthDate() {
    _beginMonthDate = DateTime(_dateTime.year, _dateTime.month, 1).weekday;
    _beginMonthDate = _beginMonthDate % 7;
  }

  void _showToday() {
    setState(() {
      _dateTime = DateTime.now();
    });
    _setMonthDate();
  }

  void _showPreviousMonth() {
    setState(() {
      if (_dateTime.month == DateTime.january) {
        _dateTime = DateTime(_dateTime.year - 1, DateTime.december);
      } else {
        _dateTime = DateTime(_dateTime.year, _dateTime.month - 1);
      }
    });
    _setMonthDate();
  }

  void _showNextMonth() {
    setState(() {
      if (_dateTime.month == DateTime.december) {
        _dateTime = DateTime(_dateTime.year + 1, DateTime.january);
      } else {
        _dateTime = DateTime(_dateTime.year, _dateTime.month + 1);
      }
    });
    _setMonthDate();
  }

  Widget _getDayCount(int dayNumber) {
    if (dayNumber - _beginMonthDate == DateTime.now().day &&
        _dateTime.month == DateTime.now().month &&
        _dateTime.year == DateTime.now().year) {
      return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: const Color.fromRGBO(53, 124, 184, 1),
        ),
        child: Center(
          child: Text(
            (dayNumber - _beginMonthDate).toString(),
          ),
        ),
      );
    } else {
      return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          border: Border.all(
              color: dayNumber <= _beginMonthDate
                  ? Colors.transparent
                  : Colors.black,
              style: BorderStyle.solid),
        ),
        child: Center(
          child: Text((dayNumber <= _beginMonthDate)
              ? ''
              : (dayNumber - _beginMonthDate).toString()),
        ),
      );
    }
  }

  int _getNumOfDaysInMonth(int month) {
    int numOfDays = 28;

    switch (month) {
      case 1:
        numOfDays = 31;
        break;
      case 2:
        numOfDays = (_dateTime.year % 100 == 0)
            ? (_dateTime.year % 400 == 0) ? 29 : 28
            : (_dateTime.year % 4 == 0) ? 29 : 28;
        break;
      case 3:
        numOfDays = 31;
        break;
      case 4:
        numOfDays = 30;
        break;
      case 5:
        numOfDays = 31;
        break;
      case 6:
        numOfDays = 30;
        break;
      case 7:
        numOfDays = 31;
        break;
      case 8:
        numOfDays = 31;
        break;
      case 9:
        numOfDays = 30;
        break;
      case 10:
        numOfDays = 31;
        break;
      case 11:
        numOfDays = 30;
        break;
      case 12:
        numOfDays = 31;
        break;
      default:
        numOfDays = 28;
    }
    return numOfDays + _beginMonthDate;
  }

  // ignore: avoid_void_async
  void _selectMonth() async {
    final DateTime picked = await showMonthPicker(
      context: context,
      // ignore: prefer_if_null_operators
      initialDate: _dateTime == null ? DateTime.now() : _dateTime,
      firstDate: DateTime(2010),
      lastDate: DateTime(DateTime.now().year + 100),
    );
    if (picked != null) {
      setState(() {
        _dateTime = picked;
        _setMonthDate();
      });
    }
  }

  void _showUsersDialog(int dayNumber) {
    _selectedDate =
        DateTime(_dateTime.year, _dateTime.month, dayNumber - _beginMonthDate);
    setState(() {
      showDialog<void>(
          context: context,
          builder: (BuildContext context) {
            return EventsDialog(
              dateTime: _selectedDate,
            );
          });
    });
  }
}
