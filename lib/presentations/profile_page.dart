import 'package:devslane_erp_flutter/presentations/customs/page_icon_chipset.dart';
import 'package:devslane_erp_flutter/presentations/profile_edit_page.dart';
import 'package:devslane_erp_flutter/stores/holiday_store.dart';
import 'package:devslane_erp_flutter/stores/user_store.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'customs/store_observer.dart';

class ProfilePage extends StatelessWidget {
  static const String routeNamed = 'Home';
  final ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    if (Provider.of<UserStore>(context).isLoggedIn) {
      Provider.of<UserStore>(context).fetchUserDetails();
      Provider.of<HolidayStore>(context).fetchData();
    }
    return StoreObserver<UserStore>(
      builder: (UserStore profilePageStore, BuildContext context) {
        return (profilePageStore.isLoading)
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : profilePageStore.loggedInUser == null
                ? const Center(
                    child: Text(
                      'Some Error Occured',
                      style: TextStyle(fontSize: 16),
                    ),
                  )
                : Container(
                    height: SizeConfig.screenHeight,
                    width: SizeConfig.screenWidth,
                    child: Stack(
                      children: <Widget>[
                        SingleChildScrollView(
                          controller: scrollController,
                          child: Column(
                            children: <Widget>[
                              Container(
                                margin: const EdgeInsets.only(top: 60),
                                child: Stack(
                                  children: <Widget>[
                                    Card(
                                      margin: EdgeInsets.only(
                                        top: SizeConfig.screenHeight * 0.10,
                                        left: 15.0,
                                        right: 15.0,
                                      ),
                                      child: Container(
                                        width: SizeConfig.screenWidth,
                                        margin: const EdgeInsets.symmetric(
                                            horizontal: 10.0, vertical: 10.0),
                                        child: Column(
                                          children: <Widget>[
                                            SizedBox(
                                              height: SizeConfig.screenWidth /
                                                  4.458,
                                            ),
                                            Text(
                                              profilePageStore
                                                  .loggedInUser.fullName,
                                              style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 26.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            SizedBox(
                                              height: SizeConfig.screenWidth /
                                                  14.117,
                                            ),
                                            profilePageStore
                                                        .loggedInUser.email ==
                                                    null
                                                ? const SizedBox()
                                                : userDetails(
                                                    field: 'Email',
                                                    value: profilePageStore
                                                        .loggedInUser.email),
                                            profilePageStore
                                                        .loggedInUser.phone ==
                                                    null
                                                ? const SizedBox()
                                                : userDetails(
                                                    field: 'Phone',
                                                    value: profilePageStore
                                                        .loggedInUser.phone),
                                            profilePageStore
                                                        .loggedInUser.gender ==
                                                    null
                                                ? const SizedBox()
                                                : userDetails(
                                                    field: 'Gender',
                                                    value: profilePageStore
                                                        .loggedInUser.gender),
                                            profilePageStore
                                                        .loggedInUser.address ==
                                                    null
                                                ? const SizedBox()
                                                : userDetails(
                                                    field: 'Address',
                                                    value: profilePageStore
                                                        .loggedInUser.address),
                                            profilePageStore
                                                        .loggedInUser.role ==
                                                    null
                                                ? const SizedBox()
                                                : userDetails(
                                                    field: 'Role',
                                                    value: profilePageStore
                                                        .loggedInUser.role),
                                            profilePageStore.loggedInUser
                                                        .joiningDate ==
                                                    null
                                                ? const SizedBox()
                                                : userDetails(
                                                    field: 'Joining Date',
                                                    value: DateFormat(
                                                            'yyyy-MM-dd')
                                                        .format(DateTime.parse(
                                                            profilePageStore
                                                                .loggedInUser
                                                                .joiningDate)),
                                                  ),
                                            SizedBox(
                                              height: SizeConfig.screenWidth /
                                                  10.588,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.center,
                                      child: Container(
                                        height: SizeConfig.screenHeight * 0.2,
                                        width: SizeConfig.screenHeight * 0.2,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                              SizeConfig.screenWidth / 4.982),
                                          boxShadow: <BoxShadow>[
                                            BoxShadow(
                                                color: Colors.black12,
                                                spreadRadius: 3.0,
                                                blurRadius: 1.0)
                                          ],
                                          color: Colors.grey,
                                          border: Border.all(
                                              color: Colors.white, width: 6.0),
                                        ),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(
                                              SizeConfig.screenHeight * 0.1),
                                          child: profilePageStore
                                                      .loggedInUser.image ==
                                                  null
                                              ? Image.asset(
                                                  'assets/user.png',
                                                  fit: BoxFit.cover,
                                                )
                                              : Image.network(
                                                  profilePageStore
                                                      .loggedInUser.image,
                                                  fit: BoxFit.cover,
                                                  loadingBuilder:
                                                      (BuildContext context,
                                                          Widget child,
                                                          ImageChunkEvent
                                                              loadingProgress) {
                                                    if (loadingProgress ==
                                                        null) {
                                                      return child;
                                                    }
                                                    return Center(
                                                      child: CircularProgressIndicator(
                                                          value: loadingProgress
                                                                      .expectedTotalBytes !=
                                                                  null
                                                              ? loadingProgress
                                                                      .cumulativeBytesLoaded /
                                                                  loadingProgress
                                                                      .expectedTotalBytes
                                                              : null),
                                                    );
                                                  },
                                                ),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      top: SizeConfig.screenHeight * 0.12,
                                      right: SizeConfig.screenWidth / 7.353,
                                      child: FloatingActionButton(
                                        backgroundColor: Colors.white,
                                        onPressed: () {
                                          Navigator.pushNamed(context,
                                              ProfileEditPage.routeNamed);
                                        },
                                        child: Center(
                                          child: Icon(
                                            Icons.edit,
                                            color: const Color.fromRGBO(
                                                53, 124, 184, 1),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 25.0,
                              ),
                              StoreObserver<HolidayStore>(
                                builder: (HolidayStore holidayStore,
                                    BuildContext context) {
                                  return holidayStore.isLoading &&
                                          holidayStore.hasMore
                                      ? const CircularProgressIndicator()
                                      : holidayStore.todayHoliday().isEmpty ||
                                              holidayStore.todayHoliday() ==
                                                  null
                                          ? const Center(
                                              child: Text(
                                                  'There are no holidays this month'),
                                            )
                                          : Column(
                                              children: <Widget>[
                                                const Divider(),
                                                Container(
                                                  alignment: Alignment.center,
                                                  margin: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 15.0,
                                                      vertical: 10.0),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Container(
                                                        child: Align(
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          child: const Text(
                                                            'Upcoming Holidays',
                                                            style: TextStyle(
                                                              color:
                                                                  Colors.blue,
                                                              fontSize: 20.0,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                        height: 10.0,
                                                      ),
                                                      const Divider(
                                                        color: Colors.blue,
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                ListView.builder(
                                                  itemCount: holidayStore
                                                      .todayHoliday()
                                                      .length,
                                                  shrinkWrap: true,
                                                  controller: scrollController,
                                                  scrollDirection:
                                                      Axis.vertical,
                                                  itemBuilder:
                                                      (BuildContext context,
                                                          int index) {
                                                    return Card(
                                                      margin: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 10.0,
                                                          vertical: 5.0),
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10.0)),
                                                      child: Container(
                                                        margin: const EdgeInsets
                                                            .all(10.0),
                                                        child: Center(
                                                          child: Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              Row(
                                                                children: <
                                                                    Widget>[
                                                                  const Text(
                                                                    'Date : ',
                                                                    style:
                                                                        TextStyle(
                                                                      fontSize:
                                                                          20,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                    holidayStore
                                                                        .todayHoliday()[
                                                                            index]
                                                                        .date,
                                                                    style:
                                                                        const TextStyle(
                                                                      fontSize:
                                                                          20,
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                              Row(
                                                                children: <
                                                                    Widget>[
                                                                  const Text(
                                                                    'Reason : ',
                                                                    style:
                                                                        TextStyle(
                                                                      fontSize:
                                                                          20,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                    holidayStore
                                                                        .todayHoliday()[
                                                                            index]
                                                                        .description,
                                                                    style:
                                                                        const TextStyle(
                                                                      fontSize:
                                                                          20,
                                                                    ),
                                                                  )
                                                                ],
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    );
                                                  },
                                                ),
                                              ],
                                            );
                                },
                              )
                            ],
                          ),
                        ),
                        PageIcon(
                          iconData: Icons.home,
                        )
                      ],
                    ),
                  );
      },
    );
  }

  Widget userDetails({String field, String value}) {
    return Container(
      width: SizeConfig.screenWidth * 0.80,
      margin: const EdgeInsets.symmetric(vertical: 5.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: SizeConfig.screenHeight * 0.15,
            child: Text(
              field + ' : ',
              style: const TextStyle(
                color: Color.fromRGBO(53, 124, 184, 1),
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(
            width: 5.0,
          ),
          Flexible(
            child: Text(
              value,
              overflow: TextOverflow.clip,
              style: const TextStyle(fontSize: 18.0),
            ),
          )
        ],
      ),
    );
  }
}
