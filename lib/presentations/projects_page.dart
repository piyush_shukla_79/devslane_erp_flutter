import 'package:devslane_erp_flutter/models/projects.dart';
import 'package:devslane_erp_flutter/models/tags.dart';
import 'package:devslane_erp_flutter/stores/projects_store.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'customs/chip.dart';
import 'customs/page_icon_chipset.dart';
import 'customs/store_observer.dart';

class ProjectsPage extends StatefulWidget {
  static const String routeNamed = 'ProjectsPage';

  @override
  _ProjectsPageState createState() => _ProjectsPageState();
}

class _ProjectsPageState extends State<ProjectsPage> {
  final ScrollController controller = ScrollController();
  final TextEditingController _textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _textEditingController.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    final ProjectsStore projectsStore = Provider.of<ProjectsStore>(context);
    if (projectsStore.projects.isEmpty || projectsStore.projects == null) {
      projectsStore.fetchProjects();
    }
    SizeConfig().init(context);
    return Stack(
      children: <Widget>[
        Container(
          height: SizeConfig.screenHeight,
          width: SizeConfig.screenWidth,
          margin: const EdgeInsets.only(left: 15.0, right: 15.0, top: 55.0),
          child: SingleChildScrollView(
            controller: controller,
            child: Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10.0),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 1.0),
                  ),
                  padding: const EdgeInsets.symmetric(
                    vertical: 10.0,
                    horizontal: 15.0,
                  ),
                  child: TextField(
                    controller: _textEditingController,
                    decoration: InputDecoration.collapsed(
                      hintText: 'Search',
                    ),
                  ),
                ),
                StoreObserver<ProjectsStore>(
                  builder: (ProjectsStore projectStore, BuildContext context) {
                    return projectStore.isLoading
                        ? const Center(
                            child: CircularProgressIndicator(),
                          )
                        : projectsStore.projects == null ||
                                projectsStore.projects.isEmpty ||
                                searchResult(projectsStore).isEmpty
                            ? const Center(
                                child: Text('No data found'),
                              )
                            : ListView.builder(
                                itemCount: projectStore.projects.length,
                                shrinkWrap: true,
                                controller: controller,
                                itemBuilder: (BuildContext context, int index) {
                                  return projectStore.projects[index].title
                                          .toLowerCase()
                                          .contains(_textEditingController.text
                                              .toLowerCase())
                                      ? projectCard(
                                          projectName: projectStore
                                              .projects[index].title,
                                          date: projectStore
                                              .projects[index].incubationDate,
                                          description: projectStore
                                              .projects[index].description,
                                          logoUrl: projectStore
                                              .projects[index].logoUrl,
                                          tags: projectStore
                                              .projects[index].tagsMap,
                                        )
                                      : const SizedBox();
                                },
                              );
                  },
                )
              ],
            ),
          ),
        ),
        PageIcon(
          iconData: Icons.tv,
        )
      ],
    );
  }

  Widget projectCard({
    String projectName = 'Kinzy',
    String logoUrl,
    String description,
    String date = 'Jun 1, 2019',
    List<Tags> tags,
  }) {
    final List<Widget> widgetList = chipSetList(tags);
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: const BorderRadius.all(
          Radius.circular(15.0),
        ),
      ),
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: Container(
        margin: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Container(
                margin: const EdgeInsets.symmetric(vertical: 10.0),
                height: SizeConfig.screenHeight * 0.2,
                child: logoUrl == null
                    ? Image.asset(
                        'assets/devslaneIcon.jpg',
                        fit: BoxFit.fitWidth,
                      )
                    : Image.network(
                        logoUrl,
                        fit: BoxFit.fitWidth,
                        loadingBuilder: (BuildContext context, Widget child,
                            loadingProgress) {
                          if (loadingProgress == null) {
                            return child;
                          }
                          return Center(
                            child: CircularProgressIndicator(
                                value: loadingProgress.expectedTotalBytes !=
                                        null
                                    ? loadingProgress.cumulativeBytesLoaded /
                                        loadingProgress.expectedTotalBytes
                                    : null),
                          );
                        },
                      ),
              ),
            ),
            const SizedBox(
              height: 15.0,
            ),
            Text(
              projectName,
              style: TextStyle(
                color: Colors.grey,
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 10.0,
            ),
            description != null
                ? Text(
                    description,
                    style: TextStyle(color: Colors.grey, fontSize: 15.0),
                  )
                : const SizedBox(),
            const SizedBox(
              height: 10.0,
            ),
            Row(
              children: <Widget>[
                Text(
                  'Start Date:',
                  style: TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,
                  ),
                ),
                const SizedBox(
                  width: 5.0,
                ),
                Text(
                  date,
                  style: TextStyle(color: Colors.grey, fontSize: 15.0),
                ),
              ],
            ),
            const SizedBox(
              height: 15.0,
            ),
            Wrap(
              spacing: 5,
              runSpacing: 5,
              children: widgetList,
            ),
          ],
        ),
      ),
    );
  }

  Widget chipSet(String text) {
    return ChipSet(
      child: Text(
        text,
        style: TextStyle(color: Colors.black, fontSize: 15),
      ),
    );
  }

  List<Widget> chipSetList(List<Tags> tags) {
    List<Widget> list = List();
    tags.forEach((tag) {
      list.add(chipSet(tag.title));
    });
    return list;
  }

  List<Projects> searchResult(ProjectsStore projectsStore) {
    if (_textEditingController.text.isNotEmpty) {
      return projectsStore.projects.where((Projects projects) {
        return projects.title
            .toLowerCase()
            .contains(_textEditingController.text.toLowerCase());
      }).toList();
    } else {
      return projectsStore.projects;
    }
  }
}
