import 'package:devslane_erp_flutter/models/user.dart';
import 'package:devslane_erp_flutter/presentations/customs/store_observer.dart';
import 'package:devslane_erp_flutter/stores/daily_update_store.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:devslane_erp_flutter/utils/string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

@immutable
class FilterDialog extends StatefulWidget {
  const FilterDialog(this.employeeList, this.isLoading);

  final bool isLoading;
  final List<User> employeeList;

  @override
  _FilterDialogState createState() => _FilterDialogState();
}

class _FilterDialogState extends State<FilterDialog> {
  DateTime startDate;
  DateTime endDate;
  User user;

  @override
  Widget build(BuildContext context) {
    final DailyUpdateStore dailyUpdateStore =
        Provider.of<DailyUpdateStore>(context);
    if (dailyUpdateStore.isFilterApplied) {
      user ??= dailyUpdateStore.user;
      if (startDate == null && dailyUpdateStore.startDate != null) {
        startDate = DateTime.parse(dailyUpdateStore.startDate);
      }
      if (endDate == null && dailyUpdateStore.endDate != null) {
        endDate = DateTime.parse(dailyUpdateStore.endDate);
      }
    }
    return SingleChildScrollView(
      child: AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
        content: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Container(
            height: SizeConfig.screenHeight * 0.80,
            width: SizeConfig.screenWidth * 0.7,
            child: widget.isLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          IconButton(
                              icon: Icon(
                                Icons.cancel,
                                color: const Color.fromRGBO(53, 124, 184, 1),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              }),
                          Align(
                            alignment: Alignment.topRight,
                            child: Icon(
                              Icons.filter_list,
                              color: const Color.fromRGBO(53, 124, 184, 1),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      Container(
                        child: Text(
                          StringValue.ALERT_BOX_FIRST_TITLE,
                          style: const TextStyle(
                            color: Color.fromRGBO(53, 124, 184, 1),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.1),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                        ),
                        width: SizeConfig.screenWidth * 0.8,
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Center(
                          child: DropdownButtonFormField<User>(
                            items: getDropDownMenuItems(context),
                            value: user,
                            onChanged: (User value) {
                              setState(() {
                                user = value;
                              });
                            },
                            decoration:
                                InputDecoration(border: InputBorder.none),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 40.0,
                      ),
                      Container(
                        child: Text(
                          StringValue.ALERT_BOX_START_DATE,
                          style: const TextStyle(
                            color: Color.fromRGBO(53, 124, 184, 1),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      InkWell(
                        onTap: () {
                          DatePicker.showDatePicker(
                            context,
                            showTitleActions: true,
                            minTime: DateTime(2017, 1, 1),
                            // ignore: prefer_if_null_operators
                            maxTime:
                                // ignore: prefer_if_null_operators
                                endDate == null ? DateTime.now() : endDate,
                            currentTime: DateTime.now(),
                            onChanged: (DateTime date) {},
                            onConfirm: (DateTime date) {
                              setState(() {
                                startDate = date;
                              });
                            },
                          );
                        },
                        child: Container(
                          width: SizeConfig.screenWidth * 0.8,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: IgnorePointer(
                            ignoring: true,
                            child: TextField(
                              controller: TextEditingController(
                                  text: startDate == null
                                      ? ''
                                      : DateFormat.yMMMMd().format(startDate)),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 40.0,
                      ),
                      Container(
                        child: Text(
                          StringValue.ALERT_BOX_END_DATE,
                          style: const TextStyle(
                            color: Color.fromRGBO(53, 124, 184, 1),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      InkWell(
                        onTap: () {
                          DatePicker.showDatePicker(context,
                              showTitleActions: true,
                              minTime: startDate,
                              maxTime: DateTime.now(),
                              currentTime: DateTime.now(),
                              onChanged: (DateTime date) {},
                              onConfirm: (DateTime date) {
                            setState(() {
                              endDate = date;
                            });
                          }, locale: LocaleType.en);
                        },
                        child: Container(
                          width: SizeConfig.screenWidth * 0.8,
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: IgnorePointer(
                            ignoring: true,
                            child: TextField(
                              controller: TextEditingController(
                                  text: endDate == null
                                      ? ''
                                      : DateFormat.yMMMMd().format(endDate)),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 40.0,
                      ),
                      StoreObserver<DailyUpdateStore>(builder:
                          (DailyUpdateStore dailyUpdateStore,
                              BuildContext context) {
                        return RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(10))),
                          onPressed: () {
                            if (!dailyUpdateStore.isFetching) {
                              if (startDate != null) {
                                if (endDate != null) {
                                  dailyUpdateStore.onChanged(
                                      user: user,
                                      startDate: startDate.toString(),
                                      endDate: endDate.toString());
                                } else {
                                  dailyUpdateStore.onChanged(
                                    user: user,
                                    startDate: startDate.toString(),
                                  );
                                }
                              } else if (endDate != null) {
                                dailyUpdateStore.onChanged(
                                    user: user, endDate: endDate.toString());
                              } else {
                                dailyUpdateStore.onChanged(user: user);
                              }
                              if (user != null) {
                                dailyUpdateStore.fetchData(length: 20);
                                when(
                                  (_) => !dailyUpdateStore.isFetching,
                                  () {
                                    Navigator.pop(context);
                                  },
                                );
                              } else {
                                dailyUpdateStore.removeFilter();
                                Navigator.pop(context);
                              }
                            }
                          },
                          color: const Color.fromRGBO(53, 124, 184, 1),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(10),
                              ),
                            ),
                            height: 35.0,
                            width: SizeConfig.screenWidth * 0.8,
                            child: Center(
                              child: dailyUpdateStore.isFetching
                                  ? Container(
                                      height: 25.0,
                                      width: 25.0,
                                      child: const CircularProgressIndicator(
                                        valueColor:
                                            AlwaysStoppedAnimation<Color>(
                                                Colors.white),
                                        strokeWidth: 2,
                                      ),
                                    )
                                  : const Text(
                                      StringValue.DONE_TEXT,
                                      style: TextStyle(color: Colors.white),
                                    ),
                            ),
                          ),
                        );
                      }),
                      const SizedBox(
                        height: 10,
                      ),
                      RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10))),
                        onPressed: () {
                          final DailyUpdateStore dailyUpdateStore =
                              Provider.of<DailyUpdateStore>(context);
                          if (!dailyUpdateStore.isFetching) {
                            setState(() {
                              startDate = null;
                              endDate = null;
                              user = null;
                            });
                            dailyUpdateStore.onChanged(
                                user: null, startDate: null, endDate: null);
                            dailyUpdateStore.removeFilter();
                            Navigator.pop(context);
                          }
                        },
                        color: const Color.fromRGBO(53, 124, 184, 1),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          width: SizeConfig.screenWidth * 0.8,
                          height: 35.0,
                          alignment: Alignment.center,
                          child: Text(
                            StringValue.CLEAR_FIELD_TEXT,
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  List<DropdownMenuItem<User>> getDropDownMenuItems(BuildContext context) {
    List<DropdownMenuItem<User>> items = [];
    // ignore: avoid_function_literals_in_foreach_calls
    widget.employeeList.forEach(
      (User user) {
        items.add(
          DropdownMenuItem<User>(
            value: user,
            key: ObjectKey(user),
            child: Text(user.fullName),
          ),
        );
      },
    );
    items.add(
      const DropdownMenuItem<User>(
        child: Text('All'),
      ),
    );
    return items;
  }
}
