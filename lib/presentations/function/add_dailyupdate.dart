import 'package:devslane_erp_flutter/presentations/customs/store_observer.dart';
import 'package:devslane_erp_flutter/stores/daily_update_store.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:devslane_erp_flutter/utils/string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

class AddDailyUpdates extends StatefulWidget {
  static const String routeNamed = 'Add DailyUpdates';

  @override
  _AddDailyUpdatesState createState() => _AddDailyUpdatesState();
}

class _AddDailyUpdatesState extends State<AddDailyUpdates> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String description = '';
  DateTime _startDate = DateTime.now();
  WebViewController _webViewController;
  bool editorLoading;
  String code = '';
  ReactionDisposer reactionDisposer;

  void popNavigation(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback(
      (Duration duration) {
        reactionDisposer = autorun(
          (Reaction e) {
            if (!Provider.of<DailyUpdateStore>(context).isAdding) {
              Navigator.pop(context);
            }
          },
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    editorLoading = true;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Post'),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            height: SizeConfig.screenHeight * 0.8,
            width: SizeConfig.screenWidth,
            margin: const EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    DatePicker.showDatePicker(context,
                        showTitleActions: true,
                        minTime: DateTime(DateTime.now().year,
                            DateTime.now().month, DateTime.now().day - 1),
                        maxTime: DateTime(DateTime.now().year,
                            DateTime.now().month, DateTime.now().day),
                        currentTime: DateTime.now(),
                        onChanged: (DateTime date) {},
                        onConfirm: (DateTime date) {
                      setState(() {
                        _startDate = date;
                      });
                    }, locale: LocaleType.en);
                  },
                  child: Container(
                    width: SizeConfig.screenWidth * 0.8,
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Colors.grey.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: IgnorePointer(
                      ignoring: true,
                      child: TextField(
                        controller: TextEditingController(
                            text: DateFormat.yMMMMd().format(_startDate)),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          labelText: 'Date',
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                editorLoading
                    ? Container(
                        height: 80.0,
                        child: const Center(
                          child: CircularProgressIndicator(),
                        ),
                      )
                    : const SizedBox(),
                Container(
                  height: SizeConfig.screenHeight * 0.4,
                  width: SizeConfig.screenWidth,
//                  padding: const EdgeInsets.all(10),
                  child: Center(
                    child: WebView(
                      initialUrl: '',
                      onPageFinished: (String s) {
                        setState(() {
                          editorLoading = false;
                        });
                      },
                      javascriptMode: JavascriptMode.unrestricted,
                      // ignore: always_specify_types, prefer_collection_literals
                      javascriptChannels: Set.from(
                        <dynamic>[
                          JavascriptChannel(
                            name: 'Add',
                            onMessageReceived: (JavascriptMessage message) {
                              if (message.message != null &&
                                  message.message.isNotEmpty &&
                                  message.message.trim().isNotEmpty &&
                                  message.message != '<p><br></p>') {
                                Provider.of<DailyUpdateStore>(context)
                                    .addDailyUpdate(
                                        date: _startDate.toString(),
                                        description: message.message);
                                popNavigation(context);
                              }
                            },
                          ),
                        ],
                      ),
                      onWebViewCreated: (WebViewController webViewController) {
                        webViewController.evaluateJavascript(
                          """document.write(`
                         <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
                         <script src="https://cdn.quilljs.com/1.3.6/quill.js" ></script>
                         <div id="editor">
                         </div>
                         <script>
                         var quill = new Quill('#editor', {
                                               theme: 'snow',
                                               readOnly: false,
                                              });
                         function save() {
                           Add.postMessage(quill.root.innerHTML);
                         }
                         </script>
                         `
                         )""",
                        );
                        setState(() {
                          _webViewController = webViewController;
                        });
                      },
                    ),
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: StoreObserver<DailyUpdateStore>(
                    builder: (DailyUpdateStore dailyUpdateStore,
                        BuildContext context) {
                      return dailyUpdateStore.isAdding
                          ? CupertinoActivityIndicator()
                          : FlatButton(
                              color: Colors.blueAccent,
                              onPressed: () {
                                _webViewController.evaluateJavascript('save()');
                              },
                              child: const Text(
                                StringValue.DONE_TEXT,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400),
                              ),
                            );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
