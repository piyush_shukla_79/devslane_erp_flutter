import 'package:devslane_erp_flutter/models/daily_update.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:flutter/material.dart';

@immutable
class UpdatesListBuilder extends StatelessWidget {
  const UpdatesListBuilder({this.scrollController, this.dailyUpdates});

  final ScrollController scrollController;
  final DailyUpdates dailyUpdates;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Card(
      margin: EdgeInsets.symmetric(
          horizontal: SizeConfig.screenWidth / 28.235, vertical: 5.0),
      child: Container(
        width: SizeConfig.screenWidth,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: const EdgeInsets.all(10.0),
              child: Text(
                dailyUpdates.date,
                style: const TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                  vertical: 10.0, horizontal: SizeConfig.screenWidth / 16.941),
              child: ListView.builder(
                shrinkWrap: true,
                controller: scrollController,
                itemCount: 3,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    margin: const EdgeInsets.symmetric(
                      vertical: 2.5,
                    ),
                    child: Text(
                      dailyUpdates.message,
                      textAlign: TextAlign.justify,
                      style: const TextStyle(fontSize: 16.0),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
