import 'package:devslane_erp_flutter/presentations/customs/store_observer.dart';
import 'package:devslane_erp_flutter/stores/request_store.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:devslane_erp_flutter/utils/string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:mobx/mobx.dart';

class AddRequest extends StatefulWidget {
  @override
  _AddRequestState createState() => _AddRequestState();
}

class _AddRequestState extends State<AddRequest> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String description = '';
  DateTime _startDate;
  DateTime _endDate;
  String data = 'leave';

  List<DropdownMenuItem<String>> getDropDownMenuItems(BuildContext context) {
    final List<DropdownMenuItem<String>> items = <DropdownMenuItem<String>>[
      const DropdownMenuItem<String>(
        value: 'leave',
        child: Text('Leave'),
      ),
      const DropdownMenuItem<String>(
        value: 'work_from_home',
        child: Text('Work from home'),
      ),
      const DropdownMenuItem<String>(
        value: 'overtime',
        child: Text('Overtime'),
      ),
      const DropdownMenuItem<String>(
        value: 'half_day',
        child: Text('Half day'),
      ),
    ];

    return items;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return StoreObserver<RequestStore>(
      builder: (RequestStore requestStore, BuildContext b) {
        return Form(
          key: _formKey,
          child: SimpleDialog(
            titlePadding: const EdgeInsets.only(left: 20.0, right: 10.0),
            contentPadding: const EdgeInsets.all(20),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                const Text(StringValue.REQUEST),
                IconButton(
                  icon: Icon(Icons.cancel),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )
              ],
            ),
            children: <Widget>[
              InkWell(
                onTap: () {},
                child: Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 15.0, vertical: 8.0),
                  decoration: BoxDecoration(
                      color: Colors.grey.withOpacity(0.1),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(10))),
                  width: SizeConfig.screenWidth * 0.8,
                  child: Center(
                    child: DropdownButtonFormField<String>(
                      items: getDropDownMenuItems(context),
                      value: data, //currentEmployee,
                      onChanged: (String value) {
                        setState(() {
                          data = value;
                        });
                      },
                      decoration: InputDecoration(border: InputBorder.none),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                padding: const EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 1),
                    borderRadius: BorderRadius.circular(5.0)),
                child: TextFormField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Reason',
                  ),
                  maxLength: 100,
                  onSaved: (String value) {
                    setState(() {
                      description = value;
                    });
                  },
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Reason required';
                    }
                    return null;
                  },
                ),
              ),
              const SizedBox(
                height: 10.0,
              ),
              InkWell(
                onTap: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime.now(),
                      maxTime: _endDate,
                      currentTime: DateTime.now(),
                      onChanged: (DateTime date) {},
                      onConfirm: (DateTime date) {
                    setState(() {
                      _startDate = date;
                    });
                  }, locale: LocaleType.en);
                },
                child: Container(
                  padding: const EdgeInsets.only(left: 20),
                  width: SizeConfig.screenWidth * 0.8,
                  decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: IgnorePointer(
                    ignoring: true,
                    child: TextFormField(
                      controller: TextEditingController(
                          text: _startDate == null
                              ? ''
                              : DateFormat.yMMMMd().format(_startDate)),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        labelText: StringValue.ALERT_BOX_START_DATE,
                        hintStyle: TextStyle(),
                      ),
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Start Date required';
                        }
                        return null;
                      },
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              _startDate == null || data == 'half_day' || data == 'overtime'
                  ? Container()
                  : InkWell(
                      onTap: () {
                        DatePicker.showDatePicker(context,
                            showTitleActions: true,
                            minTime: _startDate,
                            currentTime: DateTime.now(),
                            onChanged: (DateTime date) {},
                            onConfirm: (DateTime date) {
                          setState(() {
                            _endDate = date;
                          });
                        }, locale: LocaleType.en);
                      },
                      child: Container(
                        padding: const EdgeInsets.only(left: 20),
                        width: SizeConfig.screenWidth * 0.8,
                        decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: IgnorePointer(
                          ignoring: true,
                          child: TextFormField(
                            controller: TextEditingController(
                                text: _endDate == null
                                    ? ''
                                    : DateFormat.yMMMMd().format(_endDate)),
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              labelText: StringValue.ALERT_BOX_END_DATE,
                              hintStyle: TextStyle(),
                            ),
                            validator: (String value) {
                              if (value.isEmpty) {
                                return 'End Date required';
                              }
                              return null;
                            },
                          ),
                        ),
                      ),
                    ),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  FlatButton(
                    child: Text(
                      StringValue.CANCEL,
                      style: TextStyle(color: Colors.black26),
                    ),
                    onPressed: () {
                      if (!requestStore.isCreatingRequest) {
                        Navigator.of(context).pop();
                      }
                    },
                  ),
                  FlatButton(
                    child: requestStore.isCreatingRequest
                        ? const CircularProgressIndicator()
                        : Text(
                            StringValue.ADD,
                            style:
                                TextStyle(color: Theme.of(context).errorColor),
                          ),
                    onPressed: () {
                      _formKey.currentState.save();
                      _formKey.currentState.validate();
                      if (_formKey.currentState.validate() &&
                          _startDate != null &&
                          !requestStore.isCreatingRequest) {
                        requestStore.createRequest(
                            type: data,
                            startDate: _startDate.toString(),
                            endDate: _endDate.toString(),
                            description: description);
                        when((_) => !requestStore.isCreatingRequest, () {
                          Navigator.pop(context);
                        });
                      }
                    },
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
