import 'package:devslane_erp_flutter/presentations/home_page.dart';
import 'package:devslane_erp_flutter/presentations/login_page.dart';
import 'package:devslane_erp_flutter/stores/user_store.dart';
import 'package:devslane_erp_flutter/utils/globals.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SplashPage extends StatelessWidget {
  static const String routeNamed = 'SplashPage';

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((v) {
      _navigateToScreen(context);
    });
    return Scaffold(
      body: Container(
        height: SizeConfig.screenHeight,
        width: SizeConfig.screenWidth,
        child: const Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  void _navigateToScreen(BuildContext context) {
    preferencesService.getIsFirstLaunch().then((bool first) {
      if (first == null || first) {
        Navigator.pushReplacementNamed(
          context,
          LoginPage.routeNamed,
        );
      } else {
        Provider.of<UserStore>(context).setUserLoggedIn();
        Navigator.pushReplacementNamed(context, HomePage.routeNamed);
      }
    });
  }
}
