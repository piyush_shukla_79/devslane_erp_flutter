import 'package:devslane_erp_flutter/stores/user_store.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:devslane_erp_flutter/utils/string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

import 'customs/store_observer.dart';

class ForgetPasswordPage extends StatefulWidget {
  static const String routeNamed = 'Forgot Password';

  @override
  _ForgetPasswordPageState createState() => _ForgetPasswordPageState();
}

class _ForgetPasswordPageState extends State<ForgetPasswordPage> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  AppBar appBar;
  TextEditingController textEditingController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool showObscureText = true;
  double appBarHeight;
  ReactionDisposer reactionDisposer;

  @override
  void initState() {
    super.initState();
    initReactionFunction(context);
    appBar = AppBar(
      backgroundColor: Colors.white,
      title: Container(
        margin: const EdgeInsets.symmetric(vertical: 5.0),
        child: Center(
          child: Image.asset(
            'assets/devslaneIcon.jpg',
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
    appBarHeight = appBar.preferredSize.height;
  }

  void initReactionFunction(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback(
      (Duration v) {
        final UserStore userStore = Provider.of<UserStore>(context);
        reactionDisposer = autorun(
          (Reaction e) {
            if (userStore.isError) {
              _scaffoldKey.currentState.showSnackBar(
                const SnackBar(
                  content: Text('This email address does not exists.'),
                  duration: Duration(seconds: 3),
                ),
              );
            }
          },
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    reactionDisposer.call();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: true,
        appBar: appBar,
        body: SingleChildScrollView(
          child: Container(
            width: SizeConfig.screenWidth,
            height: SizeConfig.safeHeight - appBarHeight,
            margin: const EdgeInsets.symmetric(horizontal: 25.0),
            child: FormBuilder(
              key: _fbKey,
              autovalidate: false,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: SizeConfig.screenWidth,
                    child: const Text(
                      StringValue.EMAIL,
                      style: TextStyle(color: Colors.blue, fontSize: 18.0),
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    decoration: BoxDecoration(
                      color: Colors.black12,
                      borderRadius:
                          const BorderRadius.all(Radius.circular(10.0)),
                    ),
                    child: Center(
                      child: FormBuilderTextField(
                        controller: textEditingController,
                        attribute: 'email',
                        keyboardType: TextInputType.emailAddress,
                        validators: [
                          FormBuilderValidators.required(),
                          FormBuilderValidators.email()
                        ],
                        decoration: InputDecoration(border: InputBorder.none),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 25.0,
                  ),
                  StoreObserver<UserStore>(
                    builder: (UserStore userStore, BuildContext context) {
                      return RaisedButton(
                        color: const Color.fromRGBO(50, 152, 219, 1),
                        shape: RoundedRectangleBorder(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(10.0),
                          ),
                        ),
                        onPressed: () {
                          userStore.forgetPassword(
                              textEditingController.value.text.toString());
                          when((_) => userStore.isChangingPassword, () {
                            _scaffoldKey.currentState.showSnackBar(
                              const SnackBar(
                                content: Text('Please Check Your Mail Box'),
                                duration: Duration(seconds: 2),
                              ),
                            );
                            Future<void>.delayed(const Duration(seconds: 2),
                                () {
                              Navigator.of(context).pop();
                            });
                          });
                        },
                        child: Container(
                          height: 50.0,
                          child: Center(
                            child: userStore.isLoggingIn
                                ? CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white),
                                  )
                                : Text(
                                    StringValue.SEND_ME_LINK,
                                    style: TextStyle(color: Colors.white),
                                  ),
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
