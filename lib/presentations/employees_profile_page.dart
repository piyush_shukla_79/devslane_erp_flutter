import 'package:devslane_erp_flutter/models/tags.dart';
import 'package:devslane_erp_flutter/models/user.dart';
import 'package:devslane_erp_flutter/stores/employee_store.dart';
import 'package:devslane_erp_flutter/utils/size_config.dart';
import 'package:flutter/material.dart';

import 'customs/chip.dart';
import 'customs/page_icon_chipset.dart';
import 'customs/store_observer.dart';

class EmployeesProfilePage extends StatefulWidget {
  static const String routeNamed = 'EmployeesProfile';

  @override
  _EmployeesProfilePageState createState() => _EmployeesProfilePageState();
}

class _EmployeesProfilePageState extends State<EmployeesProfilePage> {
  ScrollController controller = ScrollController();
  TextEditingController textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();

    textEditingController.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return StoreObserver<EmployeeStore>(
      builder: (EmployeeStore employeeStore, BuildContext context) {
        if (employeeStore.employeeList.isEmpty ||
            employeeStore.employeeList == null) {
          employeeStore.fetchData();
        }
        return Stack(
          children: <Widget>[
            Container(
              height: SizeConfig.screenHeight,
              width: SizeConfig.screenWidth,
              margin: const EdgeInsets.only(left: 15.0, right: 15.0, top: 55.0),
              child: SingleChildScrollView(
                controller: controller,
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10.0),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 1.0),
                      ),
                      padding: const EdgeInsets.symmetric(
                        vertical: 10.0,
                        horizontal: 15.0,
                      ),
                      child: TextField(
                        controller: textEditingController,
                        decoration: InputDecoration.collapsed(
                          hintText: 'Search',
                        ),
                      ),
                    ),
                    employeeStore.isLoading
                        ? const Center(
                            child: CircularProgressIndicator(),
                          )
                        : employeeStore.employeeList == null ||
                                employeeStore.employeeList.isEmpty ||
                                searchResult(employeeStore).isEmpty
                            ? const Center(
                                child: Text('No data found'),
                              )
                            : ListView.builder(
                                shrinkWrap: true,
                                controller: controller,
                                itemBuilder: (BuildContext context, int index) {
                                  return employeeStore
                                          .employeeList[index].fullName
                                          .toLowerCase()
                                          .contains(textEditingController.text
                                              .toLowerCase())
                                      ? profileView(
                                          employeeStore.employeeList[index])
                                      : const SizedBox();
                                },
                                itemCount: employeeStore.employeeList.length,
                              )
                  ],
                ),
              ),
            ),
            PageIcon(
              iconData: Icons.people,
            )
          ],
        );
      },
    );
  }

  List<Widget> chipSetList(List<Tags> tags) {
    final List<Widget> list = List();
    tags.forEach((tag) {
      list.add(chipSet(tag.title));
    });
    return list;
  }

  Widget profileView(User user) {
    final List<Widget> widgetList = chipSetList(user.tagsMap);
    return Card(
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      shape: RoundedRectangleBorder(
        borderRadius: const BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      child: Container(
        margin: const EdgeInsets.all(20.0),
        width: SizeConfig.screenWidth,
        child: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10.0),
              height: SizeConfig.screenHeight * 0.2,
              width: SizeConfig.screenHeight * 0.2,
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.circular(SizeConfig.screenHeight * 0.1),
              ),
              child: ClipRRect(
                borderRadius:
                    BorderRadius.circular(SizeConfig.screenHeight * 0.1),
                child: user.image == null
                    ? Image.asset(
                        'assets/user.png',
                        fit: BoxFit.cover,
                      )
                    : Image.network(
                        user.image,
                        fit: BoxFit.cover,
                        loadingBuilder: (BuildContext context, Widget child,
                            ImageChunkEvent loadingProgress) {
                          if (loadingProgress == null) {
                            return child;
                          }
                          return Center(
                            child: CircularProgressIndicator(
                                value: loadingProgress.expectedTotalBytes !=
                                        null
                                    ? loadingProgress.cumulativeBytesLoaded /
                                        loadingProgress.expectedTotalBytes
                                    : null),
                          );
                        },
                      ),
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
            Container(
              alignment: Alignment.topLeft,
              width: SizeConfig.screenWidth,
              margin: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Text(
                user.fullName,
                style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold),
              ),
            ),
            const SizedBox(
              height: 8.0,
            ),
            userDetails('Gender', user.gender),
            const SizedBox(
              height: 8.0,
            ),
            userDetails('Email', user.email),
            const SizedBox(
              height: 8.0,
            ),
            userDetails('Phone', user.phone),
            const SizedBox(
              height: 8.0,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Wrap(
                spacing: 5,
                runSpacing: 5,
                children: widgetList,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget userDetails(String field, String value) {
    return value != null
        ? Container(
            margin: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  field + ': ',
                  style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.grey,
                      fontWeight: FontWeight.bold),
                ),
                Expanded(
                  child: Text(
                    value,
                    style: const TextStyle(
                      fontSize: 18.0,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ],
            ),
          )
        : Container();
  }

  Widget chipSet(String text) {
    return ChipSet(
      child: Text(
        text,
        style: TextStyle(color: Colors.black, fontSize: 15.0),
      ),
    );
  }

  List<User> searchResult(EmployeeStore employeeStore) {
    if (textEditingController.text.isNotEmpty) {
      return employeeStore.employeeList.where((User employee) {
        return employee.fullName
            .toLowerCase()
            .contains(textEditingController.text.toLowerCase());
      }).toList();
    } else {
      return employeeStore.employeeList;
    }
  }
}
